<header>
<!-- 	<h1>Haruhi</h1> -->
	<div id="logo"><img src="images/logo.png" alt="Haruhi" /></div>

	<nav>
	<a href="index.php">Accueil</a>
	<?php if ($fonctionnalites_statut['intervenants'] && testerPermission(PAGE_INTERVENANT)) {?><a href="intervenant.php">Intervenant</a><?php }?>
	<?php if ($fonctionnalites_statut['intervenants'] && testerPermission(LIRE_DOSSIER)) {?><a href="dossiers.php">Dossiers</a><?php }?>
	<?php if (testerPermission(LISTE_ADHERENT)) {?><a href="adherents.php">Adhérents</a><?php }?>
	<?php if ($fonctionnalites_statut['factures'] && testerPermission(VOIR_FACTURE)) {?><a href="factures.php">Factures</a><?php }?>
	<?php if ($fonctionnalites_statut['stockage']) {?><a href="stockage.php">Stockage</a><?php }?>
	<?php if ($fonctionnalites_statut['calendrier']) {?><a href="calendrier.php">Calendrier</a><?php }?>
	<?php if ($fonctionnalites_statut['tresorerie'] && testerPermission(GERER_TRESORERIE)) {?><a href="tresorerie.php">Trésorerie</a><?php }?>
	<?php if ($fonctionnalites_statut['yuki'] && testerPermission(CONSULTER_ML)) {?><a href="voir_ml.php">Mailing-List</a><?php }?>
	<?php if (testerPermission((TOUCHE_ADHERENT | GERER_ML | VOIR_LOGS_MAIL | SAUVEGARDE | TOUCHE_MODELE_MAIL | GERER_OAUTH | GERER_ACCUEIL))) {?><a href="administration.php">Administration</a><?php }?>
	<a href="mon_compte.php">Mon compte</a>
	<a href="<?php echo $infos_siteasso; ?>">Site de l'asso</a>
	<a href="deconnexion.php">Deconnexion</a>
<?php
if (isset($_SESSION['original_permission'])) {
    echo ' | ';
    echo 'Actuellement connecté en tant que ' . $_SESSION['permission_nom'];
    echo ' − ';
    echo '<a href="tester_permission.php?action=reset">Revenir en mode normal</a>';
}
?>
	</nav>
</header>
<section>
