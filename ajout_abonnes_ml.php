<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('se');
verifierSiUtilisateurAPermission(GERER_ML);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
	$idML = intval($_POST['id']);
else
	$idML = intval($_GET['id']);

if($idML <= 0)
	header('location: liste_ml.php');

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml WHERE id = '.$idML);
if(!($requete === false))
{
	$listeML = $requete->fetch();
	if($listeML === false)
	{
		ajouterErreurNotification('La liste de diffusion demandée n\'existe pas');
		header('location: liste_ml.php');
		exit();
	}
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$classesSystemeExterneDO = importerEvent('systemeExterne');
	$SystemeExterneDODestination = null;
	foreach($classesSystemeExterneDO as $classe)
	{
		if($classe == $listeML['classe'])
			$SystemeExterneDODestination = new $classe;
	}

	$mailingList = SystemeExterneHaruhi::getNomMLFromId($idML);
	$nomML = $mailingList['adresse'];

	$courriels = array();

	//On regarde si on a un courriel
	$courriel = trim(htmlspecialchars($_POST['courriel']));

	//Pas de courriel, c'est surement un adhérent alors
	if(is_null($courriel) || strlen($courriel) == 0)
	{
		$adherentId = intval($_POST['adherent']);

		//Pas d'adhérent, alors, c'est un titre
		if($adherentId == 0)
		{
			$titreId = intval($_POST['titre']);
			
			if($titreId > 0)
			{
				SystemeExterneHaruhi::addTitle($idML, $titreId);

				$adherents = recupererAdherents('titre = '.$titreId);
				foreach($adherents as $adherent)
					$courriels[] = $adherent;
			}
		} else {	
			//On récupère le mail associé
			$adherents = recupererAdherents('id = '.$adherentId);
			$adherent = $adherents[0]; //Il n'y en a qu'un, vu le filtre
			$courriel = $adherent['courriel'];
			if(!is_null($courriel) && strlen($courriel) > 0)
				$courriels[] = $adherent;
		}
	} else {
		$courriels[] = $adherent['courriel'] = $courriel;
	}

	foreach($courriels as $adhCourriel)
	{
		$SystemeExterneDODestination->addSubscriberOfSE($nomML, $adhCourriel);
	}

	ajouterSuccesNotification('L\'ajout dans la liste de diffusion a bien été effectuée');

	header('location: abonnes_ml.php?id='.$idML);
	exit();
}

importerEvent('systemeExterne');

$carnetAdresse = SystemeExterneHaruhi::getDisplayableSubscriberOfML($idML);
$listeIdAdherents = array();
foreach($carnetAdresse['adherents'] as $adherent)
{
    $listeIdAdherents[] = $adherent['idEntite'];
}
$listeIdTitres = array();
foreach($carnetAdresse['titres'] as $titre)
{
    $listeIdTitres[] = $titre['idEntite'];
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents');
if(!($requete === false))
{
    $adherents = $requete->fetchAll();
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'titre');
if(!($requete === false))
{
    $titres = $requete->fetchAll();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Liste des abonnées d'une mailing list</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Ajout d'un abonné d'une mailing list</h2>
    <p>
        Liste : <?php echo $listeML['nom']; ?><br />
        Ne remplir d'une des lignes
    </p>
    <form action="ajout_abonnes_ml.php" method="post">
        <div class="formulaire">
            <div class="ligne">
                <div class="cellule intitule"><label name="courriel">Courriel</label></div>
                <div class="cellule"><input name="courriel" type="email" /></div>
            </div>
            <?php
            foreach($carnetAdresse as $categorieNom => $listeCourriels)
            {
                if($categorieNom == 'adherents')
                {
                    echo '<div class="ligne">';
                    echo '<div class="cellule intitule"><label name="adherent">Adherent</label></div>';
                    echo '<div class="cellule"><select name="adherent">';
                    echo '<option value="0"></option>';
                    foreach($adherents as $adherent)
					{
                        if(!in_array($adherent['id'], $listeIdAdherents))
                            echo '<option value="'.$adherent['id'].'">'.$adherent['prenom'].' '.$adherent['nom'].'</option>';
                    }
                    echo '</select></div>';
                    echo '</div>';
                } else if($categorieNom == 'titres')
                {
                    echo '<div class="ligne">';
                    echo '<div class="cellule intitule"><label name="titre">Titre</label></div>';
                    echo '<div class="cellule"><select name="titre">';
                    echo '<option value="0"></option>';
                    foreach($titres as $titre)
					{
                        if(!in_array($titre['id'], $listeIdTitres))
                            echo '<option value="'.$titre['id'].'">'.$titre['nom'].'</option>';
                    }
                    echo '</select></div>';
                    echo '</div>';
                }
            }
            ?>
        </div>
        <p>
            <input type="hidden" name="envoi" value="1" />
            <input type="hidden" name="id" value="<?php echo $idML; ?>" />
            <input id="bouton_valider" type="submit" name="ajout" value="Ajouter" />
        </p>
    </form>
	<?php include('bas_page.php'); ?>
	</body>
</html>