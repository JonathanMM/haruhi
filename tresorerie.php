<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);

$where = '';
$whereLien = '';
$idExercice = null;

$requete = $pdo->query('SELECT id, nom FROM '.$bdd_prefixe.'tresorerie_exercices');
$exercices = $requete->fetchAll();

if(isset($_GET['exercice']) && intval($_GET['exercice']) > 0)
{
	$idExercice = intval($_GET['exercice']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_exercices WHERE id = '.$idExercice);
	$exercice = $requete->fetch();
	$where = ' WHERE date >= "'.$exercice['dateDebut'].'" AND date <= "'.$exercice['dateFin'].'"';
	$whereLien = ' WHERE idEtiquette IN ('.$exercice['etiquettes'].')';
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_mouvement'.$where.' ORDER BY date ASC');
$mouvements = $requete->fetchAll();

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_etiquettes');
$etiquettesSql = $requete->fetchAll();
$etiquettes = array();
foreach($etiquettesSql as $etiquette)
{
	$etiquettes[$etiquette['id']] = array('nom' => $etiquette['label'], 'couleur' => $etiquette['couleur']);
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette'.$whereLien);
$liensSql = $requete->fetchAll();
$liens = array();
foreach($liensSql as $lien)
{
	if(!isset($liens[$lien['idMouvement']]))
		$liens[$lien['idMouvement']] = array();

	$liens[$lien['idMouvement']][] = $lien['idEtiquette'];
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Trésorerie</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="tresorerie.css" type="text/css" media="screen" />

		<script type="text/javascript" src="js/tresorerie.js"></script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2>Trésorerie</h2>
		<form method="GET" action="tresorerie.php">
			Filter sur un exercice :<br />
			<select name="exercice">
				<option value="0">Aucun</option>
				<?php
				foreach($exercices as $ex)
				{
					echo '<option value="'.$ex['id'].'"';
					if(!is_null($idExercice) && $idExercice == $ex['id'])
						echo ' selected';
					echo '>'.$ex['nom'].'</option>';
				}
				?>
			</select>
			<button type="submit">Filtrer</button>
		</form>
		<?php if(!is_null($idExercice)) { ?>
			<a href="tresorerie_bilan.php?id=<?php echo $idExercice; ?>">Voir le bilan</a><br />
		<?php } ?>
		<a href="liste_tresorerie_etiquettes.php">Gérer les étiquettes</a><br />
		<a href="liste_tresorerie_exercices.php">Gérer les exercices</a><br />
		<a href="form_mouvement.php">Ajouter un mouvement</a>
    </nav>

    <div id="cadre_stockage">
	    <table class="mouvement" id="liste-mouvement">
	        <tr>
                <th class="cellule-date">Date</th>
                <th class="cellule-label">Libelé</th>
                <th class="cellule-etiquettes">Catégorie</th>
                <th class="cellule-credit">Crédit</th>
                <th class="cellule-debit">Débit</th>
				<th></th>
            </tr>
			<?php
			foreach($mouvements as $mouvement)
			{
				if(!is_null($idExercice) && !in_array($mouvement['id'], array_keys($liens))) //On restreint à un exercice, il faut que le mouvement ait au moins une étiquette
					continue;
				
				echo '<tr data-id="'.$mouvement['id'].'">';
					echo '<td class="cellule-date" data-info="date" data-value="'.$mouvement['date'].'">'.formater_date($mouvement['date']).'</td>';
					echo '<td class="cellule-label" data-info="label" data-value="'.$mouvement['label'].'">'.$mouvement['label'].'</td>';
					echo '<td class="cellule-etiquettes" data-info="etiquettes"';
					echo ' data-value="'.(isset($liens[$mouvement['id']]) ? implode(',', $liens[$mouvement['id']]) : '').'"';
					echo '><ul class="etiquettes-liste">';
					if(isset($liens[$mouvement['id']]))
					{
						foreach($liens[$mouvement['id']] as $lien)
						{
							$etiquette = $etiquettes[$lien];
							echo '<li class="etiquette-'.$lien.'">'.$etiquette['nom'].'</li>';
						}
					}
					echo '<li class="modifier-etiquettes"><img src="images/adherent_modifier.png" alt="Modifier" title="Modifier" /></li>';
					echo '</ul></td>';
					echo '<td class="cellule-credit"'.(is_null($mouvement['credit']) ? '>' : ' data-info="credit" data-value="'.$mouvement['credit'].'">'.formaterMontant($mouvement['credit'])).'</td>';
					echo '<td class="cellule-debit"'.(is_null($mouvement['debit']) ? '>' : ' data-info="debit" data-value="'.$mouvement['debit'].'">'.formaterMontant($mouvement['debit'])).'</td>';
					echo '<td>';
						echo '<a href="voir_mouvement.php?id='.$mouvement['id'].'"><img src="images/adherent_fiche.png" alt="Détails" title="Voir les détails" /></a> ';
						echo '<img src="images/adherent_supprimer.png" alt="Supprimer" title="Supprimer" class="supprimer-mouvement" />';
					echo '</td>';
				echo '</tr>';
			}
			?>
			<tr id="ligne-nouveau-mouvement">
				<td class="cellule-date"><input type="date" id="nouveau_mouvement_date" placeholder="Date (AAAA-MM-DD)" /></td>
				<td><input type="text" id="nouveau_mouvement_nom" placeholder="Libélé" /></td>
				<td>
					<ul id="nouveau_mouvement_categories_liste" class="etiquettes-liste"></ul>
					<select id="nouveau_mouvement_categories">
						<option disabled selected>Catégorie</option>
					<?php
						foreach($etiquettes as $idEtiquette => $etiquette)
						{
							echo '<option value="'.$idEtiquette.'">'.$etiquette['nom'].'</option>';
						}
					?>
					</select>
					<input type="hidden" id="nouveau_mouvement_categories_choisies" />
				</td>
				<td><input type="number" min="0" step="0.01" id="nouveau_mouvement_credit" placeholder="Crédit" /></td>
				<td><input type="number" min="0" step="0.01" id="nouveau_mouvement_debit" placeholder="Débit" /></td>
				<td><img src="images/sauvegarder.png" alt="Sauvegarder" title="Sauvegarder" id="nouveau_mouvement_sauvegarder" /></td>
			</tr>
        </table>
    </div>

	<?php include('bas_page.php'); ?>
	<script type="text/javascript">
		var etiquettes = <?php echo json_encode($etiquettes); ?>;
	</script>
	<style type="text/css">
	<?php
		foreach($etiquettes as $idEtiquette => $etiquette)
		{
			if(!is_null($etiquette['couleur']))
			{
				echo '.etiquettes-liste li.etiquette-'.$idEtiquette."\n";
				echo '{'."\n";
				echo '	border-color: '.$etiquette['couleur']."\n";
				echo '}'."\n";
			}
		}
	?>
	</style>
	</body>
</html>