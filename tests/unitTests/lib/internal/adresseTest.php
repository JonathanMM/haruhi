<?php
namespace UnitTests\Lib\Internal;

require_once(__DIR__.'/../../../../lib/internal/adresse.php');

use PHPUnit\Framework\TestCase;

class AdresseTest extends TestCase
{
    /**
     * @dataProvider providerParseAdresse
     */
    public function testParseAdresse($expected, $date)
    {
        $this->assertSame($expected, parseAdresse($date));
    }

    public function providerParseAdresse()
    {
        return array(
            [array(), ''],
            [array(), 'InvalidAdresse'],
            [array(array(
                'ligne1' => 'Ligne1',
                'ligne2' => 'Ligne2',
                'cp' => 'CP',
                'ville' => 'Ville',
                'pays' => 'Pays'
            )), 'Ligne1'.ADRESSE_SEPARATEUR_CASE.'Ligne2'.ADRESSE_SEPARATEUR_CASE.'CP'.ADRESSE_SEPARATEUR_CASE.'Ville'.ADRESSE_SEPARATEUR_CASE.'Pays']
        );
    }
    
    /**
     * @dataProvider providerAfficherAdresseSurUneLigne
     */
    public function testAfficherAdresseSurUneLigne($expected, $date)
    {
        $this->assertSame($expected, afficherAdresseSurUneLigne($date));
    }

    public function providerAfficherAdresseSurUneLigne()
    {
        return array(
            ['', ''],
            ['', 'InvalidAdresse'],
            ['Ligne1 Ligne2 CP Ville Pays', 'Ligne1'.ADRESSE_SEPARATEUR_CASE.'Ligne2'.ADRESSE_SEPARATEUR_CASE.'CP'.ADRESSE_SEPARATEUR_CASE.'Ville'.ADRESSE_SEPARATEUR_CASE.'Pays']
        );
    }
    
    /**
     * @dataProvider providerStringifyAdresse
     */
    public function testStringifyAdresse($expected, $date)
    {
        $this->assertSame($expected, stringifyAdresse($date));
    }

    public function providerStringifyAdresse()
    {
        return array(
            ['', array()],
            ['Ligne1'.ADRESSE_SEPARATEUR_CASE.'Ligne2'.ADRESSE_SEPARATEUR_CASE.'CP'.ADRESSE_SEPARATEUR_CASE.'Ville'.ADRESSE_SEPARATEUR_CASE.'Pays', array(array(
                'ligne1' => 'Ligne1',
                'ligne2' => 'Ligne2',
                'cp' => 'CP',
                'ville' => 'Ville',
                'pays' => 'Pays'
            ))]
        );
    }
}

?>