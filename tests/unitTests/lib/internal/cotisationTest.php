<?php

namespace UnitTests\Lib\Internal;

require_once(__DIR__ . '/../../../../lib/internal/cotisation.php');

use PHPUnit\Framework\TestCase;

class CotisationTest extends TestCase
{
    /**
     * @dataProvider providerDeterminerFinCotisationTypePeriode
     */
    public function testDeterminerFinCotisationTypePeriode($expected, $debut_periode, $fin_periode, $termine_periode, $aujourdhui)
    {
        $dateAuj = new \DateTime($aujourdhui);
        $this->assertSame($expected, determinerFinCotisationTypePeriode($debut_periode, $fin_periode, $termine_periode, $dateAuj));
    }

    public function providerDeterminerFinCotisationTypePeriode()
    {
        return array(
            ['2025-03-31', '0000-01-01', '0000-12-31', '0001-03-31', '2024-03-02'],
            ['2026-03-31', '0000-12-01', '0001-11-30', '0002-03-31', '2024-12-02'],
            ['2025-03-31', '0000-12-01', '0001-11-30', '0002-03-31', '2024-03-02'],
        );
    }
}
