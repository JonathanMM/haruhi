<?php
namespace UnitTests\Lib\Internal;

require_once(__DIR__.'/../../../../lib/internal/date.php');

use PHPUnit\Framework\TestCase;

class DateTest extends TestCase
{
    /**
     * @dataProvider providerFormater_date
     */
    public function testFormater_date($expected, $date)
    {
        $this->assertSame($expected, formater_date($date));
    }

    public function providerFormater_date()
    {
        return array(
            ['', '0000-00-00'],
            ['10', '10'],
            ['01/02/2003', '2003-02-01']
        );
    }

    /**
     * @dataProvider providerFormater_date_heure
     */
    public function testFormater_date_heure($expected, $dateHeure)
    {
        $this->assertSame($expected, formater_date_heure($dateHeure));
    }

    public function providerFormater_date_heure()
    {
        return array(
            [' 00:00:00', '0000-00-00 00:00:00'],
            ['01/02/2003 12:34:56', '2003-02-01 12:34:56']
        );
    }

    /**
     * @dataProvider providerReste_jour
     */
    public function testReste_jour($expected, $date)
    {
        $this->assertSame($expected, reste_jour($date));
    }

    public function providerReste_jour()
    {
        return array(
            ['', null],
            [0, date('Y-m-d')],
            [1, date('Y-m-d', time() + 24*3600)],
            [7, date('Y-m-d', time() + 7*24*3600)]
        );
    }
}

?>