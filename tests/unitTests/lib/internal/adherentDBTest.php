<?php
namespace UnitTests\Lib\Internal;

require_once(__DIR__.'/../../../classUnitTests/DBTestCase.class.php');
require_once(__DIR__.'/../../../../lib/internal/adherent.php');

use PHPUnit\Framework\TestCase;
use ClassUnitTest\DBTestCase;

class AdherentDBTest extends DBTestCase
{
    protected function setUp()
    {
        global $pdo, $bdd_prefixe;
        $pdo->exec('DELETE FROM '.$bdd_prefixe.'adherents');
    }

    public function testGetNouveauNumeroAdherent_baseVide()
    {
        $this->assertSame(1, getNouveauNumeroAdherent());
    }
    
    public function testGetNouveauNumeroAdherent_baseAvecAdherent()
    {
        global $pdo, $bdd_prefixe;
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (1, "Toto", "Jambon", "", 0, 0, "0000-00-00")');
        $this->assertSame(2, getNouveauNumeroAdherent());
    }
    
    public function testGetNouveauNumeroAdherent_baseAvecAdherents()
    {
        global $pdo, $bdd_prefixe;
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (1, "Toto", "Jambon", "", 0, 0, "0000-00-00")');
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (5, "Toto", "Jambon", "", 0, 0, "0000-00-00")');
        $this->assertSame(6, getNouveauNumeroAdherent());
    }

    public function testDeplacerElementsEntreAdherents()
    {
        global $pdo, $bdd_prefixe;
        //Assign
        $pdo->exec('DELETE FROM '.$bdd_prefixe.'factures');
        $pdo->exec('DELETE FROM '.$bdd_prefixe.'logs_mail');
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'factures (adherent, somme, date, payement, type, visibilite) 
        VALUES (1, 1, "2019-01-05", "Espece", 0, 1)');
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'logs_mail (idMembre, titre, contenu, destinataire, contexte, date) 
        VALUES (1, "Titre", "Contenu", "none@perdu.com", "unittest", "2019-01-05")');

        //Act
        deplacerElementsEntreAdherents(1, 2);

        //Assert
        $this->assertSame(0, $this->getNbElements('factures', 'adherent = 1'));
        $this->assertSame(1, $this->getNbElements('factures', 'adherent = 2'));
        $this->assertSame(0, $this->getNbElements('logs_mail', 'idMembre = 1'));
        $this->assertSame(1, $this->getNbElements('logs_mail', 'idMembre = 2'));
    }

    public function getNbElements($table, $filtre)
    {
        global $pdo, $bdd_prefixe;
        $rq = $pdo->query('SELECT COUNT(*) AS nb FROM '.$bdd_prefixe.$table.' WHERE '.$filtre);
        //fwrite(STDERR, print_r($pdo->errorInfo(), true));
        return intval($rq->fetch()['nb']);
    }
    
    public function testRecupererAdherents_sansFiltre()
    {
        global $pdo, $bdd_prefixe;
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (1, "Toto", "Jambon", "", 0, 0, "0000-00-00")');
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (5, "Toto", "Jambon", "", 0, 1, "0000-00-00")');

        $sut = recupererAdherents();

        $this->assertSame(2, count($sut));
    }
    
    public function testRecupererAdherents_avecFiltre()
    {
        global $pdo, $bdd_prefixe;
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (1, "Toto", "Jambon", "", 0, 0, "0000-00-00")');
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents (numero, prenom, nom, courriel, id_membre, titre, date_inscription) 
        VALUES (5, "Toto", "Jambon", "", 0, 1, "0000-00-00")');

        $sut = recupererAdherents('titre = 1');

        $this->assertSame(1, count($sut));
    }
}
?>