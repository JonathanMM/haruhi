<?php
namespace UnitTests\Lib\Internal;

require_once __DIR__ . '/../../../../lib/internal/adherent.php';
require_once __DIR__ . '/../../../../lib/internal/adresse.php';

use PHPUnit\Framework\TestCase;

class AdherentTest extends TestCase
{
    /**
     * @dataProvider providerGetValeurFusionner
     */
    public function testGetValeurFusionner($expected, $propriete, $adherentSource, $adherentDestination)
    {
        $this->assertSame($expected, getValeurFusionner($propriete, $adherentSource, $adherentDestination));
    }

    public function providerGetValeurFusionner()
    {
        return array(
            ['', 'prop', array(), array()],
            ['a', 'prop', array(), array('prop' => 'a')],
            ['a', 'prop', array('prop' => null), array('prop' => 'a')],
            ['a', 'prop', array('prop' => ''), array('prop' => 'a')],
            ['a', 'prop', array('prop' => 'a'), array()],
            ['a', 'prop', array('prop' => 'a'), array('prop' => null)],
            ['a', 'prop', array('prop' => 'a'), array('prop' => '')],
            ['a', 'prop', array('prop' => 'a'), array('prop' => 'a')],
            ['', 'prop', array('prop' => 'a'), array('prop' => 'b')],
        );
    }

    /**
     * @dataProvider providerGenererFormulaireFusion
     */
    public function testGenererFormulaireFusion($expected, $propriete, $adherentSource, $adherentDestination, $nom = "", $obligatoire = false, $aide = "", $typeInput = null)
    {
        $this->assertSame($expected, genererFormulaireFusion($propriete, $adherentSource, $adherentDestination, $nom, $obligatoire, $aide, $typeInput));
    }

    public function providerGenererFormulaireFusion()
    {
        return array(
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop"></label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input name="prop" value="" /></div>
        <div class="cellule">b</div>
        <div class="cellule"></div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'b')],
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop"></label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input name="prop" value="a" /></div>
        <div class="cellule">a</div>
        <div class="cellule"></div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'a')],
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop">nom</label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input name="prop" value="" /></div>
        <div class="cellule">b</div>
        <div class="cellule"></div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'b'), 'nom'],
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop"></label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input required name="prop" value="" /></div>
        <div class="cellule">b</div>
        <div class="cellule"></div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'b'), '', true],
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop"></label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input name="prop" value="" /></div>
        <div class="cellule">b</div>
        <div class="cellule">aide</div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'b'), '', false, 'aide'],
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop"></label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input type="number" name="prop" value="" /></div>
        <div class="cellule">b</div>
        <div class="cellule"></div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'b'), '', false, '', 'number'],
            ['
    <div class="ligne">
        <div class="cellule intitule"><label name="prop">nom</label></div>
        <div class="cellule">a</div>
        <div class="cellule"><input type="number" required name="prop" value="" /></div>
        <div class="cellule">b</div>
        <div class="cellule">aide</div>
    </div>', 'prop', array('prop' => 'a'), array('prop' => 'b'), 'nom', true, 'aide', 'number'],
        );
    }

    /**
     * @dataProvider providerGenererRequeteSQLListeAdherent
     */
    public function testGenererRequeteSQLListeAdherent($expected, $terme = null, $titre = null, $allCotisations = false, $tri = null, $ordre = null)
    {
        $this->assertSame($expected, genererRequeteSQLListeAdherent($terme, $titre, $allCotisations, $tri, $ordre));
    }

    public function providerGenererRequeteSQLListeAdherent()
    {
        return array(
            ['ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC'],
            ['ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', ''],
            ['WHERE a.prenom LIKE "%a%" OR a.nom LIKE "%a%" OR a.pseudo LIKE "%a%" ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', 'a'],
            ['WHERE a.prenom LIKE "%a%" OR a.prenom LIKE "%b%" OR a.nom LIKE "%a%" OR a.nom LIKE "%b%" OR a.pseudo LIKE "%a%" OR a.pseudo LIKE "%b%" ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', 'a b'],
            ['ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', null, array()],
            ['WHERE a.titre = 1 ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', null, array(1)],
            ['WHERE a.titre = 1 OR a.titre = 2 ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', null, array(1, 2)],
            ['WHERE (a.date_fin_cotisation < CURDATE() AND a.cotisation_infinie = 0 AND a.date_inscription != "0000-00-00") ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC', null, null, true],
            ['ORDER BY a.nom, a.prenom ASC', null, null, false, 'xxx'],
            ['ORDER BY a.nom, a.prenom ASC', null, null, false, 'xxx', 'xxx'],
            ['ORDER BY a.nom, a.prenom DESC', null, null, false, 'xxx', 'desc'],
            ['ORDER BY a.id ASC', null, null, false, 'id'],
            ['ORDER BY a.numero ASC', null, null, false, 'numero'],
            ['ORDER BY a.nom ASC', null, null, false, 'nom'],
            ['ORDER BY a.prenom ASC', null, null, false, 'prenom'],
            ['ORDER BY a.pseudo ASC', null, null, false, 'pseudo'],
            ['ORDER BY a.date_inscription ASC', null, null, false, 'di'],
            ['ORDER BY a.date_fin_cotisation ASC', null, null, false, 'df'],
            ['ORDER BY a.commentaire ASC', null, null, false, 'com'],
            ['ORDER BY t.nom ASC', null, null, false, 'statut'],
        );
    }

    /**
     * @dataProvider providerFormater_adresse
     */
    public function testFormater_adresse($expected, $adresse)
    {
        $this->assertSame($expected, formater_adresse($adresse));
    }

    public function providerFormater_adresse()
    {
        return array(
            ['', ''],
            ['a', 'a'],
            ['Ligne1 Ligne2 CP Ville Pays', 'Ligne1' . ADRESSE_SEPARATEUR_CASE . 'Ligne2' . ADRESSE_SEPARATEUR_CASE . 'CP' . ADRESSE_SEPARATEUR_CASE . 'Ville' . ADRESSE_SEPARATEUR_CASE . 'Pays'],
            ['Ligne1 CP Ville Pays', 'Ligne1' . ADRESSE_SEPARATEUR_CASE . ADRESSE_SEPARATEUR_CASE . 'CP' . ADRESSE_SEPARATEUR_CASE . 'Ville' . ADRESSE_SEPARATEUR_CASE . 'Pays'],
        );
    }

    /**
     * @dataProvider providerParserMailAdherent
     */
    public function testParserMailAdherent($expected, $contenu, $infosAdherentPlus = array())
    {
        global $infos_nomasso, $infos_siteasso;
        $infos_nomasso = 'NomASSO';
        $infos_siteasso = 'SiteASSO';
        $infosAdherent = array(
            'nom' => 'Nom',
            'prenom' => 'Prenom',
        );
        $infosAdherent = array_merge($infosAdherent, $infosAdherentPlus);
        $this->assertSame($expected, parserMailAdherent($contenu, $infosAdherent));
    }

    public function providerParserMailAdherent()
    {
        return array(
            [null, ''],
            ['{asso.LOL}', '{asso.LOL}'],
            ['NomASSO', '{asso.nom}'],
            ['SiteASSO', '{asso.haruhi}'],
            ['{adherent.LOL}', '{adherent.LOL}'],
            ['Nom', '{adherent.nom}'],
            ['Prenom', '{adherent.prenom}'],
            ['0000-00-00', '{adherent.fin_cotisation}'],
            ['2019-01-01', '{adherent.fin_cotisation}', array('date_fin_cotisation' => '2019-01-01')],
            ['{cotisation.date}', '{cotisation.date}'],
            ['2019-01-01', '{cotisation.date}', array('date_cotisation' => '2019-01-01')],
        );
    }
}
