<?php
namespace UnitTests\Lib\Internal;

require_once(__DIR__.'/../../../../lib/internal/calendrier.php');

use PHPUnit\Framework\TestCase;

class CalendrierTest extends TestCase
{
    /**
     * @dataProvider providerValeursConstants
     */
    public function testValeursConstants($cst)
    {
        $this->assertTrue(defined($cst));
    }

    public function providerValeursConstants()
    {
        return array(
            ['CALENDRIER_REPONSE_NON_REPONDU'],
            ['CALENDRIER_REPONSE_REFUS'],
            ['CALENDRIER_REPONSE_PROVISOIRE'],
            ['CALENDRIER_REPONSE_ACCEPTE']
        );
    }
}

?>