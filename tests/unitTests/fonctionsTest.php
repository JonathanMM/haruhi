<?php
namespace UnitTests;

require_once __DIR__ . '/../../fonctions.php';

use PHPUnit\Framework\TestCase;

class FonctionsTest extends TestCase
{

    /**
     * @dataProvider providerFormaterUrl
     */
    public function testFormaterUrl($expected, $base, $page)
    {
        $this->assertSame($expected, formaterUrl($base, $page));
    }

    public function providerFormaterUrl()
    {
        return array(
            ['', '', 'p'],
            ['', 'b', ''],
            ['base/page', 'base', 'page'],
            ['base/page', 'base/', 'page'],
        );
    }

}
