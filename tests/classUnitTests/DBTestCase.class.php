<?php
namespace ClassUnitTest;
use PHPUnit\Framework\TestCase;

abstract class DBTestCase extends TestCase
{
    protected static $oldPdo;
    protected static $oldBddPrefixe;

    public static function setUpBeforeClass()
    {
        global $pdo, $bdd_prefixe;
        self::$oldPdo = $pdo;
        self::$oldBddPrefixe = $bdd_prefixe;

        $pdo = new \PDO('sqlite::memory:');
        $bdd_prefixe = 'haruhi_';

        $sql = str_replace(array('int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT', 'ENGINE=MyISAM ', 'ENGINE=InnoDB', 'DEFAULT CHARSET=utf8'), array('INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT', ''), file_get_contents(__DIR__.'/../../install/install.sql'));
        $sql = preg_replace('/COMMENT (.*),/', ',', $sql);
        $pdo->exec($sql);
    }

    public static function tearDownAfterClass()
    {
        global $pdo, $bdd_prefixe;
        $pdo = self::$oldPdo;
        $bdd_prefixe = self::$oldBddPrefixe;
    }
}
?>