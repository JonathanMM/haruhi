<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(LISTE_ADHERENT);
verifierSiIdInGet('adherents.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<title>Haruhi → Fiche adhérent</title>
	<link rel="icon" type="image/png" href="images/favicon.png" />

	<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	<link rel="stylesheet" href="edition_adherent.css" type="text/css" media="print">
</head>

<body>
	<?php include('haut_page.php');
	$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'adherents WHERE id = ' . $id);
	$user = $requete->fetch();

	//On s'occupe des messages
	afficherNotification();
	?>
	<div id="cadre_haut">
		<div id="logo_asso_fiche"><img src="images/logo_asso_facture.png" alt="Logo" /></div>
		<h2 style="text-align: center;">FICHE ADHÉRENT</h2>
		<div id="infos_admin_adherent">
			<?php $inscription = formater_date($user['date_inscription']);
			if ($inscription == '')
				echo '<i>Non inscrit</i>';
			else {
				echo 'Numéro : ' . $user['numero'] . ' − ';
				echo 'Date d\'inscription : ' . $inscription . ' − ';
				if ($user['cotisation_infinie'] == 1)
					echo '<i>Adhérent à vie</i>';
				else
					echo 'Date de fin de cotisation : ' . formater_date($user['date_fin_cotisation']);
			} ?>
		</div>
	</div>

	<?php
	if ($fonctionnalites_statut['avatar'] && !is_null($user['avatar'])) {
	?>
		<div class="cadre_avatar">
			<img src="images/avatar/<?php echo $user['avatar']; ?>" alt="avatar" />
		</div>
	<?php
	}
	?>

	<div class="cadre_info">
		Nom : <?php echo $user['nom']; ?><br />
		Prénom : <?php echo $user['prenom']; ?><br />
		Pseudo : <?php echo $user['pseudo']; ?><br />
		Adresse : <?php $adresse = parseAdresse($user['adresse']);
					echo (count($adresse) > 0 ? implode(' ', $adresse[0]) : ''); ?><br />
		Téléphone : <?php echo $user['telephone']; ?><br />
		Courriel : <?php echo $user['courriel']; ?><br />
		Commentaire : <?php echo $user['commentaire']; ?>
	</div>

	<?php if ($fonctionnalites_statut['intervenants']) { ?>
		<div class="cadre_info">
			<h3>Enfants :</h3>
			<p>
				<?php
				$requete = $pdo->query('SELECT *, e.id AS id, e.prenom AS prenom, e.nom AS nom, a.prenom AS prenom_intervenant, a.nom AS nom_intervenant FROM ' . $bdd_prefixe . 'enfants e LEFT JOIN ' . $bdd_prefixe . 'adherents a ON a.id = e.id_intervenant WHERE id_parent = ' . $id);
				$i = 0;
				if (!($requete === false)) {
					$enfants = $requete->fetchAll();
					foreach ($enfants as $enfant) {
						echo '#' . $enfant['id'] . ' ' . $enfant['prenom'] . ' ' . $enfant['nom'] . ' ' . $enfant['classe'] . ' ' . formater_date($enfant['naissance']);
						if ($enfant['prenom_intervenant'] != NULL)
							echo ' (Intervenant : ' . $enfant['prenom_intervenant'] . ' ' . $enfant['nom_intervenant'] . ')';
						if ((int)$_SESSION['permission'] & TOUCHE_ADHERENT)
							echo '<span class="gestion_enfant"> − <a href="modif_enfant.php?id=' . $enfant['id'] . '">Modifier</a> <a href="modif_enfant.php?supp=' . $enfant['id'] . '">Supprimer</a></span>';
						echo '<br />';
						$i++;
					}
				}
				if ($i == 0)
					echo 'Pas d\'enfant'; ?>
				<br /><a class="gestion_enfant" href="ajout_enfant.php?id=<?php echo $id; ?>">Ajouter un enfant</a>
			</p>
		</div>

		<div class="cadre_info">
			<h3>Responsable en tant qu'intervenant de :</h3>
			<p>
				<?php
				$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'enfants WHERE id_intervenant = ' . $id);
				$i = 0;
				if (!($requete === false)) {
					$intervenants = $requete->fetchAll();
					foreach ($intervenants as $intervenant) {
						echo '#' . $intervenant['id'] . ' ' . $intervenant['prenom'] . ' ' . $intervenant['nom'] . ' ' . formater_date($intervenant['naissance']) . '<br />';
						$i++;
					}
				}
				if ($i == 0)
					echo 'Pas d\'enfant'; ?>
			</p>
		</div>
	<?php } ?>

	<?php if ($fonctionnalites_statut['factures']) { ?>
		<div class="cadre_info">
			<h3>Factures :</h3>
			<p>
				<?php $i = 0;
				$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'factures WHERE adherent = ' . $id);
				if (!($requete === false)) {
					$factures = $requete->fetchAll();
					foreach ($factures as $facture) {
						echo '#' . $facture['id'] . ' ' . formater_date($facture['date']) . ' ' . number_format($facture['somme'], 2, ',', ' ') . ' € ' . $facture['payement'] . '<br />';
						$i++;
					}
				}

				if ($i == 0)
					echo "Pas de facture";
				?>
			</p>
		</div>
	<?php } ?>

	<div id="cadre_bas">
		Date de génération : <?php echo date("d/m/Y"); ?>
	</div>

	<?php
	echo '<ul>';
	echo '<li><a href="contact_adherent.php?id=' . $user['id'] . '">Contacter</a></li>';
	if (testerPermission(TOUCHE_ADHERENT)) {
		echo '<li><a href="form_adherent.php?id=' . $user['id'] . '">Modifier</a></li>';
		echo '<li><a href="fusion_adherent.php?id=' . $user['id'] . '">Fusionner</a></li>';
		echo '<li><a href="supp_adherent.php?id=' . $user['id'] . '">Supprimer</a></li>';
		if ($user['id_membre'] == 0)
			echo '<li><a href="creer_compte.php?id=' . $user['id'] . '">Créer un compte</a></li>';
		else
			echo '<li><a href="generer_nouveau_mdp.php?id=' . $user['id'] . '">Regénérer le mot de passe</a></li>';
	}
	if (testerPermission(FAIRE_FACTURE))
		echo '<li><a href="cotisation.php?id=' . $user['id'] . '">Cotisation</a></li>';
	if (testerPermission(VOIR_LOGS_MAIL))
		echo '<li><a href="logs_mail.php?adherent=' . $user['id'] . '">Consulter les courriels envoyés</a></li>';
	echo '</ul>';
	?>

	<?php include('bas_page.php'); ?>
</body>

</html>