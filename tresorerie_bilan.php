<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);
verifierSiIdInGet('tresorerie.php');

$idExercice = intval($_GET['id']);
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_exercices WHERE id = '.$idExercice);
$exercice = $requete->fetch();

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_mouvement WHERE date >= "'.$exercice['dateDebut'].'" AND date <= "'.$exercice['dateFin'].'"');
$mouvements = $requete->fetchAll();

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_etiquettes WHERE id IN ('.$exercice['etiquettes'].')');
$etiquettesSql = $requete->fetchAll();
$etiquettes = array();
foreach($etiquettesSql as $etiquette)
{
	$etiquettes[$etiquette['id']] = array('nom' => $etiquette['label'], 'couleur' => $etiquette['couleur']);
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette WHERE idEtiquette IN ('.$exercice['etiquettes'].')');
$liensSql = $requete->fetchAll();
$liens = array();
foreach($liensSql as $lien)
{
	if(!isset($liens[$lien['idMouvement']]))
		$liens[$lien['idMouvement']] = array();

	$liens[$lien['idMouvement']][] = $lien['idEtiquette'];
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Bilan</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="tresorerie.css" type="text/css" media="screen" />
    </head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2>Bilan de Trésorerie</h2>
        <h3>Exercice : <?php echo $exercice['nom']; ?>, du <?php echo formater_date($exercice['dateDebut']); ?> au <?php echo formater_date($exercice['dateFin']); ?>.</h3>
	</nav>

    <ul>
        <?php
        $sommes = array();
        $totaux = array('debit' => 0, 'credit' => 0);
        foreach($mouvements as $mouvement)
        {
            if(!is_null($idExercice) && !in_array($mouvement['id'], array_keys($liens))) //On restreint à un exercice, il faut que le mouvement ait au moins une étiquette
                continue;

            foreach($liens[$mouvement['id']] as $idEtiquette)
            {
                if(!isset($sommes[$idEtiquette]))
                    $sommes[$idEtiquette] = 0;

                $sommes[$idEtiquette] += $mouvement['credit'] - $mouvement['debit'];

                $totaux['debit'] += $mouvement['debit'];
                $totaux['credit'] += $mouvement['credit'];
            }
        }

        foreach($etiquettes as $idEtiquette => $etiquette)
        {
            echo '<li>'.$etiquette['nom'].' : ';
            if(isset($sommes[$idEtiquette]))
                echo formaterMontant($sommes[$idEtiquette]);
            else
                echo formaterMontant(0);
            echo '</li>';
        }
        ?>
    </ul>

    <p>
        Total des recettes : <?php echo formaterMontant($totaux['credit']); ?><br />
        Total des dépenses : <?php echo formaterMontant($totaux['debit']); ?>
    </p>
        <?php include('bas_page.php'); ?>
        <style type="text/css">
        <?php
            foreach($etiquettes as $idEtiquette => $etiquette)
            {
                if(!is_null($etiquette['couleur']))
                {
                    echo '.etiquettes-liste li.etiquette-'.$idEtiquette."\n";
                    echo '{'."\n";
                    echo '	border-color: '.$etiquette['couleur']."\n";
                    echo '}'."\n";
                }
            }
        ?>
	</style>
	</body>
</html>