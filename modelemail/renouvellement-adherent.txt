Bonjour {adherent.prenom} {adherent.nom},

nous vous informons avec ce présent courriel que votre renouvellement au sein de l'association {asso.nom} a bien été pris en compte.

N'hésitez pas à nous contacter pour toute information.

Bonne journée ! :)