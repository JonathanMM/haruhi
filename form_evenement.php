<?php
require_once(__DIR__.'/vendor/autoload.php');
use Ramsey\Uuid\Uuid;

session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('calendrier');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;

	$titre = htmlspecialchars($_POST['titre'], ENT_QUOTES);
	$description = htmlspecialchars($_POST['description'], ENT_QUOTES);
	$date = htmlspecialchars($_POST['date'], ENT_QUOTES);
	$lieu = htmlspecialchars($_POST['lieu'], ENT_QUOTES);
	$lat = floatval($_POST['lat']);
	$lon = floatval($_POST['lon']);
	$categorie = intval($_POST['type']);
	$organisateur = intval($_POST['organisateur']);
	$participants = $_POST['participants'];
	if(!is_array($participants)) //Doit forcement être un tableau !
		$participants = array();
	$participants = array_diff($participants, array($organisateur)); //On retire l'organisateur pour éviter les doublons
	$requete = $pdo->query('SELECT c.id, c.nom FROM '.$bdd_prefixe.'calendrier_categories c
		INNER JOIN '.$bdd_prefixe.'calendrier_permissions p ON p.idCategorie = c.id
		WHERE p.idTitre = '.$_SESSION['id_titre'].' AND p.ecriture = 1');
	$categoriesSql = $requete->fetchAll();
	$categorieAutorise = false;
	foreach($categoriesSql as $cat)
	{
		if($cat['id'] == $categorie)
			$categorieAutorise = true;
	}
	if(!$categorieAutorise)
	{
		ajouterErreurNotification('Vous n\'êtes pas autorisé à utiliser cette catégorie.');
		header('location: calendrier.php');
		exit();
	}

	//On gère les participants

	if(strlen($titre) > 0 && strlen($date) > 0 && strlen($description) > 0)
	{
		$pdo->beginTransaction();
		if($mode == MODE_AJOUT)
		{
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier (uuid, titre, description, date, lieu, latitude, longitude, type) VALUE ("'.Uuid::uuid4().'", "'.$titre.'", "'.$description.'", "'.$date.'", "'.$lieu.'", "'.$lat.'", "'.$lon.'", '.$categorie.')');
			
			$idEvenement = $pdo->lastInsertId();

			$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_participants (idEvenement, idMembre, estOrganisateur, reponse) VALUES ('.$idEvenement.', '.$organisateur.', 1, '.CALENDRIER_REPONSE_ACCEPTE.')');
			
			foreach($participants as $idParticipant)
			{
				$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_participants (idEvenement, idMembre, estOrganisateur, reponse) VALUES ('.$idEvenement.', '.$idParticipant.', 0, '.CALENDRIER_REPONSE_NON_REPONDU.')');
			}

			ajouterSuccesNotification('L\'événement a été crée avec succès.');
		} elseif($mode == MODE_MODIF)
		{
			$pdo->exec('UPDATE '.$bdd_prefixe.'calendrier SET 
			titre = "'.$titre.'",
			description = "'.$description.'", 
			date = "'.$date.'", 
			lieu = "'.$lieu.'",
			latitude = "'.$lat.'",
			longitude = "'.$lon.'",
			type = '.$categorie.' WHERE id = '.$id);

			//Et on gère maintenant les participants
			$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier_participants 
			WHERE idEvenement = '.$id.' 
			ORDER BY estOrganisateur DESC'); //L'organisateur sera en premier
			$participantsSql = $requete->fetchAll();

			$organisateurActuel = null;
			$participantsActuels = array();
			foreach($participantsSql as $participant)
			{
				if($participant['estOrganisateur'] == 1)
					$organisateurActuel = $participant['idMembre'];
				else
					$participantsActuels[] = $participant['idMembre'];
			}

			if($organisateur != $organisateurActuel)
			{
				$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_participants (idEvenement, idMembre, estOrganisateur, reponse) VALUES ('.$id.', '.$organisateur.', 1, '.CALENDRIER_REPONSE_ACCEPTE.')');
				$pdo->exec('DELETE FROM '.$bdd_prefixe.'calendrier_participants WHERE idEvenement = '.$id.' AND idMembre = '.$organisateurActuel.' AND estOrganisateur = 1');
			}

			$participantsASupp = array_diff($participantsActuels, $participants);
			if(count($participantsASupp) > 0)
				$pdo->exec('DELETE FROM '.$bdd_prefixe.'calendrier_participants WHERE idEvenement = '.$id.' AND idMembre IN ('.implode(',', $participantsASupp).') AND estOrganisateur = 0');

			$participantsAAjouter = array_diff($participants, $participantsActuels);
			foreach($participantsAAjouter as $idParticipant)
			{
				$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_participants (idEvenement, idMembre, estOrganisateur, reponse) VALUES ('.$id.', '.$idParticipant.', 0, '.CALENDRIER_REPONSE_NON_REPONDU.')');
			}
			
			ajouterSuccesNotification('L\'événement a été modifié avec succès.');
		}
		$pdo->commit();
	} else
		ajouterErreurNotification('Erreur lors de la création de l\'événement : le titre, la description et la date sont obligatoires');
	
	header('location: calendrier.php');
	exit();
}

//Catégories
$requete = $pdo->query('SELECT c.id, c.nom FROM '.$bdd_prefixe.'calendrier_categories c
INNER JOIN '.$bdd_prefixe.'calendrier_permissions p ON p.idCategorie = c.id
WHERE p.idTitre = '.$_SESSION['id_titre'].' AND p.ecriture = 1');
$categoriesSql = $requete->fetchAll();
$categories = array();
foreach($categoriesSql as $categorie)
{
    $categories[$categorie['id']] = $categorie['nom'];
}

//Utilisateurs
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents');
$adherentsSql = $requete->fetchAll();
$adherents = array();
foreach($adherentsSql as $adherent)
{
	$adherents[$adherent['id']] = $adherent['prenom'].' '.$adherent['nom'];
}

if(isset($_GET['id']) && intval($_GET['id']) > 0)
{
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier WHERE id = '.$id);
	$evenement = $requete->fetch();

	if(!in_array($evenement['type'], array_keys($categories)))
	{
		ajouterErreurNotification('Vous n\'avez pas le droit de modifier cet événement.');
		header('location: calendrier.php');
		exit();
	}

	//Participants
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier_participants 
	WHERE idEvenement = '.$id.' 
	ORDER BY estOrganisateur DESC'); //L'organisateur sera en premier
	$participantsSql = $requete->fetchAll();

	$organisateur = null;
	$participants = array();
	foreach($participantsSql as $participant)
	{
		if($participant['estOrganisateur'] == 1)
			$organisateur = $participant['idMembre'];
		else
			$participants[] = $participant['idMembre'];
	}
	$mode = MODE_MODIF;
} else
	$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un événement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<script type="text/javascript">
		function getXMLHttpRequest()
		{
			var xhr = null;
			
			if (window.XMLHttpRequest || window.ActiveXObject) {
				if (window.ActiveXObject) {
					try {
						xhr = new ActiveXObject("Msxml2.XMLHTTP");
					} catch(e) {
						xhr = new ActiveXObject("Microsoft.XMLHTTP");
					}
				} else {
					xhr = new XMLHttpRequest(); 
				}
			} else {
				alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
				return null;
			}
			
			return xhr;
		}

		function recup_lieu()
		{
			var xhr = getXMLHttpRequest();

			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
					var json = eval('(' + xhr.responseText + ')');
					document.getElementById("statut").innerHTML = "";
					afficher_lieu(json);
				} else if (xhr.readyState < 4) {
					document.getElementById("statut").innerHTML = "Recherche…";
				}
			};

			xhr.open("GET", "http://nominatim.openstreetmap.org/search/"+document.getElementById("lieu").value + "?format=json&countrycode=fr&accept-language=fr", true);
			xhr.send(null);
		}

		function afficher_lieu(infos)
		{
			if(infos.length != 0)
			{
				document.getElementById("statut").innerHTML = "OK";
				document.getElementById('lieu').value = infos[0].display_name;
				document.getElementById('lat').value = infos[0].lat;
				document.getElementById('lon').value = infos[0].lon;
			} else {
				document.getElementById("statut").innerHTML = "Aucun résultat";
			}
		}
		</script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un événement</h2>

	<form action="form_evenement.php" method="post">
		<div class="formulaire">
			<div class="ligne">
				<div class="cellule intitule"><label name="titre">Titre : </label></div>
				<div class="cellule"><input name="titre" value="<?php if($mode == MODE_MODIF) echo $evenement['titre']; ?>" required maxlength="128" /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="description">Description : </label></div>
				<div class="cellule"><textarea name="description"><?php if($mode == MODE_MODIF) echo $evenement['description']; ?></textarea></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="date">Date : </label></div>
				<div class="cellule"><input name="date" type="datetime-local" placeholder="AAAA-MM-JJ HH:MM:SS" value="<?php if($mode == MODE_MODIF) echo $evenement['date']; ?>" /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="lieu">Lieu : </label></div>
				<div class="cellule"><input name="lieu" onChange="recup_lieu();" type="text" value="<?php if($mode == MODE_MODIF) echo $evenement['lieu']; ?>" /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"></div>
				<div class="cellule">
					(De préférence : nom ou adresse, ville)
					<input type="hidden" id="lat" name="lat" />
					<input type="hidden" id="lon" name="lon" />
				</div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="type">Catégorie : </label></div>
				<div class="cellule">
					<select name="type">
						<?php
						foreach($categories as $idCategorie => $nomCategorie)
						{
							echo '<option value="'.$idCategorie.'"';
							if($mode == MODE_MODIF && $evenement['type'] == $idCategorie)
								echo ' selected';
							echo '>'.$nomCategorie.'</option>';
						}
						?>
					</select>
				</div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="organisateur">Organisateur : </label></div>
				<div class="cellule">
					<?php
					if($mode == MODE_AJOUT)
					{
						if(isset($adherents[$_SESSION['id_adherent']]))
						{
							echo $adherents[$_SESSION['id_adherent']];
							echo '<input type="hidden" name="organisateur" value="'.$_SESSION['id_adherent'].'" />';
						}
					} elseif($mode == MODE_MODIF)
					{
						echo '<select name="organisateur">';
						foreach($adherents as $idAdherent => $adherent)
						{
							echo '<option value="'.$idAdherent.'"';
							if($idAdherent == $organisateur)
								echo ' selected';
							echo '>'.$adherent.'</option>';
						}
						echo '</select>';
					}
					?>
				</div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="participants">Participants : </label></div>
				<div class="cellule">
					<select name="participants[]" multiple>
					<?php
						foreach($adherents as $idAdherent => $adherent)
						{
							echo '<option value="'.$idAdherent.'"';
							if($mode == MODE_MODIF && in_array($idAdherent, $participants))
								echo ' selected';
							echo '>'.$adherent.'</option>';
						}
					?>
					</select>
				</div>
			</div>
		</div>
		<p>
			<input type="hidden" name="id" value="<?php if($mode == MODE_MODIF) echo $id; ?>" />
			<input type="hidden" name="envoi" value="1" />
			<input id="bouton_valider" type="submit" value="Valider" />
		</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>