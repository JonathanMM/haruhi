<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('avatar');

$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
$user = $requete->fetch();
if(!is_null($user['avatar']))
	unlink('images/avatar/'.$user['avatar']);

$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET avatar = NULL WHERE id = '.$_SESSION['id_adherent']);
header('location: modif_avatar.php');
exit();
?>
