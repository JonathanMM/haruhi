<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('intervenants');
verifierSiUtilisateurAPermission(PAGE_INTERVENANT);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Intervenant</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Intervenant</h2>

	<p>Liste des enfants :</p>
	<ul>
	<?php $requete = mysql_query('SELECT * FROM '.$bdd_prefixe.'enfants WHERE id_intervenant = '.$_SESSION['id_adherent']);
		$i = 0;
		if(!($requete === false))
		{
			while($donnees = mysql_fetch_array($requete))
			{
				echo '<ol>'. $donnees['prenom'].' '.$donnees['nom'].' - <a href="poster_dossier.php?id='.$donnees['id'].'">Poster un dossier</a></ol>';
				$i++;
			}
		}
		if($i == 0)
			echo '<ol>Pas d\'enfant à votre charge</ol>';
		?>
	</ul>

	<?php include('bas_page.php'); ?>
	</body>
</html>