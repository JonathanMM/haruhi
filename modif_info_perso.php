<?php
session_start();
require_once('prelude_page.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$pseudo = htmlspecialchars($_POST['pseudo']);
	$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
	$courriel = htmlspecialchars($_POST['courriel'], ENT_QUOTES);

	//On va gérer l'adresse
	$adresse['ligne1'] = htmlspecialchars($_POST['adresse_ligne1'], ENT_QUOTES);
	$adresse['ligne2'] = htmlspecialchars($_POST['adresse_ligne2'], ENT_QUOTES);
	$adresse['cp'] = htmlspecialchars($_POST['adresse_cp'], ENT_QUOTES);
	$adresse['ville'] = htmlspecialchars($_POST['adresse_ville'], ENT_QUOTES);
	$adresse['pays'] = htmlspecialchars($_POST['adresse_pays'], ENT_QUOTES);

	if(strlen($adresse['ligne1']) > 0 || strlen($adresse['ligne2']) > 0 || strlen($adresse['cp']) > 0 || strlen($adresse['ville']) > 0 || strlen($adresse['pays']) > 0)
		$adresses = array($adresse);
	else
		$adresses = array();

	$adresse_fixe = stringifyAdresse($adresses);

	if(strlen($prenom) > 0 && strlen($nom) > 0)
	{
		//On récupère les anciennes valeurs de l'adhérent
		$user_requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
		$anciennesInfoAdherent = $user_requete->fetch();

		$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET prenom = "'.$prenom.'", nom = "'.$nom.'", pseudo = "'.$pseudo.'", adresse = "'.$adresse_fixe.'", telephone = "'.$telephone.'", courriel = "'.$courriel.'" WHERE id = '.$_SESSION['id_adherent']);
		
		//Puis les nouvelles
		$user_requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
		$infoAdherent = $user_requete->fetch();

		//Et on appelle toutes les implémentations
		$classes = importerEvent('adherent');
		foreach($classes as $classe)
		{
			$classe::modifier($_SESSION['id_adherent'], $infoAdherent, $anciennesInfoAdherent);
		}

		ajouterSuccesNotification('Vos informations personnelles ont été mises à jour avec succès.');
	} else
		ajouterErreurNotification('Les noms et prénoms sont obligatoires.');
	header('location: mon_compte.php');
	exit();
}

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
$user = $requete->fetch();
$mode = MODE_MODIF;

//Et on va traiter l'adresse
$adresse = parseAdresse($user['adresse']);
//Mode de compatibilité
if(count($adresse) == 0 && strlen($user['adresse']) > 0)
{
	$adresse = array(array(
		'ligne1' => $user['adresse'],
		'ligne2' => '',
		'cp' => '',
		'ville' => '',
		'pays' => ''
	));
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de ses données personnelles</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Modifier ses informations</h2>

		<form action="modif_info_perso.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="prenom">Prénom</label></div>
					<div class="cellule"><input name="prenom" <?php echo 'value="'.$user['prenom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom</label></div>
					<div class="cellule"><input id="champ_nom" name="nom" <?php echo 'value="'.$user['nom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="pseudo">Pseudo</label></div>
					<div class="cellule"><input id="champ_pseudo" name="pseudo" <?php echo 'value="'.$user['pseudo'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="adresse">Adresse</label></div>
					<div class="cellule">
						<input name="adresse_ligne1" placeholder="Ligne 1" <?php if($mode == MODE_MODIF && count($adresse) > 0) echo 'value="'.$adresse[0]['ligne1'].'" '; ?>/><br />
						<input name="adresse_ligne2" placeholder="Ligne 2" <?php if($mode == MODE_MODIF && count($adresse) > 0) echo 'value="'.$adresse[0]['ligne2'].'" '; ?>/><br />
						<input name="adresse_cp" placeholder="Code Postal" <?php if($mode == MODE_MODIF && count($adresse) > 0) echo 'value="'.$adresse[0]['cp'].'" '; ?>/><br />
						<input name="adresse_ville" placeholder="Ville" <?php if($mode == MODE_MODIF && count($adresse) > 0) echo 'value="'.$adresse[0]['ville'].'" '; ?>/><br />
						<input name="adresse_pays" placeholder="Pays" <?php if($mode == MODE_MODIF && count($adresse) > 0) echo 'value="'.$adresse[0]['pays'].'" '; ?>/>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="telephone">Téléphone</label></div>
					<div class="cellule"><input name="telephone" <?php echo 'value="'.$user['telephone'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="courriel">Courriel</label></div>
					<div class="cellule"><input name="courriel" <?php echo 'value="'.$user['courriel'].'" '; ?>/></div>
				</div>
			</div>

			<p>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
