<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('se');
verifierSiUtilisateurAPermission(GERER_ML);
verifierSiIdInGet('liste_ml.php');

$idML = intval($_GET['id']);
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml WHERE id = '.$idML);
if(!($requete === false))
{
    $listeML = $requete->fetch();
    if($listeML === false)
    {
        ajouterErreurNotification('La liste de diffusion demandé n\'existe pas');
		header('location: liste_ml.php');
		exit();
    }
}

importerEvent('systemeExterne');

$carnetAdresse = SystemeExterneHaruhi::getDisplayableSubscriberOfML($idML);
$labelCategoriesCarnetAdresse = array(
	'externes' => 'Externes',
	'adherents' => 'Adherents',
	'titres' => 'Titres'
);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Liste des abonnées d'une mailing list</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Abonnés d'une mailing list</h2>
	<?php afficherNotification(); ?>
    <p>Liste : <?php echo $listeML['nom']; ?></p>
	<?php
	foreach($carnetAdresse as $categorieNom => $listeCourriels)
	{
		echo '<h3>'.$labelCategoriesCarnetAdresse[$categorieNom].'</h3>';
		echo '<ul>';
		if(count($listeCourriels) == 0)
			echo '<li><i>Aucun</i></li>';
		else
		{
			foreach($listeCourriels as $courrielInfo)
			{
				echo '<li>'.$courrielInfo['label'].'</li>';
			}
		}
		echo '</ul>';
	}
	?>
	<p>
		<a href="ajout_abonnes_ml.php?id=<?php echo $idML; ?>">Ajouter un abonné</a> - 
		<a href="pull_ml.php?id=<?php echo $idML; ?>">Synchroniser depuis la source distante</a> - 
		<a href="push_ml.php?id=<?php echo $idML; ?>">Synchroniser vers la source distante</a>
	</p>
	<?php include('bas_page.php'); ?>
	</body>
</html>