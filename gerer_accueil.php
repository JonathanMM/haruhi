<?php
session_start();
require_once 'prelude_page.php';
verifierSiUtilisateurAPermission(GERER_ACCUEIL);

use Accueil\AccueilModule;
use Accueil\AccueilProvider;
use Accueil\AccueilSkeleton;

$provider = new AccueilProvider($bdd);
$accueil = $provider->getAccueil();

if (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    $config = array(
        'type' => AccueilSkeleton::MESSAGE,
        'valeur' => str_replace(array("\r\n", "\n", "\r"), array('', '', ''), $_POST['message']),
    );
    $module = new AccueilModule($config);

    $accueil->setModule($module);
    $provider->save();

    ajouterSuccesNotification("Configuration de la page d'accueil enregistrée");
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Gérer la page d'accueil</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include 'haut_page.php';

afficherNotification();

?>
    <h2>Gérer la page d'accueil</h2>

	<form action="gerer_accueil.php" method="post">
		<div class="formulaire">
			<div class="ligne">
				<div class="cellule intitule"><label name="message">Message d'accueil : </label></div>
				<div class="cellule"><textarea name="message"><?php
if (!is_null($accueil->getModule(AccueilSkeleton::MESSAGE))) {
    echo str_replace("<br />", "\n", $accueil->getModule(AccueilSkeleton::MESSAGE)->getValeur());
}
?></textarea></div>
			</div>
		</div>
		<p>
			<input type="hidden" name="envoi" value="1" />
			<input id="bouton_valider" type="submit" value="Valider" />
		</p>
	</form>

	<?php include 'bas_page.php';?>
	</body>
</html>