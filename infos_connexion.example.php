<?php

$host = "localhost";
$username = "root";
$password = "";
$bdd_name = "haruhi";
$bdd_prefixe = "haruhi_";

//OVH
$applicationKey_ovh = '';
$applicationSecret_ovh = '';
$endpoint_ovh = '';
$consumerKey_ovh = '';

$domaineEmail_ovh = '';

//Yuki
$serveur_yuki = "";
$courriel_yuki = "";
$mdp_yuki = "";

//Mikuru
$adresse_mikuru = "";

//Questions tutorats :
$questions_tutorat = array();

//XenForo
$fileDir_xenforo = '';

//HelloAsso
$cle_helloasso = '';
$mdp_helloasso = '';

//Fonctionnalités
$fonctionnalites_statut = [
    "se" => false,
    "ovh" => false,
    "xenforo" => false,
    "yuki" => false,
    "mikuru" => false,
    "intervenants" => false,
    "factures" => false,
    "stockage" => false,
    "calendrier" => false,
    "avatar" => false,
    "cartemembre" => false,
    "nolife" => false,
    "helloasso" => false,
    "tresorerie" => false,
    "oauth" => false,
];

//Infos diverses
$infos_siteasso = "";
$infos_nomasso = "";
$infos_adresseasso = "";
$infos_adressemail = "";

//Configuration envoi des mails
$envoyerMails = true;
