<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('avatar');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$fichier = $_FILES['avatar'];
	if(!in_array($fichier['type'], array('image/png', 'image/jpeg', 'image/gif')))
	{
		echo 'Le fichier envoyé n\'est pas une image :(';
	} else {
		//On récupère l'extension
		$explode_nom = explode('.', $fichier['name']);
		//On génère son nouveau nom
		$nom_fichier = genererCodeAleatoire(16).'.'.$explode_nom[count($explode_nom) - 1];
		move_uploaded_file($fichier['tmp_name'], 'images/avatar/'.$nom_fichier);
		//On récupère l'adresse de l'ancien pour le supprimer
		$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
		$user = $requete->fetch();
		if(!is_null($user['avatar']))
			unlink('images/avatar/'.$user['avatar']);

		$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET avatar = "'.$nom_fichier.'" WHERE id = '.$_SESSION['id_adherent']);
		header('location: mon_compte.php');
		exit();
	}
} else if(isset($_POST['envoi']) && $_POST['envoi'] == 2)
{
	$id_membre = intval($_POST['id_nolife']);
	if($id_membre == 0 || !$fonctionnalites_statut['nolife'])
	{
		header('location: mon_compte.php');
		exit();
	}

	$contenu = file_get_contents('http://forum.nolife-tv.com/member.php?u='.$id_membre);
	if(!preg_match('#customavatars\/avatar'.$id_membre.'_([0-9]*).gif#Uis', $contenu, $resultats))
	{
		echo 'Avatar non trouvé :(';
	} else {
		$adresse_avatar = 'http://forum.nolife-tv.com/'.$resultats[0];
		$nom_fichier = genererCodeAleatoire(16).'.gif'; //On génère un nouveau nom
		$fichier = fopen('images/avatar/'.$nom_fichier, 'wb');
		fwrite($fichier, file_get_contents($adresse_avatar));
		fclose($fichier);
		//On récupère l'adresse de l'ancien pour le supprimer
		$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
		$user = $requete->fetch();
		if(!is_null($user['avatar']))
			unlink('images/avatar/'.$user['avatar']);

		$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET avatar = "'.$nom_fichier.'" WHERE id = '.$_SESSION['id_adherent']);
		header('location: mon_compte.php');
		exit();
	}
}

$requete = $pdo->query('SELECT avatar FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id_adherent']);
$user = $requete->fetch();
$mode = MODE_MODIF;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de son avatar</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Modifier son avatar</h2>

		<p>
			Avatar actuel :<br />
			<?php
			if(is_null($user['avatar']))
				echo 'Aucun avatar';
			else
			{
				echo '<img src="images/avatar/'.$user['avatar'].'" alt="Avatar" /><br />';
				echo '<a href="supp_avatar.php">Supprimer</a>';
			}
			?>
		</p>

		<form action="modif_avatar.php" method="post" enctype="multipart/form-data">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="avatar">Nouvel avatar : </label></div>
					<div class="cellule"><input name="avatar" type="file" /></div>
				</div>
			</div>

			<p>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php
		if($fonctionnalites_statut['nolife'])
		{
		?>
		<h2>Récupérer l'avatar depuis le forum de Nolife</h2>
		<form action="modif_avatar.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="id_nolife">Id Membre : </label></div>
					<div class="cellule"><input name="id_nolife" type="number" /></div>
				</div>
			</div>

			<p>
				<input type="hidden" name="envoi" value="2" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>
		<?php
		}
		?>

		<?php include('bas_page.php'); ?>
	</body>
</html>
