<?php
require_once(__DIR__.'/vendor/autoload.php');
use Jsvrcek\ICS\Model\Calendar;
use Jsvrcek\ICS\Model\CalendarEvent;
use Jsvrcek\ICS\Model\Relationship\Attendee;
use Jsvrcek\ICS\Model\Relationship\Organizer;
use Jsvrcek\ICS\Model\Description\Location;

use Jsvrcek\ICS\Utility\Formatter;
use Jsvrcek\ICS\CalendarStream;
use Jsvrcek\ICS\CalendarExport;
session_start();
require_once('prelude_page.php');
verifierSiIdInGet('calendrier.php');

$id = intval($_GET['id']);

$requete = $pdo->query('SELECT *, 
c.id AS idEvenement, c.titre AS nomEvenement, cat.nom AS nomCategorie
FROM '.$bdd_prefixe.'calendrier c
INNER JOIN '.$bdd_prefixe.'calendrier_categories cat ON cat.id = c.type
WHERE c.id = '.$id);
$evenement = $requete->fetch();
if($evenement === false)
{
    ajouterErreurNotification('Vous n\'avez pas le droit de consulter cet événement.');
    header('location: calendrier.php');
    exit();
}

$ics = new CalendarEvent();
$ics->setStart(new \DateTime($evenement['date'], new \DateTimeZone("Europe/Paris")))
    ->setSummary(htmlspecialchars_decode($evenement['titre'], ENT_QUOTES))
    ->setDescription(htmlspecialchars_decode($evenement['description'], ENT_QUOTES))
	->setUid($evenement['uuid']);
	
//Gestion des participants
//On récupère les participants
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier_participants p
INNER JOIN '.$bdd_prefixe.'adherents a ON a.id = p.idMembre
WHERE p.idEvenement = '.$id);
$participantsSql = $requete->fetchAll();
foreach($participantsSql as $participant)
{
    $nomParticipant = $participant['prenom'].' '.$participant['nom'];
    if($participant['estOrganisateur'] == 1)
    {
        $organisateur = new Organizer(new Formatter());
        $organisateur->setValue($participant['courriel'])
            ->setName($nomParticipant);
        $ics->setOrganizer($organisateur);
    }
    else
    {
        $participantIcs = new Attendee(new Formatter());
        $participantIcs->setValue($participant['courriel'])
            ->setName($nomParticipant);
        switch($participant['reponse'])
        {
            case CALENDRIER_REPONSE_ACCEPTE:
                $participantIcs->setParticipationStatus('ACCEPTED');
                break;
            case CALENDRIER_REPONSE_PROVISOIRE:
                $participantIcs->setParticipationStatus('TENTATIVE');
                break;
            case CALENDRIER_REPONSE_REFUS:
                $participantIcs->setParticipationStatus('DECLINED');
                break;
            case CALENDRIER_REPONSE_NON_REPONDU:
                $participantIcs->setParticipationStatus('NEEDS-ACTION');
                break;
        }
        $ics->addAttendee($participantIcs);
    }
}

//Lieu
$lieu = new Location();
$lieu->setName($evenement['lieu']);
$ics->addLocation($lieu);

//setup calendar
$calendar = new Calendar();
$calendar->setProdId('-//Nocle//Haruhi//EN')
	->addEvent($ics);

//setup exporter
$calendarExport = new CalendarExport(new CalendarStream, new Formatter());
$calendarExport->addCalendar($calendar);

header('Content-Type: text/calendar');
//output .ics formatted text
echo $calendarExport->getStream();
?>