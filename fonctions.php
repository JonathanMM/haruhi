<?php
//Inclusion des bibliothèques
include_once 'lib/internal/facture.php';
include_once 'lib/internal/date.php';
include_once 'lib/internal/adherent.php';
include_once 'lib/internal/mail.php';
include_once 'lib/internal/notification.php';
include_once 'lib/internal/systemeExterne.php';
include_once 'lib/internal/cotisation.php';
include_once 'lib/internal/helloasso.php';
require_once 'lib/internal/sauvegarderBDD.php';
require_once 'lib/internal/adresse.php';
require_once 'lib/internal/calendrier.php';
require_once 'lib/internal/tresorerie.php';
require_once 'lib/internal/connexionAuto.php';

set_exception_handler(
    function ($ex) {
        global $debug_message_erreur;
        $logger = Logger::getLogger('erreur');
        ob_clean();
        echo '<div style="width: 100%; border: 1px solid red;">';
        echo 'Une erreur fatale empêche le bon chargement de la page. Merci de contacter un administrateur au plus vite.<br />';
        if (isset($debug_message_erreur) && $debug_message_erreur) {
            echo 'Message d\'erreur : ' . $ex->getMessage() . '<br />';
            echo 'Fichier : ' . $ex->getFile() . '<br />';
            echo 'Ligne : ' . $ex->getLine() . '<br />';
            echo 'Pile d\'appel :<br />';
            echo $ex->getTraceAsString();
        }
        if ($logger->isErrorEnabled()) {
            $logger->error('Erreur non géré : ' . $ex->getMessage() . ' (' . $ex->getFile() . ':' . $ex->getLine() . ')' . "\n" . $ex->getTraceAsString());
        }

    }
);

function signature($date)
{
    return 'Président de l\'association';
}

function genererCodeAleatoire($longueur = 8)
{
    $list = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    $code = '';
    for ($i = 0; $i < $longueur; $i++) {
        $code .= $list[rand(0, strlen($list) - 1)];
    }

    return $code;
}

function nettoyer($label)
{
    if ($label[0] == "\"") {
        return substr($label[0], 1, -1);
    } else {
        return $label;
    }

}

function nettoyerAccent($texte)
{
    return strtr($texte, 'àâéèêîôùû', 'aaeeeiouu');
}

function importerEvent($event)
{
    //On commence par inclure tout ce qui est dans le dossier de l'event
    $dossier = __DIR__ . '/event/' . $event . '/';
    $liste_classes = array();
    $interface = '';
    foreach (scandir($dossier) as $fichier) {
        if (!is_dir($dossier . $fichier)) //C'est bien un fichier
        {
            $explode = explode('.', $fichier);
            $n = count($explode);
            if ($explode[$n - 2] == 'class') //C'est une classe
            {
                $liste_classes[] = implode('.', array_slice($explode, 0, $n - 2));
            } elseif ($explode[$n - 2] == 'interface') //C'est l'interface !
            {
                $interface = $fichier;
            }

        }
    }
    //On inclut d'abord l'interface
    include_once $dossier . $interface;
    //Puis les classes
    foreach ($liste_classes as $classe) {
        include_once $dossier . $classe . '.class.php';
    }

    return $liste_classes;
}

function getImplementations($listeClasses)
{
    $implementations = array();
    foreach ($listeClasses as $classe) {
        if ($classe::estActif()) {
            $implementations[$classe] = new $classe;
        }

    }
    return $implementations;
}

function verifierSiIdInGet($location = 'index.php')
{
    global $_GET;
    if (!(isset($_GET['id'])) || intval($_GET['id']) <= 0) {
        header('location: ' . $location);
        exit();
    }
}

function verifierSiFonctionnaliteEstActive($fonctionnaliteNecessaire)
{
    if (!testerFonctionnalite($fonctionnaliteNecessaire)) {
        header('location: index.php');
        exit();
    }
}

function testerFonctionnalite($fonctionnalite)
{
    global $fonctionnalites_statut;
    return isset($fonctionnalites_statut[$fonctionnalite]) && $fonctionnalites_statut[$fonctionnalite];
}

function verifierSiUtilisateurAPermission($permissionNecessaire)
{
    global $_SESSION;
    if (!testerPermission($permissionNecessaire)) {
        header('location: index.php');
        exit();
    }
}

function testerPermission($permission)
{
    return testerPermissionDansPermissions($permission, (int) $_SESSION['permission']);
}

function testerPermissionDansPermissions(int $permission, int $permissions)
{
    return ($permissions & $permission);
}

function formaterMontant($montant)
{
    return number_format($montant, 2, ',', ' ') . ' €';
}

function formaterUrl(string $base, string $page)
{
    if (strlen($base) == 0 || strlen($page) == 0) {
        return '';
    }

    return $base . (substr($base, -1) == '/' ? '' : '/') . $page;
}

function getCurrentUrl()
{
    return sprintf(
        "%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        $_SERVER['REQUEST_URI']
    );
}

define('MODE_AJOUT', 1);
define('MODE_MODIF', 2);
define('MODE_SUPPR', 3);
