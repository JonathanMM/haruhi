<?php
// Récupération du serveur et utilisation du système magique de token
require_once __DIR__ . '/server.php';
$server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
