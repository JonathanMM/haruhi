<?php
/**
 * Autoriser une application
 * Ce fichier sert à l'utilisateur pour se connecter avec son compte Haruhi
 * Et pour les systèmes à obtenir les différents codes nécessaires
 */
require_once __DIR__ . '/server.php';

$request = OAuth2\Request::createFromGlobals();
$response = new OAuth2\Response();

// Ne fonctionne que pour les clients autorisés
if (!$server->validateAuthorizeRequest($request, $response)) {
    $response->send();
    die;
}

if (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    $pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
    $mdp = hash('sha512', $_POST['mdp']);
    $requete = $pdo->query('SELECT *, m.id AS id, a.id, t.permission AS id_adherent FROM ' . $bdd_prefixe . 'membres m
				LEFT JOIN ' . $bdd_prefixe . 'adherents a ON a.id_membre = m.id
				LEFT JOIN ' . $bdd_prefixe . 'titre t ON t.id = a.titre
                WHERE m.pseudo = "' . $pseudo . '" AND m.mdp = "' . $mdp . '"');
    if (!($requete === false) && $donneesAdherent = $requete->fetch()) {
        //On regarde si l'adhérent peut se connecter comme cela
        if (testerPermissionDansPermissions(CONNEXION_OAUTH, $donneesAdherent['permission'])) {
            //On va gérer l'option Se souvenir de moi
            if (isset($_POST['souvenir']) && $_POST['souvenir'] == 1) {
                // On va créer un token que l'on stocke dans un cookie pour la prochaine fois
                genererCookieAutoLogin($donneesAdherent['id']);
            }
            $formulaireConnexion = false;
        } else {
            $erreur = "Vous ne pouvez pas vous connecter ici";
            $formulaireConnexion = true;
        }
    } else {
        $erreur = "Identifiant et/ou mot de passe incorrect(s).";
        $formulaireConnexion = true;
    }
} elseif (autoLogged()) {
    // Connexion automatique
    $formulaireConnexion = false;
    $donneesAdherent = array('id' => $_SESSION['id']);
} else {
    $formulaireConnexion = true;
}

// Formulaire de connexion
if ($formulaireConnexion) {
    if (isset($_GET['client_id'])) {
        $clientId = htmlspecialchars($_GET['client_id']);
        $requete = $bdd->query('SELECT nom FROM ' . $bdd->getNomTable('oauth_clients') . ' WHERE client_id = "' . $clientId . '"');
        $client = $requete->fetch();
        $titreFormulaireConnexion = $client['nom'];
    } else {
        $titreFormulaireConnexion = 'Site externe';
    }

    $pathFile = '../../';
    include '../../includes/html/connexion.php';
    exit();
}

//Pour l'instant, si on arrive à se connecter, c'est qu'on est autorisé
$estAutorise = true;
//$clientId = $request->query('client_id'); //Utile pour savoir si on doit restreindre les autorisations d'accès ou pas.
$idMembre = $donneesAdherent['id'];
$server->handleAuthorizeRequest($request, $response, $estAutorise, $idMembre);
//En cas d'echec, l'url est : error=access_denied&error_description=The+user+denied+access+to+your+application&state=xyz
$response->send();
