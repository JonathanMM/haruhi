<?php
session_start();
require __DIR__ . '/../../vendor/autoload.php';

require __DIR__ . '/../../configuration.php';
verifierSiFonctionnaliteEstActive('oauth');

// On change la config des tables pour ajouter le prefixe de la BDD
$config_storage = array(
    'client_table' => $bdd_prefixe . 'oauth_clients',
    'access_token_table' => $bdd_prefixe . 'oauth_access_tokens',
    'refresh_token_table' => $bdd_prefixe . 'oauth_refresh_tokens',
    'code_table' => $bdd_prefixe . 'oauth_authorization_codes',
);

//On va utiliser notre connexion PDO
$storage = new OAuth2\Storage\Pdo($pdo, $config_storage);

$server = new OAuth2\Server($storage);

// Refresh Token avec ajout à chaque fois dans les réponses
$server->addGrantType(new OAuth2\GrantType\RefreshToken($storage, array(
    'always_issue_new_refresh_token' => true,
)));

// Client Credentials
$server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

// Authorization Code
$server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));

// Scope
$defaultScope = 'basic';
$supportedScopes = array(
    'basic',
);
$memory = new OAuth2\Storage\Memory(array(
    'default_scope' => $defaultScope,
    'supported_scopes' => $supportedScopes,
));
$scopeUtil = new OAuth2\Scope($memory);

$server->setScopeUtil($scopeUtil);
