<?php
/**
 * API OAuth
 * Pour l'instant, ça ne fait que renvoyer les infos de l'adhérent
 */
require_once __DIR__ . '/server.php';

// Handle a request to a resource and authenticate the access token
if (!$server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
    $server->getResponse()->send();
    die;
}

$token = $server->getAccessTokenData(OAuth2\Request::createFromGlobals());
$idMembre = intval($token['user_id']);

$requete = $bdd->query('SELECT *, m.id AS id, a.id AS id_adherent, m.pseudo AS pseudo_membre
FROM ' . $bdd_prefixe . 'membres m
LEFT JOIN ' . $bdd_prefixe . 'adherents a ON a.id_membre = m.id
WHERE m.id = ' . $idMembre);
$donneesAdherent = $requete->fetch();

echo json_encode(array(
    'user_id' => $donneesAdherent['id'],
    'displayName' => $donneesAdherent['prenom'] . ' ' . $donneesAdherent['nom'],
    'lastName' => $donneesAdherent['nom'],
    'firstName' => $donneesAdherent['prenom'],
    'language' => 'fr',
    'email' => $donneesAdherent['courriel'],
    'id' => $donneesAdherent['id'],
    'name' => $donneesAdherent['prenom'] . ' ' . $donneesAdherent['nom'],
));
