<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('factures');
verifierSiUtilisateurAPermission(FAIRE_FACTURE);

//Debug Mode
$debug = (isset($_GET['debug']) && $_GET['debug'] == 'Suzumiya') || (isset($_POST['debug']) && $_POST['debug'] == 'Suzumiya');

ini_set('mbstring.substitute_character', "none");

function obtenirValeur($cellules, $case, $titre, $concat = false, $adherent = null)
{
	global $typesFacture, $debug;
	if(isset($titre[$case]) && count($titre[$case]) > 0)
	{
		if($concat)
		{
			$c = array();
			foreach($titre[$case] as $i)
				$c[] = $cellules[$i];
			$contenu = implode(' ', $c);
		}
		else
			$contenu = $cellules[$titre[$case][0]];

		//$contenu = mb_convert_encoding($contenu, 'UTF-8', 'UTF-16');
		if($case == 'type')
		{
			if($debug)
			{
				echo 'Type de facture cherché : ';
				echo "<br />\n";
				if(isset($typesFacture[$contenu]))
					print_r($typesFacture[$contenu]);
				else
					echo "Type de facture non trouvé";
				echo "<br />\n";
			}
			if(isset($typesFacture[$contenu]))
				return $typesFacture[$contenu];
			else
				return null;
		}

		if(in_array($case, array('date', 'date_inscription', 'date_fin_cotisation')))
		{
			$coupe = explode(' ', $contenu);
			$date_coupe = explode('/', $coupe[0]);

			return $date_coupe[2].'-'.$date_coupe[1].'-'.$date_coupe[0];
		}

		//Si c'est une somme, alors on converti les virgules en point
		if($case == 'somme')
			$contenu = str_replace(',', '.', $contenu);
		
		return $contenu;
	}
	else if(isset($_POST['default_'.$case]))
	{
		if($debug)
			echo 'Utilisation de la valeur par défaut pour : '.$case."<br />\n";

		$default = $_POST['default_'.$case];

		//Les types de factures, c'est à part
		if($case == 'type')
		{
			//On a l'id de la facture, on essaye de la retrouver dans le lot
			$idFacture = $default;
			if($debug)
				echo 'Id facture par défaut à trouver : '.$idFacture."<br />\n";
			foreach($typesFacture as $typeFacture)
			{
				if($typeFacture['id'] == $idFacture)
				{
					if($debug)
					{
						echo 'Facture par défaut : ';
						echo "<br />\n";
						print_r($typeFacture);
						echo "<br />\n";
					}
					return $typeFacture;
				}
			}

			//Le type de facture par défaut n'existe pas
			if($debug)
				echo 'Facture par défaut non trouvée.'."<br />\n";
			return null;
		}

		if($default == '__aujourdhui')
			return date('Y-m-d');
		elseif($default == '__date_facturation')
			return obtenirValeur($cellules, "date", $titre);
		elseif($default == '__auto')
		{
			//Là, c'est en fonction de la case qu'on a
			if($case == 'date_inscription')
			{
				//Il faut regarder si on est sur une cotisation ou pas	
				$typeFacture = obtenirValeur($cellules, 'type', $titre);
				if(is_null($typeFacture) || $typeFacture['cotisation'] == 0) //Pas une cotisation, on renvoi la valeur 0000-00-00
					return '0000-00-00';
				return obtenirValeur($cellules, "date", $titre);
			}
			else if($case == 'date_fin_cotisation')
			{
				$date = obtenirValeur($cellules, "date", $titre);
				//On doit aussi récupérer le type de facture
				$typeFacture = obtenirValeur($cellules, 'type', $titre);
				$calcul = analyserTypeFacture($typeFacture, $date, $adherent);
				if($calcul['cotisation'])
					return $calcul['fin'];
				else
				{
					if(!is_null($adherent) && isset($adherent['date_fin_cotisation']))
						return $adherent['date_fin_cotisation'];
					return null;
				}
			}
			else if($case == 'date')
				return date('Y-m-d');
		}
		return $default;
	}
	else
		return null;
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$mode = MODE_AJOUT;
	$fichier = fopen($_FILES['fichier']['tmp_name'], 'r');
	$separateur = ';';
	$i = 0;
	$titres =
		[
		"nom" => [],
		"prenom" => [],
		"pseudo" => [],
		"adresse_ligne1" => [],
		"adresse_ligne2" => [],
		"adresse_cp" => [],
		"adresse_ville" => [],
		"adresse_pays" => [],
		"telephone" => [],
		"courriel" => [],
		"date_inscription" => [],
		"date_fin_cotisation" => [],
		"commentaire" => [],
		"somme" => [],
		"date" => [],
		"payement" => [],
		"lien" => [],
		"type" => [],
		"titre" => []
	];

	//On stocke le type de facture
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type');
	if(!($requete === false))
	{
		$types = $requete->fetchAll();
		$externe = [];
		$interne = [];
		$typesFacture = array();
		foreach($types as $type)
		{
			if($type['externe'] == 1)
				$externe[] = $type['id'];
			else
				$interne[] = $type['id'];
			$typesFacture[$type['label']] = $type;
		}
	}
	if($debug)
	{
		echo 'Types de factures dans la base : '."<br />\n";
		print_r($typesFacture);
	}

	//On prépare nos requêtes
	$requeteFactureExterne = $pdo->prepare('SELECT id FROM '.$bdd_prefixe.'factures WHERE lien = ?');
	$requeteAdherent = $pdo->prepare('SELECT * FROM '.$bdd_prefixe.'adherents WHERE nom = ? AND prenom = ?');
	$liensAjoutes = array();

	$lignes = array();
	while(!feof($fichier))
	{
		$cellules = fgetcsv($fichier, 0, $separateur);
		if($i == 0) //Entête
		{
			foreach($cellules as $j => $l)
			{
				$label = nettoyer($l);
				if(isset($_POST['titre_'.$j]) && $_POST['titre_'.$j] != '_null')
					$titres[$_POST['titre_'.$j]][] = $j;
			}
		} elseif(count($cellules) > 1) //Les données
		{
			//On va stocker, pour pouvoir le traiter dans le sens inverse
			array_unshift($lignes, $cellules);
		}
		$i++;
	}

	if($debug)
	{
		echo 'Valeur de $_POST : '."<br />\n";
		print_r($_POST);
		echo "<br />\n";
	}

	$erreurs = [];

	foreach($lignes as $numLigne => $cellules)
	{
		//Chaque ligne correspond à une facture, on fait donc les données
		//Table facture :  id 	adherent 	somme 	date 	payement 	lien 	type	visibilite
		$facture = array(
			'adherent' => 0,
			'somme' => obtenirValeur($cellules, 'somme', $titres),
			'date' => obtenirValeur($cellules, 'date', $titres),
			'payement' => obtenirValeur($cellules, 'payement', $titres),
			'lien' => obtenirValeur($cellules, 'lien', $titres),
			'type' => obtenirValeur($cellules, 'type', $titres),
			'visibilite' => 1 //Fixé à toujours visible
		);

		if($debug)
		{
			echo "Facture : <br />\n";
			print_r($facture);
			echo "<br />\n";
		}

		if($facture['type'] == null) //Type non trouvé => erreur
		{
			if($debug)
				echo 'Aucun type correspondant'."<br />\n";
			$erreurs[] = 'Ligne '.($numLigne+1).' : Le type de facture « '.$cellules[$titres['type'][0]].' » n\'a pas été trouvé dans le système.';
			continue;
		} else { //On met l'id
			$facture['type'] = $facture['type']['id'];
		}

		//On regarde le type de facture
		if(in_array($facture['type'], $interne)) //Facture interne
		{
			//On vide le lien
			$facture['lien'] = null;
		} else //Externe
		{
			//Si on a déjà ajouté une facture avec ce lien dans ce fichier, on l'accepte (si don et cotisation ont même attestation par exemple)
			if(!in_array($facture['lien'], $liensAjoutes))
			{
				//On regarde si on a déjà une facture du genre
				$requeteFactureExterne->execute(array($facture['lien']));
				$factureBDD = $requeteFactureExterne->fetch();
				if(!($factureBDD === false)) //On a déjà quelque chose
				{
					if($debug)
						echo "Facture déjà dans la base"."<br />\n";
					continue; //On passe à l'élément suivant
				}
			}
		}
		//On continue, va falloir trouver l'adhérent
		//Table adhérent : id 	prenom 	nom 	pseudo 	adresse 	telephone 	courriel 	id_membre 	titre 	date_inscription 	date_fin_cotisation 	commentaire
		$adherent = array(
			'nom' => obtenirValeur($cellules, 'nom', $titres),
			'prenom' => obtenirValeur($cellules, 'prenom', $titres)
		);
		$requeteAdherent->execute(array($adherent['nom'], $adherent['prenom']));
		$adherentBDD = $requeteAdherent->fetch();
		if(!($adherentBDD === false)) ///L'adhérent existe déjà :) //TODO
		{
			if($debug)
				echo "Adhérent déjà existant "."<br />\n";
			$facture['adherent'] = $adherentBDD['id'];
			$adherent['adresse'] = $adherentBDD['adresse'];

			//On regarde s'il était inscrit avant
			if($adherentBDD['date_inscription'] == "0000-00-00")
			{
				$adherent['date_inscription'] = obtenirValeur($cellules, 'date_inscription', $titres);
				//On regarde aussi s'il avait un numéro
				if($adherentBDD['numero'] == 0)
				{
					$adherent['numero'] = obtenirValeur($cellules, 'numero', $titres);
					//Si c'est encore à zéro, on génère un nouveau
					if($adherent['numero'] == 0)
						$adherent['numero'] = getNouveauNumeroAdherent();
				} else 
					$adherent['numero'] = $adherentBDD['numero'];
			}
			else
			{
				$adherent['date_inscription'] = $adherentBDD['date_inscription'];
				$adherent['numero'] = $adherentBDD['numero'];
			}

			$adherent['date_fin_cotisation'] = $adherentBDD['date_fin_cotisation'];
			$finCotis = obtenirValeur($cellules, 'date_fin_cotisation', $titres, false, $adherent);
			$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET date_fin_cotisation = "'.$finCotis.'", date_inscription = "'.$adherent['date_inscription'].'", numero = "'.$adherent['numero'].'" WHERE id = '.$facture['adherent']);

			//Et on appelle toutes les implémentations
			$classes = importerEvent('adherent');
			foreach($classes as $classe)
			{
				$classe::modifier($adherentBDD['id'], $adherent, $adherentBDD);
			}
		} else { //On va le crée
			if($debug)
				echo "Création de l'adhérent"."<br />\n";
			$adherent['pseudo'] = obtenirValeur($cellules, 'pseudo', $titres);
			$adherent['telephone'] = obtenirValeur($cellules, 'telephone', $titres);
			$adherent['courriel'] = obtenirValeur($cellules, 'courriel', $titres);
			$adherent['titre'] = obtenirValeur($cellules, 'titre', $titres);
			$adherent['date_inscription'] = obtenirValeur($cellules, 'date_inscription', $titres);
			$adherent['date_fin_cotisation'] = obtenirValeur($cellules, 'date_fin_cotisation', $titres);
			$adherent['commentaire'] = obtenirValeur($cellules, 'commentaire', $titres, true);

			//On gère l'adresse
			$adresses = array(array(
				'ligne1' => obtenirValeur($cellules, 'adresse_ligne1', $titres, true),
				'ligne2' => obtenirValeur($cellules, 'adresse_ligne2', $titres, true),
				'cp' => obtenirValeur($cellules, 'adresse_cp', $titres, true),
				'ville' => obtenirValeur($cellules, 'adresse_ville', $titres, true),
				'pays' => obtenirValeur($cellules, 'adresse_pays', $titres, true)
			));
			$adherent['adresse'] = stringifyAdresse($adresses);

			$facture['adherent'] = creerAdherent($adherent);
		}

		//Et donc, maintenant, y a plus qu'à ajouter la facture
		try
		{
			creerFacture($facture['type'], $facture['adherent'], $facture['somme'], $facture['date'], $facture['payement'], $facture['lien'], $adherent);
			if($debug)
				echo 'Facture ajouté dans le système'."<br />\n";
			//Et on note le lien
			if(!in_array($facture['type'], $interne))
				$liensAjoutes[] = $facture['lien'];
		} catch (Exception $exception)
		{
			$erreurs[] = 'Erreur lors de l\'import de la facture #'.$numLigne.' dans le système : '.$exception->getMessage();
			if($debug)
				echo 'Erreur lors de l\'ajout de la facture dans le système : '.$exception->getMessage().".<br />\n";
		}
	}
	fclose($fichier);
	if(!$debug)
	{
		if(count($erreurs) > 0)
			ajouterErreurNotification(implode("<br />\n", $erreurs));
		else
			ajouterSuccesNotification("Toutes les factures ont été importés avec succès.");
		header('location: factures.php');
	}
	else
		echo "Fin de traitement";
	exit();
}

$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Import d'une facture</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />

		<script type="application/javascript" src="js/import_facture.min.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Import d'une facture</h2>

		<form action="import_facture.php" method="post" enctype="multipart/form-data">
			<div class="formulaire" id="formulaire">
				<div class="ligne">
					<div class="cellule intitule" style="vertical-align: middle;"><label name="fichier">Fichier :</label></div>
					<div class="cellule">
						<input type="file" name="fichier" id="fichier_import" required />
						<span id="fichier_infos"></span>
					</div>
				</div>
			</div>
			<p>
				<input type="hidden" name="envoi" value="1" />
				<input type="hidden" name="debug" value="<?php if($debug) echo $_GET['debug']; else echo 0; ?>" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>
		<script type="application/javascript">
			var typesFacture = <?php
					$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type');
					if(!($requete === false))
					{
						$types = $requete->fetchAll();
						$toJson = [];
						foreach($types as $type)
						{
							$toJson[$type['id']] = $type['nom'];
						}
						echo json_encode($toJson);
					} else
						echo '{}';
					?>;

			var titresMembre = <?php
					$titreParDefaut = '';
					$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'titre');
					if(!($requete === false))
					{
						$types = $requete->fetchAll();
						$toJson = [];
						foreach($types as $type)
						{
							$toJson[$type['id']] = $type['nom'];
							if($type['defaut'])
								$titreParDefaut = $type['id'];
						}
						echo json_encode($toJson);
					} else
						echo '{}';
					?>;

			var titreParDefaut = "<?php echo $titreParDefaut; ?>";
		</script>
		<?php include('bas_page.php'); ?>
	</body>
</html>
