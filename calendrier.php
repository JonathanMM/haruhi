<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('calendrier');
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Calendrier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/calendrier.min.js"></script>
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
		<h2>Calendrier</h2>
		<?php
			$requete = $pdo->query('SELECT c.id, c.nom FROM '.$bdd_prefixe.'calendrier_categories c
			INNER JOIN '.$bdd_prefixe.'calendrier_permissions p ON p.idCategorie = c.id
			WHERE p.idTitre = '.$_SESSION['id_titre'].' AND p.ecriture = 1');
			$categories = $requete->fetchAll();
		?>
		<?php if(count($categories) > 0) { ?>
		<p><a href="form_evenement.php">Ajouter un événement</a></p>
		<?php } ?>
		
		<p>
		<?php if(isset($_GET['annee']) && intval($_GET['annee']) > 0)
		$an = intval($_GET['annee']);
		else
		$an = date('Y') - (date('m') > 8 ? 0 : 1);
		echo '← <a href="calendrier.php?annee='.($an - 1).'">Année précédente</a><br /><a href="calendrier.php?annee='.($an + 1).'">Année suivante</a> →';
		?></p>

		<?php if((int)$_SESSION['permission'] & GERER_CATEGORIE_CALENDRIER) { ?>
		<p><a href="gerer_categorie_calendrier.php">Gérer les catégories</a></p>
		<?php } ?>
	</nav>

<script type="text/javascript">
function getArrayEvent()
{
	var array_event = new Array();
	var array_bidon;
	//Et là, c'est le moment du remplissage en php ^^'
	<?php
	if(isset($_GET['annee']) && intval($_GET['annee']) > 0)
	$date = intval($_GET['annee']) + (date('m') > 8 ? 1 : 0);
	else
	$date = 'YEAR(CURRENT_DATE() + INTERVAL 4 MONTH)';
	$requete = $pdo->query('SELECT *, c.id AS idEvenement FROM '.$bdd_prefixe.'calendrier c
	INNER JOIN '.$bdd_prefixe.'calendrier_permissions p ON p.idCategorie = c.type
	WHERE YEAR(date + INTERVAL 4 MONTH) = '.$date.' AND p.idTitre = '.$_SESSION['id_titre'].' AND p.lecture = 1
	ORDER BY date ASC'); //WHERE date >= CURRENT_DATE()
		if(!($requete === false))
		{
			$events = $requete->fetchAll();
			foreach($events as $event)
			{
				$coupeDate = explode(' ', $event['date']);
				$coupe = explode('-', $coupeDate[0]);
				echo 'array_bidon = new Array('.((intval($coupe[1]) + 3) % 12).', '.intval($coupe[2]).', '.$event['idEvenement'].', "'.$event['titre'].'");'."\n";
				echo 'array_event.push(array_bidon);'."\n";
			}
		}
	?>
	return array_event;
}
</script>
	<div id="cadre_stockage">
	<?php afficherNotification(); ?>
	<ul id="liste_calendrier">
	</ul>
<script type="text/javascript">
generer_calendrier(<?php echo (isset($_GET['annee']) && intval($_GET['annee']) > 0) ? intval($_GET['annee']) : '0'; ?>);
<?php if(!isset($_GET['annee']) || intval($_GET['annee']) == 0) { ?>
var aujourdhui = new Date();
ouvrir_mois((aujourdhui.getMonth() + 4) % 12);
<?php } else { ?>
ouvrir_mois(0);
<?php } ?>
</script>
	</div>

	<?php include('bas_page.php'); ?>
	</body>
</html>