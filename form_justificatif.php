<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
    $mode = MODE_MODIF;

    $description = htmlspecialchars($_POST['description'], ENT_QUOTES);
    $montant = floatval($_POST['montant']);

    $pdo->beginTransaction();

    $pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_justificatifs (idMouvement, typePiece, idPiece, description, montant)
            VALUES (0, '.TRESORERIE_LIEN_TYPE_FACTURE.', '.$id.', "'.$description.'", '.$montant.')');
    
    $pdo->commit();

    ajouterSuccesNotification('Lien créé avec succès.');

    header('location: factures.php');
    exit();
}

verifierSiIdInGet('tresorerie.php');

$id = intval($_GET['id']);
$mode = MODE_MODIF;

$requete = $pdo->query('SELECT *, f.id AS id_facture, f.adherent AS id_adherent FROM '.$bdd_prefixe.'factures f
WHERE f.id = '.$id);
$facture = $requete->fetch();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Création d'un lien</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Création d'un lien</h2>

		<form action="form_justificatif.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule">Facture :</div>
					<div class="cellule"><?php echo $id; ?></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="description">Description : </label></div>
					<div class="cellule"><input name="description" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="montant">Montant : </label></div>
					<div class="cellule"><input name="montant" type="number" step="0.01" min="0" value="<?php echo $facture['somme']; ?>" /></div>
				</div>
			</div>
			<p>
				<input type="hidden" name="id" value="<?php echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
