<?php
session_start();
require_once 'prelude_page.php';

if (isset($_GET['id']) && intval($_GET['id']) > 0) {
    verifierSiUtilisateurAPermission(MODIFIER_PERMISSION);
    if (!isset($_SESSION['original_permission'])) //Pas deux appels à la fois
    {
        $requete = $bdd->query('SELECT id, permission, nom FROM ' . $bdd->getNomTable('titre') . ' WHERE id = ' . intval($_GET['id']));
        $permission = $requete->fetch();
        if (!($permission === false)) {
            $_SESSION['original_permission'] = $_SESSION['permission'];
            $_SESSION['original_id_titre'] = $_SESSION['id_titre'];
            $_SESSION['permission'] = $permission['permission'];
            $_SESSION['id_titre'] = $permission['id'];
            $_SESSION['permission_nom'] = $permission['nom'];
        }
    }
} elseif (isset($_GET['action']) && $_GET['action'] == 'reset') {
    $_SESSION['permission'] = $_SESSION['original_permission'];
    $_SESSION['id_titre'] = $_SESSION['original_id_titre'];
    unset($_SESSION['original_permission']);
    unset($_SESSION['original_id_titre']);
    unset($_SESSION['permission_nom']);
}
header('location: index.php');
