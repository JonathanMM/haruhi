<?php
session_start();
require_once('prelude_page.php');

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);

	//Vérification du pseudo
	if(strlen($pseudo) == 0)
	{
		ajouterErreurNotification('Aucun pseudo n\'a été renseigné.');
		header('location: modif_pseudo.php');
		exit();
	}

	if(in_array(strtolower($pseudo), array('haruhi', 'kyon', 'mikuru', 'yuki', 'itsuki')))
	{
		ajouterErreurNotification('Ce pseudo est réservé et ne peut être utilisé.');
		header('location: modif_pseudo.php');
		exit();
	}

	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'membres WHERE pseudo = "'.$pseudo.'" AND id != '.$_SESSION['id']);
	if($requete === false || !($requete->fetch() === false))
	{
		ajouterErreurNotification('Ce pseudo est déjà utilisé et ne peut être utilisé.');
		header('location: modif_pseudo.php');
		exit();
	}

	$pdo->exec('UPDATE '.$bdd_prefixe.'membres SET pseudo = "'.$pseudo.'" WHERE id = '.$_SESSION['id']);
	//On met à jour mikuru
	//Désactivé pour le moment (logins différents)
	/*mysql_query('UPDATE user SET username = "'.$pseudo.'" WHERE username = "'.$_SESSION['pseudo'].'"');
				mysql_query('UPDATE principals SET uri = "principals/'.$pseudo.'" WHERE uri = "principals/'.$_SESSION['pseudo'].'"');
 				mysql_query('UPDATE principals SET uri = "principals/'.$pseudo.'/calendar-proxy-read" WHERE uri = "principals/'.$_SESSION['pseudo'].'/calendar-proxy-read"');
				mysql_query('UPDATE principals SET uri = "principals/'.$pseudo.'/calendar-proxy-write" WHERE uri = "principals/'.$_SESSION['pseudo'].'/calendar-proxy-read"');
				mysql_query('UPDATE calendars SET principaluri = "principals/'.$pseudo.'" WHERE principaluri = "principals/'.$_SESSION['pseudo'].'"');*/
	//Et la session
	$_SESSION['pseudo'] = $pseudo;
	ajouterSuccesNotification("Votre pseudo a été modifié avec succès.");
	header('location: mon_compte.php');
	exit();
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Modification de l'identifiant de connexion</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<?php afficherNotification(); ?>
		<h2>Modifier de l'identifiant de connexion</h2>

		<form action="modif_pseudo.php" method="post">
			<p>
				<label name="pseudo">Nouvel identifiant : <input name="pseudo" /></label><br />
				<input type="hidden" name="envoi" value="1" />
				<input type="submit" value="Valider" />
			</p>
		</form>
		<?php include('bas_page.php'); ?>
	</body>
</html>
