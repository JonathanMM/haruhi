<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(FAIRE_FACTURE);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$adherentId = intval($_POST['adherent']);
	$somme = floatval(str_replace(',', '.', $_POST['somme']));
	$date = htmlspecialchars($_POST['date'], ENT_QUOTES);
	$payement = htmlspecialchars($_POST['payement'], ENT_QUOTES);
	$typeId = intval($_POST['type']);
	$lienExterne = htmlspecialchars($_POST['lienExterne'], ENT_QUOTES);
	try
	{
		$pdo->beginTransaction();
		creerFacture($typeId, $adherentId, $somme, $date, $payement, $lienExterne);
		
		ajouterCotisation($adherentId, $typeId, $date);
		
		ajouterSuccesNotification("La cotisation a été ajoutée avec succès.");
		$pdo->commit();
	} catch (Exception $exception)
	{
		ajouterErreurNotification('Erreur lors de l\'ajout de la cotisaion : '.$exception->getMessage());
		$pdo->rollBack();
	}
	if(isset($_POST['creer_nouveau']))
		header('location: form_adherent.php');
	else
		header('location: adherents.php');
	exit();
}
elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
{
	ajouterErreurNotification("Aucun adhérent n'a été sélectionné.");
	header('location: adherents.php');
	exit();
}

$id = intval($_GET['id']);
$mode = MODE_AJOUT;

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
if($requete === false)
{
	ajouterErreurNotification("L'adhérent sélectionné n'existe pas.");
	header('location: adherents.php');
	exit();
}
$user = $requete->fetch();

//On gère le cas des adhérents à vie.
if($user['date_inscription'] != '0000-00-00' && $user['date_fin_cotisation'] == '0000-00-00')
{
	ajouterErreurNotification('Ce membre est membre à vie de l\'association.');
	header('location: adherents.php');
	exit();
}

//On récupère les types de facture, uniquement de type cotisation
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type WHERE cotisation != 0');
if($requete === false)
{
	ajouterErreurNotification("Un problème a eu lieu lors de la récupération des types de factures dans la base de données.");
	header('location: adherents.php');
	exit();
}
$types = $requete->fetchAll();

if(count($types) == 0)
{
	ajouterErreurNotification("Aucun type de facture étant une cotisation n'existe dans le système.");
	header('location: adherents.php');
	exit();
}

$typesFacture = array();
foreach($types as $type)
{
	$typesFacture[] = analyserTypeFacture($type, $user['date_fin_cotisation']);
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Cotisation</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<script type="application/javascript" src="js/communtateur.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>
		<?php afficherNotification(); ?>

		<h2>Cotisation</h2>
		<form action="cotisation.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="adherent">Adherent : </label></div>
					<div class="cellule"><?php echo $user['prenom'].' '.$user['nom']; ?></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label>Fin de cotisation actuelle : </label></div>
					<div class="cellule">
						<?php if($user['date_inscription'] == '0000-00-00') echo '<i>Non membre</i>'; else echo $user['date_fin_cotisation']; ?>
						<input type="hidden" name="dateInitFin" value="<?php echo $user['date_fin_cotisation']; ?>" />
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="cotisation">Type de cotisation : <br />(date indiquée pour cotisation aujourd'hui)</label></div>
					<div class="cellule">
						<?php
						if(count($typesFacture) == 1)
						{
							echo '<input type="hidden" name="type" value="'.$typesFacture[0]['id'].'" />'.$typesFacture[0]['nom'].' (jusqu\'au '.$typesFacture[0]['fin'].')';
							if($typesFacture[0]['externe'])
							{ ?>
								</div>
							</div>
							<div class="ligne">
								<div class="cellule intitule"><label name="lienExterne">Lien externe : </label></div>
								<div class="cellule"><input id="lienExterne" name="lienExterne" required />
							<?php }
						}
						else
						{
						?>
						<select id="typeFacture" name="type" style="width: auto;">
							<?php
							foreach($typesFacture as $type)
							{
								echo '<option value="'.$type['id'].'">'.$type['nom'].' (jusqu\'au '.$type['fin'].')</option>';
							}
							?>
						</select>
						</div>
					</div>
					<div class="ligne" id="champ_lienExterne">
						<div class="cellule intitule"><label name="lienExterne">Lien externe : </label></div>
						<div class="cellule"><input id="lienExterne" name="lienExterne" />
						<?php
						}
						?>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="date">Date : </label></div>
					<div class="cellule"><input name="date" type="date" value="<?php echo date("Y-m-d"); ?>" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="somme">Montant : </label></div>
					<div class="cellule"><input name="somme" type="number" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="payement">Moyen de payement : </label></div>
					<div class="cellule"><input name="payement" required /></div>
				</div>
			</div>
			<input type="hidden" name="adherent" value="<?php echo $id; ?>" />
			<input type="hidden" name="envoi" value="1" />
			<input type="submit" id="bouton_valider" name="creer" value="Ajouter" />
			<input id="bouton_valider" type="submit" name="creer_nouveau" value="Ajouter puis nouveau membre" />
		</form>
		<?php if(count($typesFacture) > 1) { ?>
			<script type="application/javascript">
			communtateur_select('externe', 'typeFacture');

			<?php foreach($typesFacture as $type) {
				echo "communtateur_souscrire('externe', ".$type['id'].", 'champ_lienExterne', '".($type['externe'] ? 'visible' : 'cache')."');\n";
			} ?>

			<?php echo 'communtateur_mettreValeur("externe", '.$typesFacture[0]['id'].');'."\n"; ?>
			</script>
		<?php } ?>
		<?php include('bas_page.php'); ?>
	</body>
</html>
