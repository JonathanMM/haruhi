<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('factures');
verifierSiUtilisateurAPermission(VOIR_FACTURE);

if(isset($_GET['visibilite']) && intval($_GET['visibilite']) > 0)
{
	$requete = $pdo->query('SELECT id, visibilite FROM '.$bdd_prefixe.'factures WHERE id = '.intval($_GET['visibilite']));
	$donnees = $requete->fetch();
	if($donnees['id'] != 0)
	{
		if($donnees['visibilite'] == 0)
			$pdo->exec('UPDATE '.$bdd_prefixe.'factures SET visibilite = 1 WHERE id ='.$donnees['id']);
		else
			$pdo->exec('UPDATE '.$bdd_prefixe.'factures SET visibilite = 0 WHERE id ='.$donnees['id']);
	}
}

if(testerFonctionnalite('tresorerie') && testerPermission(GERER_TRESORERIE))
{
	$requete = $pdo->query('SELECT idPiece FROM '.$bdd_prefixe.'tresorerie_justificatifs WHERE typePiece = '.TRESORERIE_LIEN_TYPE_FACTURE);
	$liensSql = $requete->fetchAll();
	$factureAvecJustificatif = array();
	foreach($liensSql as $lien)
	{
		$factureAvecJustificatif[] = $lien['idPiece'];
	}
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Factures</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.min.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.min.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<nav id="panneau_page">
			<h2>Factures</h2>
			<?php if(testerPermission(FAIRE_FACTURE)) { ?>
			<p>
				<a href="ajout_facture.php">Ajouter une facture</a><br />
				<a href="import_facture.php">Importer des factures</a>
			</p>
			<?php }
			if(testerPermission(GERER_TYPE_FACTURE)) { ?>
			<p><a href="gerer_type_factures.php">Gérer les types de factures</a></p>
			<?php } ?>
		</nav>

		<div id="cadre_stockage">
			<?php //On s'occupe des messages
				afficherNotification();
			?>
			<table>
				<tr>
					<th>N° <a href="factures.php?tri=id&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="factures.php?tri=id&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Adhérent  <a href="factures.php?tri=adherent&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="factures.php?tri=adherent&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Montant</th>
					<th>Date <a href="factures.php?tri=date&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="factures.php?tri=date&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Type <a href="factures.php?tri=type&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="factures.php?tri=type&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Actions</th>
				</tr>
				<?php
				
				if(isset($_GET['tri']) && strlen($_GET['tri']) > 0)
				{
					$option_tri = array('id' => 'f.id', 'adherent' => 'a.numero, a.prenom, a.nom', 'date' => 'f.date', 'type' => 't.id');
					$tri = htmlspecialchars($_GET['tri']);
					if(isset($option_tri[$tri]))
						$tri = $option_tri[$tri];
					else
						$tri = 'f.id';

					if(isset($_GET['ordre']) && $_GET['ordre'] == 'desc')
						$tri = ' ORDER BY '.$tri.' DESC';
					else
						$tri = ' ORDER BY '.$tri.' ASC';
				} else 
					$tri = 'ORDER BY f.id ASC';

				$requete = $pdo->query('SELECT *, f.id AS id, t.nom AS nom_type_facture, a.nom AS nom
FROM '.$bdd_prefixe.'factures f 
LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = f.adherent 
LEFT JOIN '.$bdd_prefixe.'factures_type t ON t.id = f.type '.$tri);
				if(!($requete === false))
				{
					$factures = $requete->fetchAll();
					foreach($factures as $facture)
					{
						if($facture['visibilite'] == 1 || testerPermisison(FAIRE_FACTURE))
						{
							echo '<tr>';
							echo '<td style="text-align: center;">'.$facture['id'].'</td>';
							echo '<td>'.$facture['prenom'].' '.$facture['nom'].'</td><td>'.formaterMontant($facture['somme']).'</td><td>'.formater_date($facture['date']).'</td>';
							echo '<td>'.$facture['nom_type_facture'].'</td>';
							echo '<td>';
							if($facture['externe'] == 0) //Facture interne
								echo '<a href="voir_facture.php?id='.$facture['id'].'">';
							else //Facture externe
								echo '<a target="_blank" href="'.$facture['lien'].'">';
							echo '<img src="images/stockage_voir.png" title="Afficher" alt="Afficher" /></a>';
							if(testerPermission(FAIRE_FACTURE))
							{
								echo ' <a href="factures.php?visibilite='.$facture['id'].'">';
								if($facture['visibilite'] == 0)
									echo '<img src="images/facture_afficher.png" title="Rendre visible" alt="Rendre visible" />';
								else
									echo '<img src="images/facture_masquer.png" title="Masquer" alt="Masquer" />';
								echo '</a>';
							}
							if(testerFonctionnalite('tresorerie') && testerPermission(GERER_TRESORERIE) && !in_array($facture['id'], $factureAvecJustificatif))
								echo ' <a href="form_justificatif.php?id='.$facture['id'].'"><img src="images/facture_creer_lien.png" title="Créer un lien" alt="Créer un lien" /></a>';
							echo '</td></tr>';
						}
					}
				}
				?>
			</table>
		</div>
		<?php include('bas_page.php'); ?>
	</body>
</html>
