<?php
session_start();
require_once 'prelude_page.php';
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<link rel="icon" type="image/png" href="images/favicon.png" />
		<title>Haruhi → Titres</title>

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include 'haut_page.php';?>

	<h2>Titres</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Par défaut</th><th>Action</th></tr>
	<?php
$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'titre');
$titres = $requete->fetchAll();
if (!($titres === false)) {
    foreach ($titres as $titre) {
        echo '<tr>';
        echo '<td style="background-color:' . $titre['couleur'] . '">' . $titre['id'] . '</td>';
        echo '<td>' . $titre['nom'] . '</td>';
        echo '<td>' . ($titre['defaut'] ? 'oui' : '') . '</td>';
        echo '<td><a href="form_titre.php?id=' . $titre['id'] . '">Modifier</a>';
        if (testerPermission(MODIFIER_PERMISSION)) {echo ' − <a href="tester_permission.php?id=' . $titre['id'] . '">Tester les permissions</a>';}
        echo '</td>';
        echo '</tr>';
    }
}?>
	</table>

	<p><a href="form_titre.php">Ajouter un titre</a>
	<?php if (testerPermission(MODIFIER_PERMISSION)) {?><br /><a href="permissions.php">Gérer les permissions</a><?php }?></p>

	<?php include 'bas_page.php';?>
	</body>
</html>
