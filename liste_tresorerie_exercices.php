<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<link rel="icon" type="image/png" href="images/favicon.png" />
		<title>Haruhi → Exercices</title>

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Exercices</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Début</th><th>Fin</th><th>Action</th></tr>
	<?php
		$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_exercices');
		$exercices = $requete->fetchAll();
	if(!($exercices === false))
	{
		foreach($exercices as $exercice)
		{
			echo '<tr>';
			echo '<td>'.$exercice['id'].'</td>';
			echo '<td>'.$exercice['nom'].'</td>';
			echo '<td>'.formater_date($exercice['dateDebut']).'</td>';
			echo '<td>'.formater_date($exercice['dateFin']).'</td>';
			echo '<td><a href="form_tresorerie_exercice.php?id='.$exercice['id'].'">Modifier</a></td>';
			echo '</tr>';
		}
	} ?>
	</table>

	<p><a href="form_tresorerie_exercice.php">Ajouter un exercice</a>
	
	<?php include('bas_page.php'); ?>
	</body>
</html>
