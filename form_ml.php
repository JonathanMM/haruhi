<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('se');
verifierSiUtilisateurAPermission(GERER_ML);

$classesSystemeExterneDO = importerEvent('systemeExterne');
$SystemeExterneDOs = getImplementations($classesSystemeExterneDO);
if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
    $id = intval($_POST['id']);
    $mode = ($id == 0 ? MODE_AJOUT : MODE_MODIF);

    $nomML = htmlspecialchars($_POST['adresse']);

    $infosML = array(
        'id' => $id,
        'classe' => $_POST['classe'],
        'nom' => htmlspecialchars($_POST['nom']),
        'isNewsletter' => (isset($_POST['newsletter']) && $_POST['newsletter'] == 1),
        'langue' => 'fr',
        'adhesionsModerees' => (isset($_POST['moderer_inscription']) && $_POST['moderer_inscription'] == 1),
        'messagesAbonnesSeulement' => (isset($_POST['membres_msg']) && $_POST['membres_msg'] == 1),
        'messagesModerees' => (isset($_POST['moderer_msg']) && $_POST['moderer_msg'] == 1),
        'proprietaire' => htmlspecialchars($_POST['proprio']),
        'reponseA' => htmlspecialchars($_POST['reponse'])
    );
    
    if($mode == MODE_AJOUT)
    {
        if(in_array($_POST['classe'], $classesSystemeExterneDO))
        {
            $SystemeExterneDOsChoisi = array('SystemeExterneHaruhi', $_POST['classe']);
            foreach($SystemeExterneDOsChoisi as $nomClasse)
            {
                $classe = $SystemeExterneDOs[$nomClasse];
                if($classe->estModifiable())
                    $classe->createSE($nomML, $infosML);
            }
            
            ajouterSuccesNotification('Le système externe a été ajoutée avec succès.');
        } else {
            ajouterErreurNotification('Le système externe n\'existe pas !');
        }

    }

    header('location: liste_ml.php');
    exit();
}

if(isset($_GET['id']) && intval($_GET['id']) != 0)
{
    $mode = MODE_MODIF;
    $id = intval($_GET['id']);

    //On récupère toutes les infos qu'on a sur la mailing list
    $mailingList = SystemeExterneHaruhi::getNomMLFromId($id);
    
    foreach($SystemeExterneDOs as $classe)
    {
        $mailingList = array_merge($mailingList, $classe->getInformationsSE($nomML));
    }
    
} else
    $mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'une mailing list</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
        <?php include('haut_page.php'); ?>
        <?php afficherNotification(); ?>

	    <h2><?php echo ($mode == MODE_AJOUT ? 'Ajouter' : 'Modifier'); ?> une Mailing List</h2>

	    <form action="form_ml.php" method="post">
            <div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom</label></div>
					<div class="cellule"><input name="nom" required <?php if($mode == MODE_MODIF) echo 'value="'.$mailingList['nom'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="classe">Système externe</label></div>
					<div class="cellule">
                        <select name="classe">
                            <?php
                                $classesSystemeExterneDOSaufHaruhi = array_diff(array_keys($SystemeExterneDOs), array('SystemeExterneHaruhi'));
                                foreach($classesSystemeExterneDOSaufHaruhi as $classe)
                                {
                                    echo '<option value="'.$classe.'"';
                                    if($mode == MODE_MODIF && $mailingList['classe'] == $classe)
                                        echo ' selected';
                                    echo '>'.$classe.'</option>'."\n";
                                }
                            ?>
                        </select>
                    </div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="adresse">Identifiant</label></div>
					<div class="cellule"><input name="adresse" required <?php if($mode == MODE_MODIF) echo 'value="'.$mailingList['adresse'].'" readonly '; ?>/>@<?php echo $domaineEmail_ovh; ?></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="newsletter">Liste d'information ?</label></div>
					<div class="cellule"><input name="newsletter" type="checkbox" value="1" <?php if($mode == MODE_MODIF && isset($mailingList['isNewsletter']) && $mailingList['isNewsletter']) echo 'checked '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="proprio">Courriel du propriétaire</label></div>
					<div class="cellule"><input name="proprio" type="email" <?php if($mode == MODE_MODIF) echo 'value="'.$mailingList['proprietaire'].'" '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="reponse">Réponse à</label></div>
					<div class="cellule">
                        <select name="reponse">
                            <option value="mailinglist" <?php if($mode == MODE_MODIF && $mailingList['reponseA'] == 'systemeExterne') echo 'selected '; ?>>À la Mailing List</option>
                            <option value="lastuser" <?php if($mode == MODE_MODIF && $mailingList['reponseA'] == 'lastuser') echo 'selected '; ?>>À l'auteur</option>
                            <!-- TODO : À gérer cas d'une adresse mail définie -->
                        </select>
                    </div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="moderer_msg">Modérer les messages ?</label></div>
					<div class="cellule"><input name="moderer_msg" type="checkbox" value="1" <?php if($mode == MODE_MODIF && isset($mailingList['messagesModerees']) && $mailingList['messagesModerees']) echo 'checked '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="membres_msg">Seuls les membres peuvent envoyer des messages</label></div>
					<div class="cellule"><input name="membres_msg" type="checkbox" value="1" <?php if($mode == MODE_MODIF && isset($mailingList['messagesAbonnesSeulement']) && $mailingList['messagesAbonnesSeulement']) echo 'checked '; ?>/></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="moderer_inscription">Inscriptions contrôlées ?</label></div>
					<div class="cellule"><input name="moderer_inscription" type="checkbox" value="1" <?php if($mode == MODE_MODIF && isset($mailingList['adhesionsModerees']) && $mailingList['adhesionsModerees']) echo 'checked '; ?>/></div>
				</div>

	<!-- Uniquement à jour de leur cotisation ? <input type="checkbox" name="cotisation" value="1" <?php /*if($donnees['cotisation'] == 1) echo 'checked';*/ ?> /><br />-->
                <p>
                    <input type="hidden" name="envoi" value="1" />
                    <input type="hidden" name="id" value="<?php echo ($mode == MODE_MODIF ? $id : 0); ?>" />
                    <?php if($mode == MODE_MODIF) { ?>
                    <input id="bouton_valider" type="submit" name="modifier" value="Modifier" />
                    <?php } else if($mode == MODE_AJOUT) { ?>
                    <input id="bouton_valider" type="submit" name="creer" value="Créer" />
                    <?php } ?>
                </p>
        </form>

	    <?php include('bas_page.php'); ?>

	</body>
</html>