[![pipeline status](https://framagit.org/JonathanMM/haruhi/badges/master/pipeline.svg)](https://framagit.org/JonathanMM/haruhi/commits/master)

Un vrai Lisez-moi arrive bientôt ^^'

Haruhi, c'est un outil pour les associations, afin de gérer facilement leur 
listes d'adhérents, leur inscription à des listes de diffusions, la génération 
de reçus, un espace de stockage, etc…

Pour l'installer, rendez-vous dans le dossier install/

Installation des dépendances
----------------------------

Si vous voulez utiliser le système de mailing list, il est nécessaire d'installer
la mailing list d'OVH. Ce dernier utilisant composer, il suffit de lancer un :

```sh
php composer.phar install
```

Mettre à jour
-------------

Note : Ce petit guide s'adresse à ceux qui n'ont pas modifié le code de Haruhi
sur leur installation ! Pour les autres, pensez à noter les modifs afin de les
reporter après mise à jour.

Pour mettre à jour, commencer par repérer le numéro de révision actuelle de 
votre solution qui se trouve en pied de page. Puis, télécharger la nouvelle
version de l'outil, et l'extraire à la racine.
Dans le dossier update/ se trouve un fichier par révision, afin de permettre la
mise à jour de la base de données. Vous devez executer chaque fichier de votre
revision actuelle + 1 jusqu'à la révision d'arrivée. Par exemple, si vous êtes
en r20, et que vous voulez arriver en r22, vous devez executer les requêtes SQL
se trouvant dans r21.sql et r22.sql. Ces fichiers sont conçus pour des tables
utilisant le préfixe haruhi_, si ce n'est pas votre cas, remplacez le par le
préfixe que vous utiliser !

Tests unitaires
---------------

Pour lancer les tests unitaires, lancer la commande suivante :
```sh
vendor/bin/phpunit
```
