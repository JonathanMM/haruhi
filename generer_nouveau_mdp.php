<?php
session_start();
require_once 'prelude_page.php';
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);

if (!(isset($_GET['id'])) || intval($_GET['id']) <= 0) {
    ajouterErreurNotification("Aucun adhérent n'a été sélectionné.");
    header('location: adherents.php');
    exit();
}

$id = intval($_GET['id']);
$requete = $pdo->query('SELECT a.nom, a.prenom, a.id_membre, m.pseudo, m.mail FROM ' . $bdd_prefixe . 'adherents a INNER JOIN ' . $bdd_prefixe . 'membres m ON m.id = a.id_membre WHERE a.id = ' . $id);
$userAdherent = $requete->fetch();

if (is_null($userAdherent)) {
    ajouterErreurNotification("Adhérent non trouvé ou qui n'a pas de compte.");
    header('location: adherents.php');
    exit();
}

$pseudo = $userAdherent['pseudo'];
$mail = $userAdherent['mail'];

//Génération du mot de passe
$mdp = genererCodeAleatoire(16);
$mdp_hash = hash('sha512', $mdp);

$contenu = file_get_contents('./mail/nouveau-mdp.txt');
$contenu = str_replace(array('{membre.pseudo}', '{membre.motdepasse}'), array($pseudo, $mdp), $contenu);
$contenu = parserMailAdherent($contenu, $userAdherent);
$courriel = envoyer_mail($mail, "Renouvellement du mot de passe", $contenu, "generationMotDePasse", null, $id, false);
if ($courriel) {
    $pdo->exec('UPDATE ' . $bdd_prefixe . 'membres SET mdp = "' . $mdp_hash . '" WHERE id = ' . $userAdherent['id_membre']);
    ajouterSuccesNotification("Un nouveau mot de passe à été généré pour l'adhérent.");
} else {
    ajouterErreurNotification("Une erreur a eu lieu lors de l'envoi du courriel.");
}

header('location: adherents.php');
