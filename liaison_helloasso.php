<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('helloasso');
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Liaison HelloAsso</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php');
	afficherNotification();
	?>
	<h2>Liaison HelloAsso</h2>
    <nav>
        <a href="import_helloasso.php">Forcer un import</a>
    </nav>
	<?php include('bas_page.php'); ?>
	</body>
</html>