<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;

    $nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
    $dateDebut = htmlspecialchars($_POST['dateDebut']);
    $dateFin = htmlspecialchars($_POST['dateFin']);
    $etiquettes = implode(',', $_POST['etiquettes']);

    if(strlen($nom) > 0)
    {
		if($mode == MODE_AJOUT)
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_exercices (nom, dateDebut, dateFin, etiquettes) VALUE ("'.$nom.'", "'.$dateDebut.'", "'.$dateFin.'", "'.$etiquettes.'")');
		elseif($mode == MODE_MODIF)
			$pdo->exec('UPDATE '.$bdd_prefixe.'tresorerie_exercices SET nom = "'.$nom.'", dateDebut = "'.$dateDebut.'", dateFin = "'.$dateFin.'", etiquettes = "'.$etiquettes.'" WHERE id = '.$id);
	}
	header('location: liste_tresorerie_exercices.php');
	exit();
}

if(isset($_GET['id']) && intval($_GET['id']) > 0)
{
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_exercices WHERE id = '.$id);
    $exercice = $requete->fetch();
	$mode = MODE_MODIF;
} else
    $mode = MODE_AJOUT;
    
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_etiquettes');
$etiquettes = $requete->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un exercice</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un exercice</h2>

		<form action="form_tresorerie_exercice.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom : </label></div>
                    <div class="cellule"><input name="nom" value="<?php if($mode == MODE_MODIF) echo $exercice['nom']; ?>" required maxlength="128" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="dateDebut">Date de début : </label></div>
                    <div class="cellule"><input type="date" name="dateDebut" value="<?php if($mode == MODE_MODIF) echo $exercice['dateDebut']; ?>" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="dateFin">Date de fin : </label></div>
                    <div class="cellule"><input type="date" name="dateFin" value="<?php if($mode == MODE_MODIF) echo $exercice['dateFin']; ?>" required /></div>
				</div>
                <div class="ligne">
                    <div class="cellule intitule"><label name="etiquettes">Étiquettes</label></div>
                    <div class="cellule">
                        <select name="etiquettes[]" multiple required>
                        <?php
                            if($mode == MODE_MODIF)
                                $etiquettesActuelles = explode(',', $exercice['etiquettes']);
                            foreach($etiquettes as $etiquette)
                            {
                                echo '<option value="'.$etiquette['id'].'"';
                                if($mode == MODE_MODIF && in_array($etiquette['id'], $etiquettesActuelles))
                                    echo ' selected';
                                echo '>'.$etiquette['label'].'</option>';
                            }
                        ?>
                        </select>
                    </div>
                </div>
			</div>
			<p>
				<input type="hidden" name="id" value="<?php if($mode == MODE_MODIF) echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
