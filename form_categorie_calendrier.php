<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('calendrier');
verifierSiUtilisateurAPermission(GERER_CATEGORIE_CALENDRIER);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;

	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);

	if(substr($_POST['couleur'], 0, 1) == '#')
		$couleur = '#'.substr($_POST['couleur'], 1);
	else
		$couleur = htmlspecialchars($_POST['couleur']);

	if(strlen($nom) == 0)
	{
		ajouterErreurNotification("Une catégorie doit avoir un nom");
		header('location: gerer_categorie_calendrier.php');
		exit();
	}

	$pdo->beginTransaction();
		
	if($mode == MODE_AJOUT)
	{
		$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_categories (nom, couleur) VALUE ("'.$nom.'", "'.$couleur.'")');
		ajouterSuccesNotification("La catégorie a été créée avec succès.");	

		$id = $pdo->lastInsertId();
	}
	elseif($mode == MODE_MODIF)
	{
		$pdo->exec('UPDATE '.$bdd_prefixe.'calendrier_categories SET nom = "'.$nom.'", couleur = "'.$couleur.'" WHERE id = '.$id);
		ajouterSuccesNotification("La catégorie a été modifiée avec succès.");
	}

	$titres = array();
	//Permissions
	foreach($_POST as $key => $value)
	{
		$coupe = explode('_', $key);
		if(count($coupe) == 3 && $coupe[0] == 'permission')
		{
			$idTitre = intval($coupe[1]);
			$perm = $coupe[2];
			if(!isset($titres[$idTitre]))
				$titres[$idTitre] = array();
			
			$titres[$idTitre][$perm] = $value == 1;
		}
	}

	if($mode == MODE_MODIF)
		$pdo->exec('DELETE FROM '.$bdd_prefixe.'calendrier_permissions WHERE idCategorie = '.$id);
	foreach($titres as $idTitre => $titre)
	{
		if($mode == MODE_AJOUT)
		{
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_permissions (idCategorie, idTitre, lecture, ecriture) VALUES ('.
			$id.', '.$idTitre.', '.((isset($titre['lecture']) && $titre['lecture']) ? '1' : '0').', '.((isset($titre['ecriture']) && $titre['ecriture']) ? '1' : '0').')');		
		}
		elseif($mode == MODE_MODIF)
		{
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'calendrier_permissions (idCategorie, idTitre, lecture, ecriture) VALUES ('.
				$id.', '.$idTitre.', '.((isset($titre['lecture']) && $titre['lecture']) ? '1' : '0').', '.((isset($titre['ecriture']) && $titre['ecriture']) ? '1' : '0').')');		
		}
	}

	$pdo->commit();
	header('location: gerer_categorie_calendrier.php');
	exit();
}

//On récupère les titres
$requete = $pdo->query('SELECT id, nom FROM '.$bdd_prefixe.'titre');
$titres = $requete->fetchAll();
$permissions = array();
if(isset($_GET['id']) && intval($_GET['id']) > 0)
{
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier_categories WHERE id = '.$id);
	$categorie = $requete->fetch();
	//Et on récupère aussi les permissions !
	$requete = $pdo->query('SELECT idTitre, lecture, ecriture FROM '.$bdd_prefixe.'calendrier_permissions WHERE idCategorie = '.$id);
	$permissionsSql = $requete->fetchAll();
	foreach($permissionsSql as $permission)
	{
		$permissions[$permission['idTitre']] = array(
			'lecture' => $permission['lecture'] == 1,
			'ecriture' => $permission['ecriture'] == 1
		);
	}
	$mode = MODE_MODIF;
} else
	$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'une catégorie</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />

		<script type="application/javascript" src="js/form_couleur.min.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'une catégorie</h2>

		<form action="form_categorie_calendrier.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom : </label></div>
					<div class="cellule"><input name="nom" value="<?php if($mode == MODE_MODIF) echo $categorie['nom']; ?>" required maxlength="128" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="couleur">Couleur : </label></div>
					<div class="cellule"><input name="couleur" type="color" id="couleur" onChange="maj_couleur();" value="<?php if($mode == MODE_MODIF) echo $categorie['couleur']; ?>" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"></div>
					<div class="cellule">
						<div class="test_couleur" onClick="choisir_couleur('#46B8F1');" style="background-color:#46B8F1"></div>
						<div class="test_couleur" onClick="choisir_couleur('#ffedb0');" style="background-color:#ffedb0"></div>
						<div class="test_couleur" onClick="choisir_couleur('#f0f0f0');" style="background-color:#f0f0f0"></div>
						<div class="test_couleur" onClick="choisir_couleur('#F0BDE5');" style="background-color:#F0BDE5"></div>
						<div class="test_couleur" onClick="choisir_couleur('#d6d6d6');" style="background-color:#d6d6d6"></div>
						<div class="test_couleur" onClick="choisir_couleur('#FFB5B5');" style="background-color:#FFB5B5"></div>
						<div class="test_couleur" onClick="choisir_couleur('#FFE991');" style="background-color:#FFE991"></div>
						<div class="test_couleur" onClick="choisir_couleur('#CFFECC');" style="background-color:#CFFECC"></div>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Permissions</div>
					<div class="cellule">
						<table>
							<tr>
								<th>Titre</th>
								<th>Lecture</th>
								<th>Écriture</th>
							</tr>
							<?php
							foreach($titres as $titre)
							{
								echo '<tr>';
									echo '<td>'.$titre['nom'].'</td>';
									echo '<td><input type="checkbox" name="permission_'.$titre['id'].'_lecture" value="1"';
									if($mode == MODE_MODIF && isset($permissions[$titre['id']]) && $permissions[$titre['id']]['lecture'])
										echo 'checked';
									echo ' /></td>';
									echo '<td><input type="checkbox" name="permission_'.$titre['id'].'_ecriture" value="1"';
									if($mode == MODE_MODIF && isset($permissions[$titre['id']]) && $permissions[$titre['id']]['ecriture'])
										echo 'checked';
									echo ' /></td>';
								echo '</tr>';
							}
							?>
						</table>
					</div>
				</div>
			</div>
			<p>
				<input type="hidden" name="id" value="<?php if($mode == MODE_MODIF) echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
