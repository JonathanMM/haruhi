<?php
session_start();
require_once 'prelude_page.php';
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Administation</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include 'haut_page.php';?>

	<h2>Administration</h2>

    <nav>
	<?php if (testerPermission(TOUCHE_ADHERENT)) {?><a href="liste_titres.php">Titres</a><br /><?php }?>
	<?php if ($fonctionnalites_statut['se'] && testerPermission(GERER_ML)) {?><a href="liste_ml.php">Listes ML</a><br /><?php }?>
	<?php if (testerPermission(VOIR_LOGS_MAIL)) {?><a href="logs_mail.php">Logs courriel</a><br /><?php }?>
	<?php if (testerPermission(SAUVEGARDE)) {?><a href="sauvegardes.php">Sauvegardes</a><br /><?php }?>
	<?php if (testerPermission(TOUCHE_MODELE_MAIL)) {?><a href="form_modele_mail.php">Modèles de mails</a><br /><?php }?>
	<?php if (testerPermission(TOUCHE_ADHERENT) && isset($fonctionnalites_statut['helloasso']) && $fonctionnalites_statut['helloasso']) {?><a href="liaison_helloasso.php">Liaison HelloAsso</a><br /><?php }?>
	<?php if (testerPermission(GERER_OAUTH) && isset($fonctionnalites_statut['oauth']) && $fonctionnalites_statut['oauth']) {?><a href="liste_oauth.php">Gérer les clients OAuth2</a><br /><?php }?>
	<?php if (testerPermission(GERER_ACCUEIL)) {?><a href="gerer_accueil.php">Gérer la page d'accueil</a><br /><?php }?>
    </nav>

	<?php include 'bas_page.php';?>
	</body>
</html>