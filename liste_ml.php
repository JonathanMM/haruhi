<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('se');
verifierSiUtilisateurAPermission(GERER_ML);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Liste ML</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
	<h2>Mailing list</h2>
	<?php afficherNotification(); ?>
	<table>
	<tr><th>Id</th><th>Nom</th><th>Adresse</th><th>Action</th></tr>
	<?php $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml');
	if(!($requete === false))
	{
		foreach($requete->fetchAll() as $donnees)
		{
			echo '<tr><td>'.$donnees['id'].'</td><td>'.$donnees['nom'].'</td><td>'.$donnees['adresse'].'</td><td>';
			echo '<a href="abonnes_ml.php?id='.$donnees['id'].'">Liste des abonnés</a> - ';
			echo '<a href="pull_ml.php?id='.$donnees['id'].'">Synchroniser depuis la source distante</a> - ';
			echo '<a href="push_ml.php?id='.$donnees['id'].'">Synchroniser vers la source distante</a>';
			//echo '<a href="modif_ml.php?id='.$donnees['id'].'">Modifier</a> <a href="supp_ml.php?id='.$donnees['id'].'">Supprimer</a>';
			echo '</td></tr>';
		}
	} ?>
	</table>

	<p>
		<a href="form_ml.php">Ajouter une ML</a><br />
		<a href="pull_ml.php">Synchroniser les listes depuis la source distante</a><br />
		<a href="push_ml.php">Synchroniser les listes vers la source distante</a>
	</p>
	<?php include('bas_page.php'); ?>
	</body>
</html>