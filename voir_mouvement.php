<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);
verifierSiIdInGet('tresorerie.php');

$idMouvement = intval($_GET['id']);

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_mouvement WHERE id = '.$idMouvement);
$mouvement = $requete->fetch();

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette l
INNER JOIN '.$bdd_prefixe.'tresorerie_etiquettes e ON l.idEtiquette = e.id
WHERE l.idMouvement = '.$idMouvement);
$etiquettes = $requete->fetchAll();

//Liens
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_justificatifs j '.
//LEFT JOIN '.$bdd_prefixe.'factures f ON j.idPiece = f.id AND j.typePiece = '.TRESORERIE_LIEN_TYPE_FACTURE.'
'WHERE j.idMouvement = '.$idMouvement);
$liens = $requete->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Mouvement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php');
	afficherNotification();
	?>
    <h2>Mouvement #<?php echo $mouvement['id']; ?></h2>

    <p>
        Date : <?php echo formater_date($mouvement['date']); ?><br />
        Libélé : <?php echo $mouvement['label']; ?><br />
        <?php if(!is_null($mouvement['credit'])) { ?>Crédit : <?php echo formaterMontant($mouvement['credit']); ?><br /><?php } ?>
        <?php if(!is_null($mouvement['debit'])) { ?>Débit : <?php echo formaterMontant($mouvement['debit']); ?><br /><?php } ?>
        <ul class="etiquettes-liste"><?php
        foreach($etiquettes as $etiquette)
        {
            echo '<li class="etiquette-'.$etiquette['id'].'">'.$etiquette['label'].'</li>';
        }
        ?></ul>
    </p>

	<h3>Liens</h3>

	<table>
		<tr>
			<th>Lien</th>
			<th>Description</th>
			<th>Montant</th>
		</tr>
		<?php
		foreach($liens as $lien)
		{
			echo '<tr>';
				echo '<td>'.recuperer_label_lien($lien['typePiece'], $lien['idPiece'], true).'</td>';
				echo '<td>'.$lien['description'].'</td>';
				echo '<td>'.formaterMontant($lien['montant']).'</td>';
			echo '</tr>';
		}
		?>
	</table>

	<nav>
		<a href="form_mouvement.php?id=<?php echo $idMouvement; ?>">Modifier le mouvement</a><br />
		<a href="attacher_lien_mouvement.php?id=<?php echo $idMouvement; ?>">Attacher des liens au mouvement</a>
	</nav>

	<?php include('bas_page.php'); ?>
	<style type="text/css">
	<?php
		foreach($etiquettes as $etiquette)
		{
			if(!is_null($etiquette['couleur']))
			{
				echo '.etiquettes-liste li.etiquette-'.$etiquette['id']."\n";
				echo '{'."\n";
				echo '	border-color: '.$etiquette['couleur']."\n";
				echo '}'."\n";
			}
		}
	?>
	</style>
	</body>
</html>