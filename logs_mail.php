<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(VOIR_LOGS_MAIL);

$whereAdherent = '';
$idAdherent = null;
if(isset($_GET['adherent']) && intval($_GET['adherent']) > 0)
{
	$idAdherent = intval($_GET['adherent']);
	$whereAdherent = ' WHERE a.id = '.$idAdherent;
}

$requete = $pdo->query('SELECT *, l.id AS id_mail, l.titre AS titre_mail FROM '.$bdd_prefixe.'logs_mail l 
LEFT JOIN '.$bdd_prefixe.'adherents a ON a.id = l.idMembre'.$whereAdherent.' 
ORDER BY l.id DESC');
$mails = $requete->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Logs Courriel</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="edition_facture.css" type="text/css" media="print">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
		<nav id="panneau_page">
			<h2>Logs courriel</h2>
			<?php
			if(!is_null($idAdherent))
				echo '<a href="voir_adherent.php?id='.$idAdherent.'">Voir la fiche de l\'adhérent</a>';
			?>
		</nav>

		<div id="cadre_stockage">
			<?php //On s'occupe des messages
			afficherNotification();
			?>
			<table>
				<tr>
					<th>Id</th>
					<th>Date</th>
					<th>Titre</th>
					<!--<th>Contenu</th>-->
					<th>Destinataire</th>
					<th>Adhérent</th>
					<th>Contexte</th>
					<th>Statut</th>
				</tr>
				<?php
					foreach($mails as $mail)
					{
						echo '<tr>';
						echo '<td>'.$mail['id_mail'].'</td>';
						echo '<td>'.$mail['date'].'</td>';
						echo '<td>'.$mail['titre_mail'].'</td>';
						//echo '<td>'.((is_null($mail['contenu']) || strlen($mail['contenu']) == 0) ? '***SECRET***' : htmlspecialchars_decode($mail['contenu'])).'</td>';
						echo '<td>'.$mail['destinataire'].'</td>';
						echo '<td>'.$mail['pseudo'].'</td>';
						echo '<td>'.$mail['contexte'].'</td>';
						//Statut envoi mail
						echo '<td>';
						switch($mail['envoye'])
						{
							case MAIL_STATUT_ERREUR:
								echo 'erreur';
								break;
							case MAIL_STATUT_ENVOYE:
								echo 'envoyé';
								break;
							case MAIL_STATUT_RETENU:
								echo 'retenu';
								break;
						}
						echo '</td>';
						echo '</tr>';
					}
				?>
			</table>
		</div>
	<?php include('bas_page.php'); ?>
	</body>
</html>