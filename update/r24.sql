-- Création de la table des logs
CREATE TABLE `haruhi_logs_mail` (
  `id` int(11) NOT NULL,
  `titre` text NOT NULL,
  `contenu` text NOT NULL,
  `destinataire` varchar(512) NOT NULL,
  `idMembre` int(11) NOT NULL,
  `contexte` varchar(64) NOT NULL,
  `date` datetime NOT NULL,
  `envoye` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `haruhi_logs_mail`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `haruhi_logs_mail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
