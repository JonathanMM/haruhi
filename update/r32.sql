-- Ajout d'une case pour l'adhésion à vie
ALTER TABLE `haruhi_adherents` ADD `cotisation_infinie` TINYINT(1) NOT NULL DEFAULT '0' AFTER `date_fin_cotisation`;
ALTER TABLE `haruhi_adherents` CHANGE `date_fin_cotisation` `date_fin_cotisation` DATE NULL DEFAULT NULL;
UPDATE `haruhi_adherents` SET cotisation_infinie = 1, date_fin_cotisation = NULL WHERE date_inscription != '0000-00-00' and date_fin_cotisation = '0000-00-00';