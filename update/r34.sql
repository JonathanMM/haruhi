-- Option : Se souvenir de moi
CREATE TABLE `haruhi_membresToken` (
  `id` int(11) NOT NULL,
  `token` varchar(36) COLLATE utf8_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `haruhi_membresToken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

ALTER TABLE `haruhi_membresToken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;