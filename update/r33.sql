-- Création de la table de configuration
CREATE TABLE `haruhi_configuration` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cle` varchar(32) NOT NULL UNIQUE KEY,
  `valeur` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `haruhi_configuration` (`id`, `cle`, `valeur`) VALUES
(1, 'modeleMailHtml', '<!DOCTYPE html>\r\n<html>\r\n    <title></title>\r\n    <body>\r\n        <h1>Haruhi</h1>\r\n        <p>{contenu}</p>\r\n    </body>\r\n</html>\r\n');

-- Oauth2 : Création des tables nécessaires au serveur
CREATE TABLE haruhi_oauth_clients (
  client_id             VARCHAR(80)   NOT NULL,
  client_secret         VARCHAR(80),
  redirect_uri          VARCHAR(2000),
  grant_types           VARCHAR(80),
  scope                 VARCHAR(4000),
  user_id               VARCHAR(80),
  nom                   VARCHAR(64),
  PRIMARY KEY (client_id)
);

CREATE TABLE haruhi_oauth_access_tokens (
  access_token         VARCHAR(40)    NOT NULL,
  client_id            VARCHAR(80)    NOT NULL,
  user_id              VARCHAR(80),
  expires              TIMESTAMP      NOT NULL,
  scope                VARCHAR(4000),
  PRIMARY KEY (access_token)
);

CREATE TABLE haruhi_oauth_authorization_codes (
  authorization_code  VARCHAR(40)     NOT NULL,
  client_id           VARCHAR(80)     NOT NULL,
  user_id             VARCHAR(80),
  redirect_uri        VARCHAR(2000),
  expires             TIMESTAMP       NOT NULL,
  scope               VARCHAR(4000),
  id_token            VARCHAR(1000),
  PRIMARY KEY (authorization_code)
);

CREATE TABLE haruhi_oauth_refresh_tokens (
  refresh_token       VARCHAR(40)     NOT NULL,
  client_id           VARCHAR(80)     NOT NULL,
  user_id             VARCHAR(80),
  expires             TIMESTAMP       NOT NULL,
  scope               VARCHAR(4000),
  PRIMARY KEY (refresh_token)
);