-- Mailing list : Ajout des externes
CREATE TABLE `haruhi_ml_externe` (
  `id` int(11) NOT NULL,
  `id_ml` int(11) NOT NULL,
  `courriel` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `haruhi_ml_externe`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `haruhi_ml_externe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;