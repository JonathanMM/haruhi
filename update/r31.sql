-- Création des tables de catégories

CREATE TABLE `haruhi_calendrier_categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `couleur` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `haruhi_calendrier_permissions` (
  `id` int(11) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  `idTitre` int(11) NOT NULL,
  `lecture` tinyint(1) NOT NULL,
  `ecriture` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `haruhi_calendrier_categories`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `haruhi_calendrier_permissions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `haruhi_calendrier_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `haruhi_calendrier_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Changement du type de la date dans le calendrier
ALTER TABLE `haruhi_calendrier` CHANGE `date` `date` DATETIME NOT NULL;

-- Création de la table des participants dans les événements
CREATE TABLE `haruhi_calendrier_participants` (
  `id` int(11) NOT NULL,
  `idEvenement` int(11) NOT NULL,
  `idMembre` int(11) NOT NULL,
  `estOrganisateur` tinyint(1) NOT NULL,
  `reponse` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `haruhi_calendrier_participants`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `haruhi_calendrier_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Ajout de la case uuid
ALTER TABLE `haruhi_calendrier` ADD `uuid` VARCHAR(36) NOT NULL AFTER `id`, ADD UNIQUE `uuid` (`uuid`);

-- Création des tables de trésorerie

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_etiquettes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(16) NOT NULL,
  `couleur` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_exercices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `etiquettes` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_justificatifs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idMouvement` int(11) NOT NULL,
  `typePiece` tinyint(4) NOT NULL,
  `idPiece` int(11) NOT NULL,
  `description` varchar(256) NOT NULL,
  `montant` decimal(8,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_liens_mouvement_etiquette` (
  `idMouvement` int(11) NOT NULL,
  `idEtiquette` int(11) NOT NULL,
  PRIMARY KEY (`idMouvement`,`idEtiquette`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_mouvement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `label` varchar(512) NOT NULL,
  `credit` decimal(8,2) DEFAULT NULL,
  `debit` decimal(8,2) DEFAULT NULL,
  `nbPieces` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
