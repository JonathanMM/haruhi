-- Type de facture : Augmentation de la taille du nom
ALTER TABLE `haruhi_factures_type` CHANGE `nom` `nom` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- Type de facture : Ajout du label
ALTER TABLE `haruhi_factures_type` ADD `label` VARCHAR(64) NULL DEFAULT NULL AFTER `nom`;
UPDATE `haruhi_factures_type` SET label = nom;

-- Titre : Ajout de la notion de titre par défaut
ALTER TABLE `haruhi_titre` ADD `defaut` BOOLEAN NOT NULL DEFAULT FALSE AFTER `ordre`;
