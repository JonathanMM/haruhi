<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;

    $libele = htmlspecialchars($_POST['label'], ENT_QUOTES);
    $date = htmlspecialchars($_POST['date']);
    $estCredit = $_POST['type'] == 0;
    $credit = null;
    $debit = null;
    if($estCredit)
        $credit = floatval($_POST['montant']);
    else
        $debit = floatval($_POST['montant']);
    $etiquettes = $_POST['etiquettes'];

    $pdo->beginTransaction();

    if($mode == MODE_AJOUT)
		$id = creer_mouvement($libele, $date, $credit, $debit, $etiquettes);
    elseif($mode == MODE_MODIF)
    {
		$pdo->exec('UPDATE '.$bdd_prefixe.'tresorerie_mouvement SET label = "'.$libele.'", date = "'.$date.'", credit = '.(is_null($credit) ? 'NULL' : $credit).', debit = '.(is_null($debit) ? 'NULL' : $debit).' WHERE id = '.$id);
        modifier_mouvement_etiquettes($id, $etiquettes);
    }

    //On regarde les liens maintenant
    $nouveauxLiens = array();
    foreach($_POST as $key => $value)
    {
        if(substr($key, 0, 14) == 'detacher-lien-' && $value == 1)
        {
            $idLien = intval(substr($key, 14));
            $pdo->exec('DELETE FROM '.$bdd_prefixe.'tresorerie_justificatifs WHERE id = '.$idLien.' AND typePiece = '.TRESORERIE_LIEN_TYPE_AUCUN);
            $pdo->exec('UPDATE '.$bdd_prefixe.'tresorerie_justificatifs SET idMouvement = 0 WHERE id = '.$idLien.' AND typePiece = '.TRESORERIE_LIEN_TYPE_FACTURE);
        } elseif((substr($key, 0, 17) == 'lien-description-' || substr($key, 0, 13) == 'lien-montant-') && strlen($value) > 0)
        {
            $idAjout = null;
            $typeInfo = null;
            $valeurInfo = null;
            if(substr($key, 0, 17) == 'lien-description-')
            {
                $idAjout = intval(substr($key, 17));
                $typeInfo = 'description';
                $valeurInfo = htmlspecialchars($value, ENT_QUOTES);
            } else {
                $idAjout = intval(substr($key, 13));
                $typeInfo = 'montant';
                $valeurInfo = floatval($value);
            }
            if(!isset($nouveauxLiens[$idAjout]))
                $nouveauxLiens[$idAjout] = array();
            $nouveauxLiens[$idAjout][$typeInfo] = $valeurInfo;
        }
    }
    
    foreach($nouveauxLiens as $infoLien)
    {
        if(isset($infoLien['description']) && isset($infoLien['montant']))
            $pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_justificatifs (idMouvement, typePiece, idPiece, description, montant)
            VALUES ('.$id.', '.TRESORERIE_LIEN_TYPE_AUCUN.', NULL, "'.$infoLien['description'].'", '.$infoLien['montant'].')');
    }

    $pdo->commit();

    ajouterSuccesNotification('Le mouvement a été modifié avec succès.');

	header('location: voir_mouvement.php?id='.$id);
	exit();
}

if(isset($_GET['id']) && intval($_GET['id']) > 0)
{
    $id = intval($_GET['id']);
    
    $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_mouvement WHERE id = '.$id);
    $mouvement = $requete->fetch();

    $estCredit = is_null($mouvement['debit']);
    $montant = $estCredit ? $mouvement['credit'] : $mouvement['debit'];

    $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette l
    WHERE l.idMouvement = '.$id);
    $etiquettesSql = $requete->fetchAll();
    $etiquettesActuelles = array();
    foreach($etiquettesSql as $etiquette)
    {
        $etiquettesActuelles[] = $etiquette['idEtiquette'];
    }

    //Liens
    $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_justificatifs j WHERE j.idMouvement = '.$id);
    $liens = $requete->fetchAll();

	$mode = MODE_MODIF;
} else
    $mode = MODE_AJOUT;
    
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_etiquettes');
$etiquettes = $requete->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un mouvement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un mouvement</h2>

		<form action="form_mouvement.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="date">Date : </label></div>
                    <div class="cellule"><input type="date" name="date" value="<?php if($mode == MODE_MODIF) echo $mouvement['date']; ?>" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="label">Libélé : </label></div>
                    <div class="cellule"><input name="label" value="<?php if($mode == MODE_MODIF) echo $mouvement['label']; ?>" required maxlength="128" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="type">Type : </label></div>
                    <div class="cellule">
                        <input name="type" id="type_credit" type="radio" value="0" <?php if($mode == MODE_MODIF && $estCredit) echo 'checked'; ?> required /><label for="type_credit"> Crédit</label>
                        <input name="type" id="type_debit" type="radio" value="1" <?php if($mode == MODE_MODIF && !$estCredit) echo 'checked'; ?> required /><label for="type_debit"> Débit</label>
                    </div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="montant">Montant : </label></div>
                    <div class="cellule"><input name="montant" type="number" min="0" step="0.01" value="<?php if($mode == MODE_MODIF) echo $montant; ?>" required /></div>
				</div>
                <div class="ligne">
                    <div class="cellule intitule"><label name="etiquettes">Étiquettes</label></div>
                    <div class="cellule">
                        <select name="etiquettes[]" multiple required>
                        <?php
                            foreach($etiquettes as $etiquette)
                            {
                                echo '<option value="'.$etiquette['id'].'"';
                                if($mode == MODE_MODIF && in_array($etiquette['id'], $etiquettesActuelles))
                                    echo ' selected';
                                echo '>'.$etiquette['label'].'</option>';
                            }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="ligne">
                    <div class="cellule intitule">Liens</div>
                    <div class="cellule">
                        <table>
                            <tr>
                                <th>Détacher</th>
                                <th>Lien</th>
                                <th>Description</th>
                                <th>Montant</th>
                            </tr>
                            <?php
                            if($mode == MODE_MODIF)
                            {

                                foreach($liens as $lien)
                                {
                                    echo '<tr>';
                                    echo '<td><input type="checkbox" name="detacher-lien-'.$lien['id'].'" value="1" /></td>';
                                    echo '<td>'.recuperer_label_lien($lien['typePiece'], $lien['idPiece'], false).'</td>';
                                    echo '<td>'.$lien['description'].'</td>';
                                    echo '<td>'.formaterMontant($lien['montant']).'</td>';
                                    echo '</tr>';
                                }
                            }
                            ?>
                            <tr>
                                <td></td>
                                <td>Nouvelle note</td>
                                <td><input type="text" name="lien-description-1" /></td>
                                <td><input type="number" step="0.01" min="0" name="lien-montant-1" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
			</div>
			<p>
				<input type="hidden" name="id" value="<?php if($mode == MODE_MODIF) echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
