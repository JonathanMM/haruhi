<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('calendrier');
verifierSiUtilisateurAPermission(GERER_CATEGORIE_CALENDRIER);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Catégories du calendrier</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>
    <h2>Gérer les catégories du calendrier</h2>
	<?php afficherNotification(); ?>
	<table>
	    <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
		<?php
		$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier_categories');
		$categories = $requete->fetchAll();
		foreach($categories as $categorie)
		{
			echo '<tr>';
				echo '<td>'.$categorie['id'].'</td>';
				echo '<td>'.$categorie['nom'].'</td>';
				echo '<td><a href="form_categorie_calendrier.php?id='.$categorie['id'].'">Modifier</a></td>';
			echo '</tr>';
		}
		?>
    </table>
	<nav>
		<a href="form_categorie_calendrier.php">Ajouter une nouvelle catégorie</a>
	</nav>
	<?php include('bas_page.php'); ?>
	</body>
</html>