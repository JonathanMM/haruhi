<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('se');
verifierSiUtilisateurAPermission(GERER_ML);

$classesSystemeExterneDO = importerEvent('systemeExterne');
$classesSystemeExterneDO = array_diff($classesSystemeExterneDO, array('SystemeExterneHaruhi'));
$SystemeExterneDOs = getImplementations($classesSystemeExterneDO);
try
{
    $SystemeExterneDODestination = new SystemeExterneHaruhi();
    if(isset($_GET['id']) && intval($_GET['id']) > 0)
    {
        $mailingList = $SystemeExterneDODestination->getNomMLFromId(intval($_GET['id']));
        $SystemeExterneDOSource = $SystemeExterneDOs[$mailingList['classe']];
        $ml = $mailingList['adresse'];

        synchroniserCourriels($SystemeExterneDOSource, $SystemeExterneDODestination, $ml);
    } else {
        foreach($SystemeExterneDOs as $SystemeExterneDOSource)
        {
            $listeML = synchroListSE($SystemeExterneDOSource, $SystemeExterneDODestination);
            
            foreach($listeML as $ml)
            {
                synchroniserCourriels($SystemeExterneDOSource, $SystemeExterneDODestination, $ml);
            }
        }
    }
    ajouterSuccesNotification('Les systèmes externes ont bien été synchronisés.');
} catch(Exception $ex)
{
    ajouterErreurNotification('Une erreur a eu lieu lors de la synchronisation : '.$ex->getMessage());
}

header('location: liste_ml.php');
?>