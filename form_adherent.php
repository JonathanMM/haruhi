<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);

if (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
	$id = intval($_POST['id']);
	if ($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;
	$numero = intval($_POST['numero']);
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
	$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
	$courriel = trim(htmlspecialchars($_POST['courriel'], ENT_QUOTES));
	$titre = intval($_POST['titre']);
	$date_fin_cotisation = htmlspecialchars($_POST['date_fin_cotisation'], ENT_QUOTES);
	$membre_a_vie = isset($_POST['membre_a_vie']) && $_POST['membre_a_vie'] == 1;

	//On va gérer l'adresse
	$adresse['ligne1'] = htmlspecialchars($_POST['adresse_ligne1'], ENT_QUOTES);
	$adresse['ligne2'] = htmlspecialchars($_POST['adresse_ligne2'], ENT_QUOTES);
	$adresse['cp'] = htmlspecialchars($_POST['adresse_cp'], ENT_QUOTES);
	$adresse['ville'] = htmlspecialchars($_POST['adresse_ville'], ENT_QUOTES);
	$adresse['pays'] = htmlspecialchars($_POST['adresse_pays'], ENT_QUOTES);

	if (strlen($adresse['ligne1']) > 0 || strlen($adresse['ligne2']) > 0 || strlen($adresse['cp']) > 0 || strlen($adresse['ville']) > 0 || strlen($adresse['pays']) > 0)
		$adresses = array($adresse);
	else
		$adresses = array();

	$adresse_fixe = stringifyAdresse($adresses);

	if (isset($_POST['avatar']) && $_POST['avatar'] == 1)
		$avatar_supp = true;
	else
		$avatar_supp = false;

	//On récupère l'ancien utilisateur
	if ($mode == MODE_MODIF) {
		$user_requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'adherents WHERE id = ' . $id);
		$user = $user_requete->fetch();
	}

	//Maintenant, on regarde du côté des créations
	if ($titre == 0) //Nouveau titre, on le créer
	{
		$titre_autre = htmlspecialchars($_POST['titre_autre'], ENT_QUOTES);
		$pdo->exec('INSERT INTO ' . $bdd_prefixe . 'titre (id, nom, permission, visibilite, couleur, ordre) VALUES (NULL, "' . $titre_autre . '", 0, 0, "#FFFFFF", 0)');
		$titre = $pdo->lastInsertId();
	}

	$date_inscription = htmlspecialchars($_POST['date_inscription'], ENT_QUOTES);
	$commentaire = htmlspecialchars($_POST['com'], ENT_QUOTES);

	$adherent = array(
		'numero' => $numero,
		'prenom' => $prenom,
		'nom' => $nom,
		'pseudo' => $pseudo,
		'adresse' => $adresse_fixe,
		'telephone' => $telephone,
		'courriel' => $courriel,
		'titre' => $titre,
		'date_inscription' => $date_inscription,
		'date_fin_cotisation' => $date_fin_cotisation,
		'cotisation_infinie' => $membre_a_vie,
		'commentaire' => $commentaire
	);

	if (strlen($adherent['date_inscription']) == 0) {
		$adherent['date_inscription'] = '0000-00-00';
		$adherent['date_fin_cotisation'] = null;
		$adherent['cotisation_infinie'] = false;
	}

	if ($membre_a_vie || strlen($date_fin_cotisation) == 0) {
		$adherent['date_fin_cotisation'] = null;
	}

	if ($mode == MODE_AJOUT) {
		if (isset($_POST['newsletter']))
			$adherent['newsletter'] = $_POST['newsletter'];

		if ($fonctionnalites_statut['intervenants'] && count($_POST['nom_enfants']) == count($_POST['prenom_enfants']) && count($_POST['nom_enfants']) == count($_POST['date_enfants'])) {
			$enfants = array();
			foreach ($_POST['nom_enfants'] as $i => $nome) {
				if (strlen($nome) > 0) {
					$enfants[] = array(
						'nom' => htmlspecialchars($nome),
						'prenom' => htmlspecialchars($_POST['prenom_enfants'][$i]),
						'classe' => htmlspecialchars($_POST['classe_enfants'][$i]),
						'date_naissance' => htmlspecialchars($_POST['date_enfants'][$i])
					);
				}
			}
			$adherent['enfants'] = $enfants;
		}

		$id_adherent = creerAdherent($adherent); //TODO : Gérer les erreurs
		if ($id_adherent === false)
			ajouterErreurNotification("L'adhérent n'a pas pu être créé");
		else {
			ajouterSuccesNotification("L'adhérent a été ajouté avec succès.");

			if (isset($_POST['creer_cotisation'])) {
				header('location: cotisation.php?id=' . $id_adherent);
				exit();
			}

			if (isset($_POST['creer_nouveau'])) {
				header('location: form_adherent.php');
				exit();
			}
		}
	} elseif ($mode == MODE_MODIF) {
		if (strlen($prenom) > 0 && strlen($nom) > 0) {
			//Attention : $user a déjà appeler avant, avec les anciennes valeurs de l'adherent !
			if ($avatar_supp) {
				//On doit récupérer le nom de l'avatar courant
				if (!is_null($user['avatar']))
					unlink('images/avatar/' . $user['avatar']);
			}

			if ($numero == 0 && $date_inscription != '0000-00-00')
				$numero = getNouveauNumeroAdherent();

			$pdo->beginTransaction();

			$pdo->exec('UPDATE ' . $bdd_prefixe . 'adherents SET numero = "' . $numero . '", prenom = "' . $prenom . '", nom = "' . $nom . '", pseudo = "' . $pseudo . '", 
			adresse = "' . $adresse_fixe . '", telephone = "' . $telephone . '", courriel = "' . $courriel . '",
			titre = "' . $titre . '", date_inscription = "' . $date_inscription . '", 
			date_fin_cotisation = ' . (is_null($adherent['date_fin_cotisation']) ? 'NULL' : '"' . $adherent['date_fin_cotisation'] . '"') . ', 
			cotisation_infinie = ' . ($adherent['cotisation_infinie'] ? '1' : '0') . ', 
			commentaire = "' . $commentaire . '"' . ($avatar_supp ? ', avatar = NULL' : '') . ' WHERE id = ' . $id);

			//On regarde s'il y a un compte associé et si l'adresse mail a changé
			if ($user['id_membre'] != 0 && $user['courriel'] != $adherent['courriel']) {
				$pdo->exec('UPDATE ' . $bdd_prefixe . 'membres SET mail = "' . $adherent['courriel'] . '" WHERE id = ' . $user['id_membre']);
			}

			//Et on appelle toutes les implémentations
			$classes = importerEvent('adherent');
			foreach ($classes as $classe) {
				$classe::modifier($id, $adherent, $user);
			}

			$pdo->commit();
			ajouterSuccesNotification("L'adhérent a été modifié avec succès.");
		} else
			ajouterErreurNotification("L'adhérent n'a pu être modifié, les mentions suivantes étant obligatoires : Nom, Prénom.");
	}

	header('location: adherents.php');
	exit();
}

if (isset($_GET['id']) && intval($_GET['id']) != 0) {
	$mode = MODE_MODIF;
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'adherents WHERE id = ' . $id);
	$user = $requete->fetch();

	//Et on va traiter l'adresse
	$adresse = parseAdresse($user['adresse']);
	//Mode de compatibilité
	if (count($adresse) == 0 && strlen($user['adresse']) > 0) {
		$adresse = array(array(
			'ligne1' => $user['adresse'],
			'ligne2' => '',
			'cp' => '',
			'ville' => '',
			'pays' => ''
		));
	}
} else
	$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un adhérent</title>
	<link rel="icon" type="image/png" href="images/favicon.png" />

	<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	<script type="text/javascript" src="js/form_adherent.js"></script>
</head>

<body>
	<?php include('haut_page.php'); ?>
	<?php afficherNotification(); ?>

	<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un adhérent</h2>

	<form action="form_adherent.php" method="post">
		<div class="formulaire">
			<div class="ligne">
				<div class="cellule intitule"><label name="numero">Numéro</label></div>
				<div class="cellule"><input name="numero" <?php if ($mode == MODE_MODIF) echo 'value="' . $user['numero'] . '" '; ?> /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="prenom">Prénom</label></div>
				<div class="cellule"><input name="prenom" <?php if ($mode == MODE_MODIF) echo 'value="' . $user['prenom'] . '" '; ?> required /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="nom">Nom</label></div>
				<div class="cellule"><input id="champ_nom" name="nom" <?php if ($mode == MODE_MODIF) echo 'value="' . $user['nom'] . '" '; ?> required /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="pseudo">Pseudo</label></div>
				<div class="cellule"><input id="champ_pseudo" name="pseudo" <?php if ($mode == MODE_MODIF) echo 'value="' . $user['pseudo'] . '" '; ?> /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="adresse">Adresse</label></div>
				<div class="cellule">
					<input name="adresse_ligne1" placeholder="Ligne 1" <?php if ($mode == MODE_MODIF && count($adresse) > 0) echo 'value="' . $adresse[0]['ligne1'] . '" '; ?> /><br />
					<input name="adresse_ligne2" placeholder="Ligne 2" <?php if ($mode == MODE_MODIF && count($adresse) > 0) echo 'value="' . $adresse[0]['ligne2'] . '" '; ?> /><br />
					<input name="adresse_cp" placeholder="Code Postal" <?php if ($mode == MODE_MODIF && count($adresse) > 0) echo 'value="' . $adresse[0]['cp'] . '" '; ?> /><br />
					<input name="adresse_ville" placeholder="Ville" <?php if ($mode == MODE_MODIF && count($adresse) > 0) echo 'value="' . $adresse[0]['ville'] . '" '; ?> /><br />
					<input name="adresse_pays" placeholder="Pays" <?php if ($mode == MODE_MODIF && count($adresse) > 0) echo 'value="' . $adresse[0]['pays'] . '" '; ?> />
				</div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="telephone">Téléphone</label></div>
				<div class="cellule"><input name="telephone" <?php if ($mode == MODE_MODIF) echo 'value="' . $user['telephone'] . '" '; ?> /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="courriel">Courriel</label></div>
				<div class="cellule"><input name="courriel" <?php if ($mode == MODE_MODIF) echo 'value="' . $user['courriel'] . '" '; ?> /></div>
			</div>
			<?php
			if ($mode == MODE_MODIF && $fonctionnalites_statut['avatar']) {
			?>
				<div class="ligne">
					<div class="cellule intitule">Avatar</div>
					<div class="cellule">
						<?php
						if (is_null($user['avatar']))
							echo 'Aucun avatar';
						else
							echo '<input id="avatar_supp" name="avatar" type="checkbox" value="1" /><label for="avatar_supp">Supprimer l\'avatar de l\'adherent</label>';
						?>
					</div>
				</div>
			<?php
			}
			?>
			<div class="ligne">
				<div class="cellule intitule">Titre</div>
				<div class="cellule"><select name="titre">
						<?php
						$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'titre ORDER BY ordre ASC');
						if (!($requete === false)) {
							$titres = $requete->fetchAll();
							foreach ($titres as $titre) {
								echo '<option value="' . $titre['id'] . '" style="background-color: ' . $titre['couleur'] . ';"';
								if (($mode == MODE_MODIF && $titre['id'] == $user['titre']) || ($mode == MODE_AJOUT && $titre['defaut'])) echo ' selected';
								echo '>' . $titre['nom'] . '</option>';
							}
						} ?>
						<option value="0">Nouveau titre</option>
					</select></div>
				<div class="cellule">ou <input name="titre_autre" placeholder="Nouveau titre" /></div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="date_inscription">Date d'inscription</label></div>
				<div class="cellule">
					<input id="debut_cotis" name="date_inscription" type="date" onChange="afficher_verif_format_date('debut_cotis', 'erreur_debut_cotis'); <?php if ($mode == MODE_AJOUT) echo 'maj_date_fin_cotisation();'; ?>" value="<?php if ($mode == MODE_MODIF) echo $user['date_inscription'];
																																																										else echo date("Y-m-d"); ?>" <?php if ($mode == MODE_MODIF && $user['date_inscription'] == '0000-00-00') echo 'readonly'; ?> />
				</div>
				<div class="cellule" id="erreur_debut_cotis">(Format : AAAA-MM-JJ)</div>
				<div class="cellule">
					<input type="checkbox" name="non_membre" id="non_membre" value="1" <?php if ($mode == MODE_MODIF && $user['date_inscription'] == '0000-00-00') echo 'checked'; ?> onclick="membre_non_inscrit()" />
					<label for="non_membre">Non inscrit</label>
				</div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="date_fin_cotisation">Date de fin de cotisation</label></div>
				<div class="cellule">
					<input id="fin_cotis" name="date_fin_cotisation" type="date" onChange="afficher_verif_format_date('fin_cotis', 'erreur_fin_cotis');" value="<?php if ($mode == MODE_MODIF) echo $user['date_fin_cotisation'];
																																								else echo date("Y-m-d"); ?>" <?php if ($mode == MODE_MODIF && ($user['cotisation_infinie'] == 1 || $user['date_inscription'] == '0000-00-00')) echo 'readonly'; ?> />
				</div>
				<div class="cellule" id="erreur_fin_cotis">(Format : AAAA-MM-JJ)</div>
				<div class="cellule">
					<input type="checkbox" name="membre_a_vie" id="membre_a_vie" value="1" <?php if ($mode == MODE_MODIF && $user['cotisation_infinie'] == 1) echo 'checked'; ?> onclick="membre_vitam_eternam()" <?php if ($mode == MODE_MODIF && $user['date_inscription'] == '0000-00-00') echo 'disabled'; ?> />
					<label for="membre_a_vie"> À vie</label>
				</div>
			</div>
			<div class="ligne">
				<div class="cellule intitule"><label name="com">Commentaire</label></div>
				<div class="cellule"><textarea name="com"><?php if ($mode == MODE_MODIF) echo $user['commentaire']; ?></textarea></div>
			</div>
			<?php if (false && $fonctionnalites_statut['se']) { //Désactivé pour le moment car non fonctionnel 
			?>
				<div class="ligne">
					<div class="cellule intitule"><label name="newsletter">Lettre d'information</label></div>
					<div class="cellule"><select multiple name="newsletter[]">
							<?php
							$nl_user = array();

							if ($mode == MODE_MODIF) {
								$requete = $pdo->query('SELECT id_ml FROM ' . $bdd_prefixe . 'ml_lien WHERE id_entite = ' . $id . ' AND is_titre = 0');
								$user_ml = $requete->fetchAll();

								foreach ($user_ml as $nl) {
									$nl_user[] = $nl['id_ml'];
								}
							}

							$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'ml WHERE newsletter = 1');
							if (!($requete === false)) {
								$newsletters = $requete->fetchAll();
								foreach ($newsletters as $nl) {
									echo '<option value="' . $nl['id'] . '"';
									if (in_array($nl['id'], $nl_user))
										echo ' selected';
									echo '>' . $nl['nom'] . '</option>';
								}
							}
							?>
						</select></div>
				</div>
			<?php } ?>
		</div>
		<?php if ($fonctionnalites_statut['intervenants']) { ?>
			<h3>Ajouter des enfants</h3>
			<p>Note : Ne pas laisser vide de champ vide ! Pour les dates de naissances, au pire, mettre 0000-00-00. Format : AAAA-MM-JJ.</p>
			<div class="formulaire" id="tab_enfants">
				<div class="ligne">
					<div class="cellule"><input name="nom_enfants[]" placeholder="Nom" /></div>
					<div class="cellule"><input name="prenom_enfants[]" placeholder="Prénom" /></div>
					<div class="cellule"><input name="classe_enfants[]" placeholder="Classe" /></div>
					<div class="cellule"><input name="date_enfants[]" placeholder="Date de naissance" /></div>
					<div class="cellule"><input type="button" onClick="ajouter_ligne();" value="+" /> <input type="button" onClick="supp_ligne(1);" value="-" /></div>
				</div>
			</div>
		<?php } ?>

		<p>
			<input type="hidden" name="envoi" value="1" />
			<input type="hidden" name="id" value="<?php echo ($mode == MODE_MODIF ? $id : 0); ?>" />
			<?php if ($mode == MODE_MODIF) { ?>
				<input id="bouton_valider" type="submit" name="modifier" value="Modifier" />
			<?php } else if ($mode == MODE_AJOUT) { ?>
				<input id="bouton_valider" type="submit" name="creer" value="Créer" />
				<input id="bouton_valider" type="submit" name="creer_cotisation" value="Créer puis cotiser" />
				<input id="bouton_valider" type="submit" name="creer_nouveau" value="Créer puis nouveau" />
			<?php } ?>
		</p>
	</form>

	<?php include('bas_page.php'); ?>
</body>

</html>