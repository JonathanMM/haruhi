<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(SAUVEGARDE);

if(!isset($_GET['nom']) || strlen($_GET['nom']) == 0)
{
    header('location: sauvegardes.php');
    exit();
}

unlink('sauvegardes/'.htmlspecialchars($_GET['nom']).'.sql');
ajouterSuccesNotification('Sauvegarde détruite');
header('location: sauvegardes.php');
exit();
?>