<?php
function resultatHumainDroitsEcriture($chemin)
{
    if (is_writable('../' . $chemin)) {
        return 'OUI';
    }

    return 'NON';
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Installateur d'Haruhi</title>
		<link rel="icon" type="image/png" href="../images/favicon.png" />

		<link rel="stylesheet" href="../principal.css" type="text/css" media="screen" />
	</head>
	<body>
		<form action="install.php" method="post">
			<h1>Installateur d'Haruhi</h1>
			<p>Attention, pour activer le service cron, donner les droits d'écriture sur le fichier config/cron-derniere-execution.txt, et appeler quotidiennement le fichier cron.php.</p>
			<h2>Vérification de la configuration</h2>
			<ul>
				<li>Droits d'écriture sur config/cron-derniere-execution.txt : <?php echo resultatHumainDroitsEcriture('config/cron-derniere-execution.txt'); ?>
				<li>Droits d'écriture sur images/avatar/ : <?php echo resultatHumainDroitsEcriture('images/avatar/'); ?>
				<li>Droits d'écriture sur mail/ : <?php echo resultatHumainDroitsEcriture('mail/'); ?>
				<li>Droits d'écriture sur sauvegardes/ : <?php echo resultatHumainDroitsEcriture('sauvegardes/'); ?>
				<li>Droits d'écriture sur vendor/ : <?php echo resultatHumainDroitsEcriture('vendor/'); ?>
			</ul>
			<div class="formulaire">
				<div class="ligne">
					<h2>Administrateur</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="admin_pseudo">Pseudo</label></div>
					<div class="cellule"><input name="admin_pseudo" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="admin_mdp">Mot de passe</label></div>
					<div class="cellule"><input type="password" name="admin_mdp" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="admin_mail">Courriel</label></div>
					<div class="cellule"><input name="admin_mail" type="email" required /></div>
				</div>
				<div class="ligne">
					<h2>Base de données</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="bdd_host">Serveur</label></div>
					<div class="cellule"><input name="bdd_host" value="localhost" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="bdd_login">Login</label></div>
					<div class="cellule"><input name="bdd_login" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="bdd_mdp">Mot de passe</label></div>
					<div class="cellule"><input type="password" name="bdd_mdp" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="bdd_name">Nom de la BDD</label></div>
					<div class="cellule"><input name="bdd_name" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="bdd_prefixe">Préfixe des tables</label></div>
					<div class="cellule"><input name="bdd_prefixe" value="haruhi_" /></div>
				</div>
				<div class="ligne">
					<h2>API Ovh</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Activer</div>
					<div class="cellule">
						<input type="radio" name="ovh_state" id="ovh_state_true" value="true" /> <label for="ovh_state_true">Oui</label>
						<input type="radio" name="ovh_state" id="ovh_state_false" value="false" /> <label for="ovh_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="ovh_applicationKey">Application key</label></div>
					<div class="cellule"><input name="ovh_applicationKey" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="ovh_applicationSecret">Application secret</label></div>
					<div class="cellule"><input name="ovh_applicationSecret" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="ovh_endpoint">Endpoint</label></div>
					<div class="cellule"><input name="ovh_endpoint" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="ovh_consumerKey">Consumer key</label></div>
					<div class="cellule"><input name="ovh_consumerKey" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="ovh_domaineEmail">Domaine e-mail</label></div>
					<div class="cellule"><input name="ovh_domaineEmail" /></div>
				</div>
				<div class="ligne">
					<h2>Yuki</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Activer</div>
					<div class="cellule">
						<input type="radio" name="yuki_state" id="yuki_state_true" value="true" /> <label for="yuki_state_true">Oui</label>
						<input type="radio" name="yuki_state" id="yuki_state_false" value="false" /> <label for="yuki_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="yuki_courriel">Courriel</label></div>
					<div class="cellule"><input name="yuki_courriel" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="yuki_mdp">Mot de passe</label></div>
					<div class="cellule"><input type="password" name="yuki_mdp" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="yuki_server">Serveur</label></div>
					<div class="cellule"><input name="yuki_server" /></div>
				</div>
				<div class="ligne">
					<h2>XenForo</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Activer</div>
					<div class="cellule">
						<input type="radio" name="xenforo_state" id="xenforo_state_true" value="true" /> <label for="xenforo_state_true">Oui</label>
						<input type="radio" name="xenforo_state" id="xenforo_state_false" value="false" /> <label for="xenforo_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="xenforo_fileDir">Chemin du forum</label></div>
					<div class="cellule"><input name="xenforo_fileDir" /></div>
				</div>
				<div class="ligne">
					<h2>HelloAsso</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Activer</div>
					<div class="cellule">
						<input type="radio" name="helloasso_state" id="helloasso_state_true" value="true" /> <label for="helloasso_state_true">Oui</label>
						<input type="radio" name="helloasso_state" id="helloasso_state_false" value="false" /> <label for="helloasso_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="helloasso_cle">Clé API</label></div>
					<div class="cellule"><input name="helloasso_cle" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="helloasso_mdp">Mot de passe API</label></div>
					<div class="cellule"><input name="helloasso_mdp" /></div>
				</div>
				<div class="ligne">
					<h2>Fonctionnalités</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Système externe</div>
					<div class="cellule">
						<input type="radio" name="se_state" id="se_state_true" value="true" /> <label for="se_state_true">Oui</label>
						<input type="radio" name="se_state" id="se_state_false" value="false" /> <label for="se_state_false">Non</label>
						(doit être à oui si OVH ou Xenforo activé)
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Mikuru</div>
					<div class="cellule">
						<input type="radio" name="mikuru_state" id="mikuru_state_true" value="true" /> <label for="mikuru_state_true">Oui</label>
						<input type="radio" name="mikuru_state" id="mikuru_state_false" value="false" /> <label for="mikuru_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Intervenants</div>
					<div class="cellule">
						<input type="radio" name="intervenants_state" id="intervenants_state_true" value="true" /> <label for="intervenants_state_true">Oui</label>
						<input type="radio" name="intervenants_state" id="intervenants_state_false" value="false" /> <label for="intervenants_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Factures</div>
					<div class="cellule">
						<input type="radio" name="factures_state" id="factures_state_true" value="true" /> <label for="factures_state_true">Oui</label>
						<input type="radio" name="factures_state" id="factures_state_false" value="false" /> <label for="factures_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Stockage</div>
					<div class="cellule">
						<input type="radio" name="stockage_state" id="stockage_state_true" value="true" /> <label for="stockage_state_true">Oui</label>
						<input type="radio" name="stockage_state" id="stockage_state_false" value="false" /> <label for="stockage_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Calendrier</div>
					<div class="cellule">
						<input type="radio" name="calendrier_state" id="calendrier_state_true" value="true" /> <label for="calendrier_state_true">Oui</label>
						<input type="radio" name="calendrier_state" id="calendrier_state_false" value="false" /> <label for="calendrier_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Avatar</div>
					<div class="cellule">
						<input type="radio" name="avatar_state" id="avatar_state_true" value="true" /> <label for="avatar_state_true">Oui</label>
						<input type="radio" name="avatar_state" id="avatar_state_false" value="false" /> <label for="avatar_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Carte de membre</div>
					<div class="cellule">
						<input type="radio" name="cartemembre_state" id="cartemembre_state_true" value="true" /> <label for="cartemembre_state_true">Oui</label>
						<input type="radio" name="cartemembre_state" id="cartemembre_state_false" value="false" /> <label for="cartemembre_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Nolife (imports d'informations)</div>
					<div class="cellule">
						<input type="radio" name="nolife_state" id="nolife_state_true" value="true" /> <label for="nolife_state_true">Oui</label>
						<input type="radio" name="nolife_state" id="nolife_state_false" value="false" /> <label for="nolife_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Trésorerie</div>
					<div class="cellule">
						<input type="radio" name="tresorerie_state" id="tresorerie_state_true" value="true" /> <label for="tresorerie_state_true">Oui</label>
						<input type="radio" name="tresorerie_state" id="tresorerie_state_false" value="false" /> <label for="tresorerie_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<div class="cellule intitule">Serveur Oauth2</div>
					<div class="cellule">
						<input type="radio" name="oauth_state" id="oauth_state_true" value="true" /> <label for="oauth_state_true">Oui</label>
						<input type="radio" name="oauth_state" id="oauth_state_false" value="false" /> <label for="oauth_state_false">Non</label>
					</div>
				</div>
				<div class="ligne">
					<h2>Autres infos</h2>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="infos_nomasso">Nom de l'association</label></div>
					<div class="cellule"><input name="infos_nomasso" value="" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="infos_siteasso">Site web de l'association</label></div>
					<div class="cellule"><input name="infos_siteasso" value="http://" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="infos_adressemail">Adresse mail de l'association</label></div>
					<div class="cellule"><input name="infos_adressemail" value="" type="email" required /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="infos_adresseasso">Adresse de l'association</label></div>
					<div class="cellule"><textarea name="infos_adresseasso" required></textarea></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label for="envoyerMails">Activer l'envoi des mails</label></div>
					<div class="cellule"><input type="checkbox" name="envoyerMails" id="envoyerMails" value="1" /></div>
				</div>
				<div class="ligne">
					<input id="bouton_valider" type="submit" value="Installer" />
				</div>
		</form>
	</body>
</html>
