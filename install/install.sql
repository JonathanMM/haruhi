CREATE TABLE IF NOT EXISTS `haruhi_adherents` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `numero` int(11) NOT NULL DEFAULT 0,
  `prenom` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `pseudo` varchar(128) NULL,
  `adresse` text DEFAULT NULL,
  `telephone` varchar(32) DEFAULT NULL,
  `courriel` varchar(512) NOT NULL,
  `avatar` varchar(24) DEFAULT NULL,
  `cartemembre` varchar(256) DEFAULT NULL,
  `id_membre` int(11) NOT NULL,
  `titre` int(11) NOT NULL,
  `date_inscription` date NOT NULL,
  `date_fin_cotisation` date DEFAULT NULL,
  `cotisation_infinie` tinyint(1) NOT NULL DEFAULT '0',
  `commentaire` text DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_archives_ml` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `titre` varchar(256) NOT NULL,
  `date` datetime NOT NULL,
  `id_ml` int(11) NOT NULL,
  `expediteur` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `id_message_serveur` varchar(128) NOT NULL,
  `id_reponse_serveur` varchar(128) NOT NULL,
  `id_citation_serveur` varchar(128) NOT NULL,
  `type_contenu` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_calendrier` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL UNIQUE,
  `titre` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL,
  `lieu` varchar(512) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `type` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_dossiers` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `message` text NOT NULL,
  `intervenant` int(11) NOT NULL,
  `enfant` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_enfants` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `naissance` date NOT NULL,
  `id_intervenant` int(11) NOT NULL,
  `classe` varchar(16) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_factures` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `adherent` int(11) NOT NULL,
  `somme` float NOT NULL,
  `date` date NOT NULL,
  `payement` varchar(128) NOT NULL,
  `lien` varchar(1024) DEFAULT NULL,
  `type` int(2) NOT NULL COMMENT '0: Cotisation, 1: Don',
  `visibilite` tinyint(1) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_factures_type` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `externe` tinyint(1) NOT NULL DEFAULT '0',
  `phrase` text,
  `cotisation` tinyint(1) NOT NULL,
  `duree` int(11) DEFAULT NULL,
  `unite` tinyint(1) DEFAULT NULL,
  `debut_periode` date DEFAULT NULL,
  `fin_periode` date DEFAULT NULL,
  `termine_periode` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE `haruhi_factures_details` (
  `id` int(11) NOT NULL,
  `asso` text,
  `nom_adherent` varchar(256) DEFAULT NULL,
  `adresse_adherent` text,
  `signature` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_fichiers` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom` varchar(1024) NOT NULL,
  `taille` int(12) NOT NULL,
  `auteur` int(11) NOT NULL,
  `version` int(11) NOT NULL,
  `nouvelle_version` int(11) NOT NULL,
  `visibilite` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_lien_archives` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_message` int(11) NOT NULL,
  `id_fichier` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_membres` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `pseudo` varchar(128) NOT NULL,
  `mdp` varchar(512) NOT NULL,
  `mail` varchar(512) NOT NULL,
  `type` int(2) NOT NULL COMMENT '0: rien, 1: boite mail, 2: alias',
  `mikuru` varchar(64) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_ml` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom` varchar(256) NOT NULL,
  `adresse` varchar(256) NOT NULL,
  `classe` VARCHAR(32) NULL,
  `newsletter` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `haruhi_ml_lien` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_ml` int(11) NOT NULL,
  `id_entite` int(11) NOT NULL,
  `is_titre` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_titre` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom` varchar(128) NOT NULL,
  `permission` int(11) NOT NULL,
  `visibilite` int(11) NOT NULL,
  `couleur` varchar(16) NOT NULL,
  `ordre` int(2) NOT NULL,
  `defaut` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ;

CREATE TABLE `haruhi_logs_mail` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `titre` text NOT NULL,
  `contenu` text NOT NULL,
  `destinataire` varchar(512) NOT NULL,
  `idMembre` int(11) NOT NULL,
  `contexte` varchar(64) NOT NULL,
  `date` datetime NOT NULL,
  `envoye` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `haruhi_ml_externe` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `id_ml` int(11) NOT NULL,
  `courriel` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `haruhi_calendrier_categories` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom` varchar(128) NOT NULL,
  `couleur` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `haruhi_calendrier_permissions` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `idCategorie` int(11) NOT NULL,
  `idTitre` int(11) NOT NULL,
  `lecture` tinyint(1) NOT NULL,
  `ecriture` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `haruhi_calendrier_participants` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `idEvenement` int(11) NOT NULL,
  `idMembre` int(11) NOT NULL,
  `estOrganisateur` tinyint(1) NOT NULL,
  `reponse` tinyint(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_etiquettes` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `label` varchar(16) NOT NULL,
  `couleur` varchar(7) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_exercices` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `etiquettes` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_justificatifs` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `idMouvement` int(11) NOT NULL,
  `typePiece` tinyint(4) NOT NULL,
  `idPiece` int(11) NOT NULL,
  `description` varchar(256) NOT NULL,
  `montant` decimal(8,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_liens_mouvement_etiquette` (
  `idMouvement` int(11) NOT NULL,
  `idEtiquette` int(11) NOT NULL,
  PRIMARY KEY (`idMouvement`,`idEtiquette`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `haruhi_tresorerie_mouvement` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `date` date NOT NULL,
  `label` varchar(512) NOT NULL,
  `credit` decimal(8,2) DEFAULT NULL,
  `debit` decimal(8,2) DEFAULT NULL,
  `nbPieces` int(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `haruhi_configuration` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cle` varchar(32) NOT NULL UNIQUE KEY,
  `valeur` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `haruhi_configuration` (`id`, `cle`, `valeur`) VALUES
(1, 'modeleMailHtml', '<!DOCTYPE html>\r\n<html>\r\n    <title></title>\r\n    <body>\r\n        <h1>Haruhi</h1>\r\n        <p>{contenu}</p>\r\n    </body>\r\n</html>\r\n');

-- Oauth2 : Création des tables nécessaires au serveur
CREATE TABLE haruhi_oauth_clients (
  client_id             VARCHAR(80)   NOT NULL,
  client_secret         VARCHAR(80),
  redirect_uri          VARCHAR(2000),
  grant_types           VARCHAR(80),
  scope                 VARCHAR(4000),
  user_id               VARCHAR(80),
  nom                   VARCHAR(64),
  PRIMARY KEY (client_id)
);

CREATE TABLE haruhi_oauth_access_tokens (
  access_token         VARCHAR(40)    NOT NULL,
  client_id            VARCHAR(80)    NOT NULL,
  user_id              VARCHAR(80),
  expires              TIMESTAMP      NOT NULL,
  scope                VARCHAR(4000),
  PRIMARY KEY (access_token)
);

CREATE TABLE haruhi_oauth_authorization_codes (
  authorization_code  VARCHAR(40)     NOT NULL,
  client_id           VARCHAR(80)     NOT NULL,
  user_id             VARCHAR(80),
  redirect_uri        VARCHAR(2000),
  expires             TIMESTAMP       NOT NULL,
  scope               VARCHAR(4000),
  id_token            VARCHAR(1000),
  PRIMARY KEY (authorization_code)
);

CREATE TABLE haruhi_oauth_refresh_tokens (
  refresh_token       VARCHAR(40)     NOT NULL,
  client_id           VARCHAR(80)     NOT NULL,
  user_id             VARCHAR(80),
  expires             TIMESTAMP       NOT NULL,
  scope               VARCHAR(4000),
  PRIMARY KEY (refresh_token)
);

-- Option se souvenir de moi
CREATE TABLE `haruhi_membresToken` (
  `id` int(11) NOT NULL,
  `token` varchar(36) COLLATE utf8_bin NOT NULL,
  `userId` int(11) NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

ALTER TABLE `haruhi_membresToken`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token` (`token`);

ALTER TABLE `haruhi_membresToken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;