<?php
if(!file_exists('../vendor/autoload.php'))
{
	mkdir('../vendor/composer');
    $composerPhar = new Phar("../composer.phar");
    $composerPhar->extractTo('../vendor/composer');
}
require_once('../vendor/composer/vendor/autoload.php');

use Composer\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;

chdir('../');
$input = new ArrayInput(array('command' => 'install'));
$application = new Application();
$application->run($input);

echo 'Installation des dépendances réussites. Vous pouvez fermer cette fenêtre.';
?>
