<?php
require_once '../fonctions.php';

function def($key)
{
    return (isset($_POST[$key]) ? $_POST[$key] : 'false');
}

//TODO : À faire la partie ovh
$contenu = '<?php

	$host = "' . $_POST['bdd_host'] . '";
	$username = "' . $_POST['bdd_login'] . '";
	$password = "' . $_POST['bdd_mdp'] . '";
	$bdd_name = "' . $_POST['bdd_name'] . '";
	$bdd_prefixe = "' . $_POST['bdd_prefixe'] . '";

	//OVH
	$applicationKey_ovh = "' . $_POST['ovh_applicationKey'] . '";
	$applicationSecret_ovh = "' . $_POST['ovh_applicationSecret'] . '";
	$endpoint_ovh = "' . $_POST['ovh_endpoint'] . '";
	$consumerKey_ovh = "' . $_POST['ovh_consumerKey'] . '";

	$domaineEmail_ovh = "' . $_POST['ovh_domaineEmail'] . '";

	//Yuki
	$serveur_yuki = "' . $_POST['yuki_server'] . '";
	$courriel_yuki = "' . $_POST['yuki_courriel'] . '";
	$mdp_yuki = "' . $_POST['yuki_mdp'] . '";

	//Mikuru
	$adresse_mikuru = "";

	//Questions tutorats :
	$questions_tutorat = array();

	//XenForo
	$fileDir_xenforo = "' . $_POST['xenforo_fileDir'] . '";

	//HelloAsso
	$cle_helloasso = "' . $_POST['helloasso_cle'] . '";
	$mdp_helloasso = "' . $_POST['helloasso_mdp'] . '";

	//Fonctionnalités
	$fonctionnalites_statut = [
		"se" => ' . def('se_state') . ',
		"ovh" => ' . def('ovh_state') . ',
		"xenforo" => ' . def('xenforo_state') . ',
		"yuki" => ' . def('yuki_state') . ',
		"mikuru" => ' . def('mikuru_state') . ',
		"intervenants" => ' . def('intervenants_state') . ',
		"factures" => ' . def('factures_state') . ',
		"stockage" => ' . def('stockage_state') . ',
		"calendrier" => ' . def('calendrier_state') . ',
		"avatar" => ' . def('avatar_state') . ',
		"cartemembre" => ' . def('cartemembre_state') . ',
		"nolife" => ' . def('nolife_state') . ',
		"helloasso" => ' . def('helloasso_state') . ',
		"tresorerie" => ' . def('tresorerie_state') . ',
		"oauth" => ' . def('oauth_state') . '
	];

	//Infos diverses
	$infos_nomasso = "' . $_POST['infos_nomasso'] . '";
	$infos_siteasso = "' . $_POST['infos_siteasso'] . '";
	$infos_adresseasso = "' . $_POST['infos_adresseasso'] . '";
	$infos_adressemail = "' . $_POST['infos_adressemail'] . '";

	//Configuration envoi des mails
	$envoyerMails = ' . ((isset($_POST['envoyerMails']) && $_POST['envoyerMails'] == '1') ? 'true' : 'false') . ';
?>';

$fichier = fopen('../infos_connexion.php', 'w');
fwrite($fichier, $contenu);
fclose($fichier);

//On va créer les templates de courriel
$destination = '../mail/';
if (!is_dir($destination)) {
    $source = '../modelemail/';
    mkdir($destination);
    $modelemail = opendir($source);
    while (false !== ($nomFichier = readdir($modelemail))) {
        if ($nomFichier != '.' && $nomFichier != '..') {
            copy($source . $nomFichier, $destination . $nomFichier);
        }

    }
}

//Il faut encore remplir la base de données
try
{
    $bdd_pdo_dns = 'mysql:host=' . $_POST['bdd_host'] . ';dbname=' . $_POST['bdd_name'];
    $pdo = new PDO($bdd_pdo_dns, $_POST['bdd_login'], $_POST['bdd_mdp']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Renvoi des exceptions lors d'erreurs
} catch (Exception $e) {
    error("Connexion à MySQL impossible : ", $e->getMessage());
}

try
{
    $pdo->beginTransaction();

    $requete_sql = str_replace('haruhi_', $_POST['bdd_prefixe'], file_get_contents('install.sql'));
    foreach (explode(';', $requete_sql) as $sql) {
        if (!is_null($sql) && strlen(trim($sql)) > 1) {
            $pdo->exec($sql);
        }

    }

    //On créer les premières données
    //Titre
    $pdo->exec("INSERT INTO `" . $_POST['bdd_prefixe'] . "titre` (`id`, `nom`, `permission`, `visibilite`, `couleur`, `ordre`) VALUES
	(1, 'Administrateur', 16383, 3, '#46b8f1', 1)");

    //Membre
    $pdo->exec("INSERT INTO `" . $_POST['bdd_prefixe'] . "membres` (`id`, `pseudo`, `mdp`, `mail`, `type`, `mikuru`) VALUES
	(1, '" . $_POST['admin_pseudo'] . "', '" . hash('sha512', $_POST['admin_mdp']) . "', '" . $_POST['admin_mail'] . "', 0, '')");

    //Adhérent
    $pdo->exec("INSERT INTO `" . $_POST['bdd_prefixe'] . "adherents` (`id`, numero, `prenom`, `nom`, adresse, `courriel`, telephone, `id_membre`, `titre`, `date_inscription`, `date_fin_cotisation`) VALUES
	(1, 1, '" . $_POST['admin_pseudo'] . "', 'Admin', NULL, '" . $_POST['admin_mail'] . "', NULL, 1, 1, CURRENT_DATE(), CURRENT_DATE())") or die();

    $pdo->commit();

    ajouterSuccesNotification('Installation terminée, n\'oubliez pas de supprimer le dossier install !');

    afficherNotification();

    echo 'Il reste néanmoins les dépendances à installer. Pour cela, deux solutions :<br />';
    echo 'Soit, depuis un accès console, executer la commande : php composer.phar install<br />';
    echo 'Soit, aller sur <a href="dependances.php" target="_blank">cette page</a>. Attention, l\'opération peut durer quelques minutes !<br />';

    echo '<a href="..">Aller à l\'accueil</a>';
} catch (Exception $e) {
    $pdo->rollBack();
    throw $e;
}
