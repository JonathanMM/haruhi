function ouvrir_mois(num)
{
	var ul_event = document.getElementsByTagName('ul');
	var j = 0;
	for(var i = 0; i < ul_event.length; i++)
	{
		if(ul_event[i].getAttribute('class') == 'event')
		{
            if(j == num)
                ul_event[i].style.width = 'auto';
            else
                ul_event[i].style.width = '0px';
            j++;
		}
	}
}

function generer_calendrier(annee)
{
	var aujourdhui = new Date();
	if(annee == undefined || annee == 0)
		annee = aujourdhui.getFullYear() - (aujourdhui.getMonth() < 8 ? 1 : 0);
	var premierJourCalendrier = new Date(annee, 8, 1);
	var ul = document.getElementById('liste_calendrier');
	var nom_mois = new Array('Septembre', 'Octobre', 'Novembre', 'Décembre', 'Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août');
	var nom_jour = new Array('Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa');
	var array_event = getArrayEvent();
	var j_array_event = 0;
	var an_debut;
	if(premierJourCalendrier.getMonth() >= 8) //Septembre
		an_debut = premierJourCalendrier.getFullYear();
	else
		an_debut = premierJourCalendrier.getFullYear() - 1;

	for(var i = 0; i < 12; i++)
	{
		var an;
		if(i >= 4)
			an = an_debut + 1;
		else
			an = an_debut;

		var li_mois = document.createElement('li'); //On va créer la liste du mois
		var ul_li_mois = document.createElement('ul');
		var li_event = document.createElement('li'); //Et pour les events du mois
		var ul_li_event = document.createElement('ul');
		li_mois.innerHTML = '<span class="nom_mois">' + nom_mois[i] + ' ' + an + '</span>'; //On met le mois
		ul_li_mois.setAttribute('class', 'mois');
		li_event.setAttribute('class', 'event');
		ul_li_event.setAttribute('class', 'event');
		li_mois.setAttribute('onclick', 'ouvrir_mois(' + i + ')');
		//On va trouver le nombre de jour
		if(i == 0 || i == 2 || i == 7 || i == 9)
			var nb_jour = 30;
		else if(i == 5) //Février
		{
			if(((an % 4 == 0) && (an % 100 != 0)) || (an % 400 == 0)) //bisextile
				var nb_jour = 29;
			else
				var nb_jour = 28;
		} else
			var nb_jour = 31;

		var premier = new Date(); //On va trouver le premier
		premier.setDate(1);
		premier.setMonth((i + 8) % 12);
		premier.setYear(an);
		i_jour = premier.getDay();
		for(var j = 1; j <= nb_jour; j++)
		{
			var nouveau_li = document.createElement('li');
			nouveau_li.innerHTML = nom_jour[i_jour] + ' ' + j;
			if(i_jour == 0) //Dimanche
				nouveau_li.classList.add("dim");
			
			if((i + 8) % 12 == aujourdhui.getMonth() && j == aujourdhui.getDate() && 
				((i <= 4 && premierJourCalendrier.getFullYear() == aujourdhui.getFullYear()) ||
				(i > 4 && premierJourCalendrier.getFullYear() + 1 == aujourdhui.getFullYear())))
				nouveau_li.classList.add('today');
			
			//On regarde pour l'evenement
			var nouveau_li_event = document.createElement('li');
			while(j_array_event < array_event.length && array_event[j_array_event][0] == i && array_event[j_array_event][1] == j) //Event
			{
				nouveau_li.classList.add('evenement');
				nouveau_li.classList.remove('dim');
				nouveau_li_event.innerHTML += '<a href="lire_evenement.php?id='+array_event[j_array_event][2]+'">'+array_event[j_array_event][3]+'</a>';
				j_array_event++;
			}
			i_jour = (i_jour + 1) % 7;
			ul_li_mois.appendChild(nouveau_li);
			ul_li_event.appendChild(nouveau_li_event);
		}
		li_mois.appendChild(ul_li_mois);
		ul.appendChild(li_mois);
		li_event.appendChild(ul_li_event);
		ul.appendChild(li_event);
	}
}