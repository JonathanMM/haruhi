document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('select.changer-participation').forEach(function(elt) {
        elt.addEventListener('change', function(event) {
            var select = event.target;
            var idEvenement = select.dataset.id;
            var reponse = select.value;

            var xhr = new XMLHttpRequest();
            xhr.open('get', 'api.php?action=repondreInvitation&evenement='+idEvenement+'&reponse='+reponse);
            xhr.send();
        });
    });
});