var correspondanceHelloAsso = {
	'Formule': 'type',
	'Type': 'type',
	'Date': 'date',
	'Montant adhésion': 'somme',
	'Moyen de paiment': 'payement',
	'Attestation': 'lien',
	'Prénom': 'prenom',
	'Nom': 'nom',
	'Adresse': 'adresse_ligne1',
	'Code Postal': 'adresse_cp',
	'Ville': 'adresse_ville',
	'Pays': 'adresse_pays',
	'Adresse acheteur': 'adresse_ligne1',
	'Code Postal acheteur': 'adresse_cp',
	'Ville acheteur': 'adresse_ville',
	'Pays acheteur': 'adresse_pays',
	'Email': 'courriel',
	'Commentaire': 'commentaire',
	'Champ additionnel: Pseudo sur le forum': 'pseudo',
	'Champ additionnel: Pseudo': 'pseudo'
};

//Table adhérent : id 	prenom 	nom 	pseudo 	adresse 	telephone 	courriel 	id_membre 	titre 	date_inscription 	date_fin_cotisation 	commentaire
//Table facture :  id 	adherent 	somme 	date 	payement 	lien 	type	visibilite
var options = //key = name, value = label
		{
			_null: {label: '', type: 'null'},
			nom: {label: 'Nom', type: 'string'},
			prenom: {label: 'Prénom', type: 'string'},
			pseudo: {label: 'Pseudo', type: 'string'},
			adresse_ligne1: {label: 'Adresse Ligne 1', type: 'string'},
			adresse_ligne2: {label: 'Adresse Ligne 2', type: 'string'},
			adresse_cp: {label: 'Code Postal', type: 'string'},
			adresse_ville: {label: 'Ville', type: 'string'},
			adresse_pays: {label: 'Pays', type: 'string'},
			telephone: {label: 'Téléphone', type: 'string'},
			courriel: {label: 'Courriel', type: 'email'},
			date_inscription: {label: 'Date d\'inscription', type: 'date'},
			date_fin_cotisation: {label: 'Date de fin de cotisation', type: 'date'},
			commentaire: {label: 'Commentaire', type: 'string'},
			somme: {label: 'Montant', type: 'number'},
			date: {label: 'Date de facturation', type: 'date'},
			payement: {label: 'Moyen de payement', type: 'string', defaut: 'HelloAsso'},
			lien: {label: 'Lien du reçu', type: 'string'},
			titre: {label: 'Titre', type: 'titre'},
			type: {label: 'Type de facture', type: 'type_facture'}
		};

function afficherFormulaireChoixDefaut()
{
	var formulaire = document.getElementById('formulaire');

	var divLigne = document.createElement('div');
	divLigne.className = 'ligne';

	var divLabel = document.createElement('div');
	divLabel.className = 'cellule intitule';
	divLabel.innerHTML = 'Valeurs par défauts : ';
	divLigne.appendChild(divLabel);

	formulaire.appendChild(divLigne);

	for(var key in options)
	{
		var infos = options[key];
		if(infos.type != 'null')
		{
			divLigne = document.createElement('div');
			divLigne.className = 'ligne';

			divLabel = document.createElement('div');
			divLabel.className = 'cellule intitule';
			divLabel.innerHTML = infos.label+' : ';
			divLigne.appendChild(divLabel);

			var divSelect = document.createElement('div');
			divSelect.className = 'cellule';
			var elementForm;
			if(infos.type == 'string')
			{
				elementForm = document.createElement('input');
				if(infos.defaut !== undefined)
					elementForm.value = infos.defaut;
			}
			else if(infos.type == 'email')
			{
				elementForm = document.createElement('input');
				elementForm.setAttribute('type', 'email');
			}
			else if(infos.type == 'number')
			{
				elementForm = document.createElement('input');
				elementForm.setAttribute('type', 'number');
			}
			else if(infos.type == 'date')
			{
				elementForm = ajouterOption({'__auto': 'Automatique', '__aujourdhui': 'Aujourd\'hui', '__date_facturation': 'Date de facturation'});
			}
			else if(infos.type == 'type_facture')
			{
				elementForm = ajouterOption(typesFacture);
			}
			else if(infos.type == 'titre')
			{
				elementForm = ajouterOption(titresMembre, titreParDefaut);
			}
			elementForm.setAttribute('name', 'default_'+key);
			divSelect.appendChild(elementForm);
			divLigne.appendChild(divSelect);

			formulaire.appendChild(divLigne);
		}
	}
}

function ajouterOption(options, selected)
{
	var select = document.createElement('select');
	for(var key in options)
	{
		var option = options[key];
		var tagOption = document.createElement('option');
		tagOption.value = key;
		if(selected != undefined && key == selected)
			tagOption.selected = true;
		tagOption.innerHTML = option;
		select.appendChild(tagOption);
	}
	return select;
}

function faireSelect(i, nomColonne)
{
	var select = document.createElement('select');
	select.setAttribute('name', 'titre_'+i);
	for(var key in options)
	{
		var infos = options[key];
		var label = infos.label;
		var option = document.createElement('option');
		option.value = key;
		option.innerHTML = label;
		if(
			(correspondanceHelloAsso[nomColonne] != undefined && correspondanceHelloAsso[nomColonne] == key) ||
			(nomColonne.toLowerCase() == label.toLowerCase())
		)
			option.setAttribute('selected', true);
		select.appendChild(option);
	}
	return select;
}

function chargementFichier(evt)
{
	var fichier = evt.target.files[0]; //On récupère le fichier
	var fileInput = document.getElementById("fichier_import");
	fileInput.style.display = 'none';
	//fichier.name : Nom du fichier
	//fichier.type : Type du fichier
	//fichier.size : Taille du fichier
	//fichier.lastModifiedDate : Date de dernière modification
	var fichierInfo = document.getElementById('fichier_infos');
	fichierInfo.innerHTML = fichier.name+' ('+fichier.size.toLocaleString()+'o)';

	//On récupère le contenu du fichier
	var reader = new FileReader();
	reader.onload = function(c)
	{
		var contenu = c.target.result;
		var lignes = contenu.split("\n");
		var entete = lignes[0].trim();
		var colonnes = entete.split(";");
		var infos = document.getElementById('formulaire');

		var divLigne = document.createElement('div');
		divLigne.className = 'ligne';

		var divLabel = document.createElement('div');
		divLabel.className = 'cellule intitule';
		divLabel.innerHTML = 'Lignes : ';
		divLigne.appendChild(divLabel);

		var divSelect = document.createElement('div');
		divSelect.className = 'cellule';
		divSelect.innerHTML = (lignes.length - 1);
		divLigne.appendChild(divSelect);

		infos.appendChild(divLigne);
		for(var i in colonnes)
		{
			var label = colonnes[i];
			var divLigne = document.createElement('div');
			divLigne.className = 'ligne';

			var divLabel = document.createElement('div');
			divLabel.className = 'cellule intitule';
			divLabel.innerHTML = label;
			divLigne.appendChild(divLabel);

			var divSelect = document.createElement('div');
			divSelect.className = 'cellule';
			divSelect.appendChild(faireSelect(i, label));
			divLigne.appendChild(divSelect);

			infos.appendChild(divLigne);
		}

		afficherFormulaireChoixDefaut();
	};
	reader.readAsText(fichier, "UTF-8");
}

window.addEventListener('load', function() {
	var fileInput = document.getElementById("fichier_import");
	fileInput.addEventListener('change', chargementFichier, false);
});
