var communtateurs = {};

function communtateur_creer(idCommutateur, valeur, idDom)
{
    //on initialise la structure
    if(communtateurs[idCommutateur] == undefined)
    {
        communtateurs[idCommutateur] = {};
        communtateurs[idCommutateur][valeur] = [];
    }
    else if(communtateurs[idCommutateur][valeur] == undefined)
        communtateurs[idCommutateur][valeur] = [];

    //On attache l'événement
    document.getElementById(idDom)
        .addEventListener(
        'change',
        function()
        {
            communtateur_mettreValeur(idCommutateur, valeur);
        });
}

function communtateur_select(idCommutateur, idDom)
{
    //on initialise la structure
    if(communtateurs[idCommutateur] == undefined)
        communtateurs[idCommutateur] = {};

    //On récupère les valeurs possibles
    var select = document.getElementById(idDom);
    var options = select.options;
    for(var i in options)
    {
        var item = options[i];
        communtateurs[idCommutateur][item.value] = [];
    }

    //On attache l'événement
    select
        .addEventListener(
        'change',
        function(event)
        {
            communtateur_recupererValeurSelect(event, idCommutateur);
        });
}

function communtateur_souscrire(idCommutateur, valeur, idDom, action)
{
    if(communtateurs[idCommutateur] == undefined)
        console.error('Le commutateur '+idCommutateur+' n\'existe pas');
    else if(communtateurs[idCommutateur][valeur] == undefined)
        console.error('La valeur '+valeur+' pour le commutateur '+idCommutateur+' n\'existe pas');

    var obj = {obj: idDom, action: action};
    communtateurs[idCommutateur][valeur].push(obj);
}

function communtateur_recupererValeurSelect(event, idCommuntateur)
{
    var valeur = event.target.value;
    communtateur_mettreValeur(idCommuntateur, valeur);
}

function communtateur_mettreValeur(idCommutateur, valeur)
{
    if(communtateurs[idCommutateur] == undefined)
        console.error('Le commutateur '+idCommutateur+' n\'existe pas');
    else if(communtateurs[idCommutateur][valeur] == undefined)
        console.error('La valeur '+valeur+' pour le commutateur '+idCommutateur+' n\'existe pas');

    for(var i in communtateurs[idCommutateur][valeur])
    {
        var item = communtateurs[idCommutateur][valeur][i];
        if(item.action == 'visible')
            document.getElementById(item.obj).style.display = 'table-row';
        else if(item.action == 'cache')
            document.getElementById(item.obj).style.display = 'none';
    }
}
