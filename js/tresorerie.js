document.addEventListener('DOMContentLoaded', function() {
    document.getElementById('nouveau_mouvement_credit').addEventListener('change', evenement_credit_change);
    document.getElementById('nouveau_mouvement_debit').addEventListener('change', evenement_debit_change);
    document.getElementById('nouveau_mouvement_categories').addEventListener('change', evenement_ajout_categorie);
    document.getElementById('nouveau_mouvement_sauvegarder').addEventListener('click', creerMouvement);
    document.querySelectorAll('td').forEach(function(elt)
    { 
        elt.addEventListener('dblclick', evenement_double_clic);
        elt.querySelectorAll('.modifier-etiquettes').forEach(function(li) {
            li.querySelector('img').addEventListener('click', demarrer_modification_etiquettes);
        });
    });
    document.querySelectorAll('.supprimer-mouvement').forEach(function(elt) { elt.addEventListener('click', evenement_supprimer_mouvement); });
});

function evenement_credit_change(event)
{
    evenement_montant_change(event.target.value, 'nouveau_mouvement_debit');
}

function evenement_debit_change(event)
{
    evenement_montant_change(event.target.value, 'nouveau_mouvement_credit');
}

function evenement_montant_change(valeur, idElement)
{
    if(valeur == '')
        document.getElementById(idElement).removeAttribute('disabled');
    else
        document.getElementById(idElement).setAttribute('disabled', 'true');
}

function ajouter_li_etiquette(ul, idEtiquette, etiquette)
{
    var li = document.createElement('li');
    li.innerHTML = etiquette.nom;
    li.dataset.id = idEtiquette;
    li.classList.add('etiquette-'+idEtiquette);
    li.addEventListener('click', evenement_supprimer_categorie);
    ul.appendChild(li);
}

function evenement_ajout_categorie(event)
{
    var select = event.target;
    var idEtiquette = select.value;
    var etiquette = etiquettes[idEtiquette];
    var td = select.parentNode;

    //On l'ajoute à la liste
    var ul = td.querySelector('ul');
    ajouter_li_etiquette(ul, idEtiquette, etiquette);

    //Ainsi que dans l'input caché
    var input = td.querySelector('input');
    var nouvelleListe = input.value.split(',');
    nouvelleListe.push(idEtiquette);
    input.value = nouvelleListe.join(',');

    //Et on le retire du select
    select.remove(select.selectedIndex);
    //Que l'on remet sur le premier élémént
    select.selectedIndex = 0;
}

function ajouter_option_etiquette(select, idEtiquette, etiquette)
{
    var option = document.createElement('option');
    option.innerHTML = etiquette.nom;
    option.value = idEtiquette;
    select.appendChild(option);
}

function evenement_supprimer_categorie(event)
{
    var li = event.target;
    var idEtiquette = parseInt(li.dataset.id);
    var etiquette = etiquettes[idEtiquette];

    var td = li.parentNode.parentNode;
    var input = td.querySelector('input');
    var listeEtiquette = input.value.split(',');
    var nouvelleListe = listeEtiquette.filter(function(elt) { return elt != '' && parseInt(elt) != idEtiquette });
    input.value = nouvelleListe.join(',');

    var select = td.querySelector('select');
    ajouter_option_etiquette(select, idEtiquette, etiquette);

    li.parentNode.removeChild(li);
}

function formater_date(texte)
{
    if(texte.length == 0) return '';

    var coupe = texte.split('-');
    return coupe[2] + '/' + coupe[1] + '/' + coupe[0];
}

function creerMouvement()
{
    var libele = document.getElementById('nouveau_mouvement_nom').value;
    var date = document.getElementById('nouveau_mouvement_date').value;
    var credit = parseFloat(document.getElementById('nouveau_mouvement_credit').value);
    if(isNaN(credit))
        credit = 0;
    var debit = parseFloat(document.getElementById('nouveau_mouvement_debit').value);
    if(isNaN(debit))
        debit = 0;
    var listeEtiquettes = document.getElementById('nouveau_mouvement_categories_choisies').value;

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'api.php?action=creerMouvement', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    var formulaire =     'libele=' + encodeURIComponent(libele) +
                        '&date=' + encodeURIComponent(date) +
                        '&credit=' + encodeURIComponent(credit) +
                        '&debit=' + encodeURIComponent(debit) +
                        '&etiquettes=' + encodeURIComponent(listeEtiquettes);
    
    xhr.onreadystatechange = function()
    {
        if(this.readyState == XMLHttpRequest.DONE && this.status == 200)
        {
            var idMouvement = parseInt(JSON.parse(xhr.responseText));

            var tr = document.createElement('tr');
            tr.dataset.id = idMouvement;

            var td = document.createElement('td');
            td.innerHTML = formater_date(date);
            td.dataset.info = 'date';
            td.dataset.value = date;
            td.addEventListener('dblclick', evenement_double_clic);
            tr.appendChild(td);

            var td = document.createElement('td');
            td.innerHTML = libele;
            td.dataset.value = libele;
            td.dataset.info = 'label';
            td.addEventListener('dblclick', evenement_double_clic);
            tr.appendChild(td);

            var td = document.createElement('td');
            td.addEventListener('dblclick', evenement_double_clic);
            creer_liste_etiquette(td, listeEtiquettes);
            tr.appendChild(td);

            var td = document.createElement('td');
            if(credit != 0)
            {
                td.addEventListener('dblclick', evenement_double_clic);
                td.dataset.value = credit;
                td.dataset.info = 'credit';
                td.innerHTML = credit.toLocaleString('fr', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'EUR'});
            }
            tr.appendChild(td);

            var td = document.createElement('td');
            if(debit != 0)
            {
                td.addEventListener('dblclick', evenement_double_clic);
                td.dataset.debit = debit;
                td.dataset.info = 'debit';
                td.innerHTML = debit.toLocaleString('fr', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'EUR'});
            }
            tr.appendChild(td);

            var td = document.createElement('td');

            // Bouton voir fiche
            var a = document.createElement('a');
            a.href = 'voir_mouvement.php?id=' + idMouvement;

            var img = document.createElement('img');
            img.src = 'images/adherent_fiche.png';
            img.alt = 'Détails';
            img.title = 'Voir les détails';

            a.appendChild(img);
            td.appendChild(a);

            // Bouton suppression
            img = document.createElement('img');
            img.src = 'images/adherent_supprimer.png';
            img.classList.add('supprimer-mouvement');
            img.alt = 'Supprimer';
            img.title = 'Supprimer';
            img.addEventListener('click', evenement_supprimer_mouvement);
            td.appendChild(img);

            tr.appendChild(td);

            var ligneAjout = document.getElementById('ligne-nouveau-mouvement');
            ligneAjout.parentNode.insertBefore(tr, ligneAjout);

            //On remet à zéro le formulaire
            document.getElementById('nouveau_mouvement_nom').value = '';
            document.getElementById('nouveau_mouvement_date').value = '';
            document.getElementById('nouveau_mouvement_debit').value = '';
            document.getElementById('nouveau_mouvement_credit').value = '';
            document.getElementById('nouveau_mouvement_categories_choisies').value = '';
            document.getElementById('nouveau_mouvement_categories_liste').innerHTML = '';

            //On réactive les champs
            document.getElementById('nouveau_mouvement_debit').disabled = false;
            document.getElementById('nouveau_mouvement_credit').disabled = false;
            
            //Et on recrée le sélecteur de catégories
            var select = document.getElementById('nouveau_mouvement_categories');
            select.innerHTML = '';

            var option = document.createElement('option');
            option.innerHTML = 'Catégorie';
            option.disabled = true;
            select.appendChild(option);

            for(var idEtiquette in etiquettes)
            {
                var etiquette = etiquettes[idEtiquette];
                option = document.createElement('option');
                option.innerHTML = etiquette.nom;
                option.value = idEtiquette;
                select.appendChild(option);
            }

            select.selectedIndex = 0;
        }
    }
    xhr.send(formulaire);
}

function creer_liste_etiquette(td, listeEtiquettes)
{
    td.dataset.info = 'etiquettes';
    td.dataset.value = listeEtiquettes;
    var ul = document.createElement('ul');
    ul.classList.add('etiquettes-liste');

    var tableauEtiquette = listeEtiquettes.split(',');
    for(var idEtiquette in tableauEtiquette)
    {
        var idEtiquette = parseInt(tableauEtiquette[idEtiquette]);
        var etiquette = etiquettes[idEtiquette];
        if(etiquette != undefined)
            ajouter_li_etiquette(ul, idEtiquette, etiquette);
    }

    //On met le bouton de modification
    li = document.createElement('li');
    li.classList.add('modifier-etiquettes');
    var img = document.createElement('img');
    img.src = 'images/adherent_modifier.png';
    img.alt = 'Modifier';
    img.title = 'Modifier';
    img.addEventListener('click', demarrer_modification_etiquettes);
    li.appendChild(img);
    ul.appendChild(li);

    td.appendChild(ul);
}

function demarrer_modification_etiquettes(event)
{
    evenement_double_clic({target: event.target.parentNode.parentNode.parentNode}); //img -> li -> ul -> td
}

function evenement_double_clic(event)
{
    var td = event.target;
    if(td.dataset.info != undefined)
    {
        var info = td.dataset.info;
        var valeur = td.dataset.value;

        var input = document.createElement('input');
        switch(info)
        {
            case 'date':
                input.type = 'date';
                break;
            case 'label':
                input.type = 'text';
                break;
            case 'credit':
            case 'debit':
                input.type = 'number';
                input.min = 0;
                input.step = 0.01;
                input.style.textAlign = 'right';
                break;
            case 'etiquettes':
                input.type = 'hidden';
                break;
        }
        input.value = valeur;
        td.innerHTML = '';
        td.appendChild(input);

        if(info == 'etiquettes')
        {
            //On doit créer un select et un ul en plus
            var listeEtiquettes = valeur.split(',');

            var ul = document.createElement('ul');
            ul.classList.add('etiquettes-liste');
            ul.classList.add('categories_liste_modification');

            var select = document.createElement('select');
            var optionDisabled = document.createElement('option');
            optionDisabled.disabled = true;
            optionDisabled.selected = true;
            optionDisabled.innerHTML = 'Catégorie';
            select.appendChild(optionDisabled);
            select.selectedIndex = 0;
            select.addEventListener('change', evenement_ajout_categorie);

            for(var idEtiquette in etiquettes)
            {
                var etiquette = etiquettes[idEtiquette];
                if(listeEtiquettes.indexOf(idEtiquette) != -1) //Il est dans la liste !
                {
                    ajouter_li_etiquette(ul, idEtiquette, etiquette);
                } else { //Direction le select
                    ajouter_option_etiquette(select, idEtiquette, etiquette);
                }
            }

            td.appendChild(ul);
            td.appendChild(select);

            //Bouton de sauvegarde
            var img = document.createElement('img');
            img.src = 'images/sauvegarder.png';
            img.classList.add('sauvegarder-modifications');
            img.alt = 'Sauvegarder';
            img.title = 'Sauvegarder';
            img.addEventListener('click', sauvegarder_modifications_etiquette);
            td.appendChild(img);
        } else
        {   
            input.addEventListener('blur', evenement_blur_input);
            input.addEventListener('keyup', evenement_keyup_input);
        
            //Bug dans firefox sur le focus sur un input type=number
            setTimeout(function() {
                input.focus();
            }, 250);
        }
    }
}

function sauvegarder_modifications_etiquette(event)
{
    //Il faut trouver l'input
    var img = event.target;
    var td = img.parentNode;
    var input = td.querySelector('input');
    evenement_blur_input({target: input}); //On créer un pseudo event
}

function evenement_keyup_input(event)
{
    if(event.key == 'Enter')
        evenement_blur_input(event);
}

function evenement_blur_input(event)
{
    var input = event.target;
    var td = input.parentNode;
    if(td.dataset.info != undefined)
    {
        var info = td.dataset.info;
        var idMouvement = td.parentNode.dataset.id;
        var valeur = input.value;

        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'api.php?action=modifierMouvement', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        var formulaire =     'type=' + encodeURIComponent(info) +
                            '&valeur=' + encodeURIComponent(valeur) +
                            '&idMouvement=' + encodeURIComponent(idMouvement);
        
        xhr.onreadystatechange = function()
        {
            if(this.readyState == XMLHttpRequest.DONE && this.status == 200)
            {
                td.dataset.value = valeur;
                //On remet comme avant
                switch(info)
                {
                    case 'date':
                        td.innerHTML = formater_date(valeur);
                        break;
                    case 'label':
                        td.innerHTML = valeur;
                        break;
                    case 'credit':
                    case 'debit':
                        td.innerHTML = parseFloat(valeur).toLocaleString('fr', {minimumFractionDigits: 2, maximumFractionDigits: 2, style: 'currency', currency: 'EUR'});
                        break;
                    case 'etiquettes':
                        td.innerHTML = '';
                        creer_liste_etiquette(td, valeur);
                        break;
                }
            }
        };
        xhr.send(formulaire);
    }
}

function evenement_supprimer_mouvement(event)
{
    var tr = event.target.parentNode.parentNode; //button -> td -> tr
    var idMouvement = tr.dataset.id;

    if(window.confirm('Voulez-vous vraiment supprimer le mouvement #'+idMouvement+' ?'))
    {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', 'api.php?action=supprimerMouvement', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        var formulaire = 'idMouvement=' + encodeURIComponent(idMouvement);
        
        xhr.onreadystatechange = function()
        {
            if(this.readyState == XMLHttpRequest.DONE && this.status == 200)
            {
                //On efface la ligne
                tr.parentNode.removeChild(tr);
            }
        };

        xhr.send(formulaire);
    }
}
