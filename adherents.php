<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(LISTE_ADHERENT);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Adhérents</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="edition_facture.css" type="text/css" media="print">
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<nav id="panneau_page">
			<h2>Adhérents</h2>

			<?php if((int)$_SESSION['permission'] & TOUCHE_ADHERENT) { ?>
			<p><a href="form_adherent.php">Ajouter un adhérent</a></p>
			<?php } ?>

			<p>Recherche :</p>
			<form action="adherents.php" method="post">
				<input name="terme" placeholder="Nom, prénom ou pseudo" value="<?php if(isset($_POST['terme'])) echo htmlspecialchars($_POST['terme']); ?>" /><br />
				Catégorie : <br />
				<select multiple name="titre[]">
					<?php
					$requete = $pdo->query('SELECT id, nom FROM '.$bdd_prefixe.'titre');
					$titres = $requete->fetchAll();
					foreach($titres as $titre)
					{
						echo '<option value="'.$titre['id'].'"';
						if(isset($_POST['titre']) && count($_POST['titre']) > 0 && in_array($titre['id'], $_POST['titre']))
							echo ' selected ';
						echo '>'.$titre['nom'].'</option>';
					}
					?>
				</select><br />
				<input type="checkbox" value="1" name="view_address" id="view_address" <?php if(isset($_POST['view_address'])) echo 'checked'; ?> />
				<label for="view_address">
					Voir les adresses
				</label>
				<br />
				<input type="checkbox" value="1" name="view_expired" id="view_expired" <?php if(isset($_POST['view_expired'])) echo 'checked'; ?> />
				<label for="view_expired">
					Voir uniquement les cotisations expirées
				</label>
				<br />
				<!-- 	    <input type="checkbox" value="1" name="liste_ml" /> Afficher sous forme de liste de courriel<br /> -->
				<input type="submit" value="OK" />
			</form>

			<p>
				<a href="exportCSVadherents.php" download="adherents.csv">Export CSV</a>
			</p>
		</nav>

		<div id="cadre_stockage">
			<?php //On s'occupe des messages
			afficherNotification();
			?>
			<table>
				<tr><th>N° <a href="adherents.php?tri=numero&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=numero&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Prénom <a href="adherents.php?tri=prenom&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=prenom&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Nom <a href="adherents.php?tri=nom&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=nom&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Pseudo <a href="adherents.php?tri=pseudo&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=pseudo&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<?php if(isset($_POST['view_address']) && $_POST['view_address']) echo '<th>Adresse</th>'; ?>
					<!--<th>Téléphone</th>-->
					<th>Courriel</th>
					<th>Statut <a href="adherents.php?tri=statut&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=statut&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Date<br />inscription <a href="adherents.php?tri=di&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=di&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<th>Date de fin<br />de cotisation <a href="adherents.php?tri=df&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=df&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>
					<!--<th>Commentaires <a href="adherents.php?tri=com&amp;ordre=asc"><img src="images/tri_croissant.png" alt="↑" /></a> <a href="adherents.php?tri=com&amp;ordre=desc"><img src="images/tri_decroissant.png" alt="↓" /></a></th>-->
					<th>Actions</th></tr>
				<?php
				$conditionSQL = genererRequeteSQLListeAdherent(
					isset($_POST['terme']) ? $_POST['terme'] : NULL,
					isset($_POST['titre']) ? $_POST['titre'] : NULL,
					isset($_POST['view_expired']) && $_POST['view_expired'],
					isset($_GET['tri']) ? $_GET['tri'] : NULL,
					isset($_GET['ordre']) ? $_GET['ordre'] : NULL
				);
				$requete = $pdo->query('SELECT *, a.id AS id, a.nom AS nom, t.nom AS nom_titre FROM '.$bdd_prefixe.'adherents a LEFT JOIN '.$bdd_prefixe.'titre t ON a.titre = t.id '.$conditionSQL);
				if(!($requete === false))
				{
					$items = $requete->fetchAll();
					foreach($items as $donnees)
					{
						if(isset($_POST['liste_ml']) && $_POST['liste_ml'] == 1)
							echo $donnees['courriel'].'<br />';
						else
						{
							$jour = reste_jour($donnees['date_fin_cotisation']);
							echo '<tr';
							if($donnees['date_inscription'] == "0000-00-00")
								echo ' class="pas_membre" ';
							elseif($donnees['cotisation_infinie'] == 1 || $jour > 30)
								echo ' class="cotisation_ok" ';
							elseif($jour <= 30 && $jour > 7)
								echo ' class="cotisation_fin_loin" ';
							elseif($jour <= 7 && $jour >= 0)
								echo ' class="cotisation_fin_proche" ';
							else
								echo ' class="cotisation_fini" ';
							//echo ' style="background-color: '.$donnees['couleur'].';"';
							echo '><td style="text-align: center;">'.($donnees['numero'] == 0 ? '-' : $donnees['numero']).'</td>';
							echo '<td>'.$donnees['prenom'].'</td><td>'.$donnees['nom'].'</td>';
							echo '<td>'.$donnees['pseudo'].'</td>';
							if(isset($_POST['view_address']) && $_POST['view_address']) echo '<td>'.formater_adresse($donnees['adresse']).'</td>';
							//echo '<td>'.$donnees['telephone'].'</td>';
							echo '<td>'.$donnees['courriel'].'</td>';
							echo '<td>'.$donnees['nom_titre'].'</td>';
							echo '<td>'.($donnees['date_inscription'] == "0000-00-00" ? '<i>Non inscrit</i>' : formater_date($donnees['date_inscription'])).'</td>';
							echo '<td>';
							if($donnees['cotisation_infinie'] == 1)
								echo '<i>À vie</i>';
							elseif ($donnees['date_inscription'] == "0000-00-00")
								echo '';
							else 
								echo formater_date($donnees['date_fin_cotisation']);
							echo '</td>';	
							//echo '<td>'.$donnees['commentaire'].'</td>';
							echo '<td>';
							if((int)$_SESSION['permission'] & TOUCHE_ADHERENT)
								echo '<a href="form_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a> ';
							echo '<a href="voir_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_fiche.png" title="Fiche" alt="Fiche" /></a> ';
							//echo '<a href="enfant.php?id='.$donnees['id'].'"><img src="images/adherent_enfant.png" title="Enfants" alt="Enfants" /></a>';
							if((int)$_SESSION['permission'] & FAIRE_FACTURE)
								echo ' <a href="cotisation.php?id='.$donnees['id'].'"><img src="images/adherent_cotisation.png" title="Cotisation" alt="Cotisation" /></a>';
							if($donnees['id_membre'] == 0 && (int)$_SESSION['permission'] & TOUCHE_ADHERENT)
								echo ' <a href="creer_compte.php?id='.$donnees['id'].'"><img src="images/adherent_creer_compte.png" title="Créer un compte" alt="Créer un compte" /></a>';
							if((int)$_SESSION['permission'] & TOUCHE_ADHERENT)
							{
								echo '<a href="fusion_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_fusion.png" title="Fusionner" alt="Fusionner" /></a> ';
								echo '<a href="supp_adherent.php?id='.$donnees['id'].'"><img src="images/adherent_supprimer.png" title="Supprimer" alt="Supprimer" /></a> ';
							}
							echo '</td></tr>';
						}
					}
				}
				?>
			</table>

			<p>Nombre de résultats : <?php echo count($items); ?></p>
		</div>

		<?php include('bas_page.php'); ?>
	</body>
</html>
