<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(SAUVEGARDE);

sauvegarder();
ajouterSuccesNotification('Sauvegarde créée');
header('location: sauvegardes.php');
exit();
?>