<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);

if(isset($_POST['envoi']) && $_POST['envoi'] == 2)
{
	$id = intval($_POST['adherentDestinationId']);
	$numero = intval($_POST['numero']);
	$prenom = htmlspecialchars($_POST['prenom'], ENT_QUOTES);
	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
	$pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
	$adresse_fixe = htmlspecialchars($_POST['adresse'], ENT_QUOTES);
	$telephone = htmlspecialchars($_POST['telephone'], ENT_QUOTES);
	$courriel = htmlspecialchars($_POST['courriel'], ENT_QUOTES);
	$titre = intval($_POST['titre']);
	$date_fin_cotisation = htmlspecialchars($_POST['date_fin_cotisation'], ENT_QUOTES);

	$obj_date = new DateTime($date_fin_cotisation);
	$obj_auj = new DateTime();

	//On récupère l'ancien utilisateur
	$user_requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
	$user = $user_requete->fetch();

	$date_inscription = htmlspecialchars($_POST['date_inscription'], ENT_QUOTES);
	$commentaire = htmlspecialchars($_POST['com'], ENT_QUOTES);

	$adherent = array(
		'numero' => $numero,
		'prenom' => $prenom,
		'nom' => $nom,
		'pseudo' => $pseudo,
		'adresse' => $adresse_fixe,
		'telephone' => $telephone,
		'courriel' => $courriel,
		'titre' => $titre,
		'date_inscription' => $date_inscription,
		'date_fin_cotisation' => $date_fin_cotisation,
		'commentaire' => $commentaire
	);

	if(strlen($adherent['date_inscription']) == 0)
	{
		$adherent['date_inscription'] = '0000-00-00';
	}
	
	if(strlen($adherent['date_fin_cotisation']) == 0)
	{
		$adherent['date_fin_cotisation'] = '0000-00-00';
	}
	
	if(strlen($prenom) > 0 && strlen($nom) > 0)
	{
		$adherentSourceId = intval($_POST['adherentSourceId']);

		//On déplace ce qu'il faut déplacer
		deplacerElementsEntreAdherents($adherentSourceId, $id);

		//Tout est bon, on peut passer à la suppression de l'ancien adhérent
		supprimerAdherent(intval($_POST['adherentSourceId']));

		if($numero == 0 && $date_inscription != '0000-00-00')
			$numero = getNouveauNumeroAdherent();

		$pdo->exec('UPDATE '.$bdd_prefixe.'adherents SET numero = "'.$numero.'", prenom = "'.$prenom.'", nom = "'.$nom.'", pseudo = "'.$pseudo.'", adresse = "'.$adresse_fixe.'", telephone = "'.$telephone.'", courriel = "'.$courriel.'",
		titre = "'.$titre.'", date_inscription = "'.$date_inscription.'", date_fin_cotisation = "'.$date_fin_cotisation.'", commentaire = "'.$commentaire.'"'.($avatar_supp ? ', avatar = NULL' : '').' WHERE id = '.$id);

		//Et on appelle toutes les implémentations
		$classes = importerEvent('adherent');
		foreach($classes as $classe)
		{
			$classe::modifier($id, $adherent, $user);
		}
		ajouterSuccesNotification("L'adhérent a été fusionné avec succès.");
	} else
		ajouterErreurNotification("L'adhérent n'a pu être fusionné, les mentions suivantes étant obligatoires : Nom, Prénom.");
	
	header('location: adherents.php');
	exit();
}

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
    if(!isset($_POST['id']) || !isset($_POST['adherent']) || intval($_POST['id']) <= 0 || intval($_POST['adherent']) <= 0)
    {
        ajouterErreurNotification('Problème lors de la récupération des informations des adhérents à fusionner.');
        header('location: adherents.php');
        exit();
    }
    $adherentSourceId = intval($_POST['id']);
    $adherentDestinationId = intval($_POST['adherent']);
    $requeteSource = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$adherentSourceId);
    $adherentSource = $requeteSource->fetch();
    $requeteDestination = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$adherentDestinationId);
    $adherentDestination = $requeteDestination->fetch();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Fusion d'un adhérent</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<script type="text/javascript" src="js/form_adherent.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Fusion d'un adhérent</h2>

		<form action="fusion_adherent.php" method="post">
			<div class="formulaire">
				<div class="ligne">
                    <div class="cellule intitule"></div>
                    <div class="cellule">Adhérent source</div>
                    <div class="cellule">Informations fusionnées</div>
                    <div class="cellule">Adhérent destination</div>
                    <div class="cellule"></div>
                </div>
				<div class="ligne">
                    <div class="cellule intitule">Id</div>
                    <div class="cellule"><?php echo $adherentSource['id']; ?></div>
                    <div class="cellule"><?php echo $adherentDestination['id']; ?></div>
                    <div class="cellule"><?php echo $adherentDestination['id']; ?></div>
                    <div class="cellule"></div>
                </div>
                <?php
                    echo genererFormulaireFusion('numero', $adherentSource, $adherentDestination, 'Numéro');
                    echo genererFormulaireFusion('prenom', $adherentSource, $adherentDestination, 'Prénom', true);
                    echo genererFormulaireFusion('nom', $adherentSource, $adherentDestination, 'Nom', true);
                    echo genererFormulaireFusion('pseudo', $adherentSource, $adherentDestination, 'Pseudo');
                ?>
				<div class="ligne">
                    <div class="cellule intitule">Adresse</div>
                    <div class="cellule"><?php echo $adherentSource['adresse']; ?></div>
                    <div class="cellule"><textarea name="adresse"><?php echo getValeurFusionner('adresse', $adherentSource, $adherentDestination); ?></textarea></div>
                    <div class="cellule"><?php echo $adherentDestination['adresse']; ?></div>
                    <div class="cellule"></div>
				</div>
                <?php
                    echo genererFormulaireFusion('telephone', $adherentSource, $adherentDestination, 'Téléphone');
                    echo genererFormulaireFusion('courriel', $adherentSource, $adherentDestination, 'Courriel', false, '', 'email');
                ?>
				<div class="ligne">
                	<?php
						$titres = [];
						$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'titre ORDER BY ordre ASC');
						if(!($requete === false))
						{
                            $titresBDD = $requete->fetchAll();
							$titres = [];
							foreach($titresBDD as $titre)
								$titres[$titre['id']] = $titre;
						}
					?>
					<div class="cellule intitule">Titre</div>
                    <div class="cellule"><?php if(isset($titres[$adherentSource['titre']])) echo $titres[$adherentSource['titre']]['nom']; ?></div>
					<div class="cellule"><select name="titre">
						<?php
						$titreFusion = getValeurFusionner('titre', $adherentSource, $adherentDestination);
						foreach($titres as $titre)
						{
							echo '<option value="'.$titre['id'].'" style="background-color: '.$titre['couleur'].';"';
							if($titre['id'] == $titreFusion) echo ' selected';
							echo '>'.$titre['nom'].'</option>';
						}?></select>
                    </div>
					<div class="cellule"><?php if(isset($titres[$adherentDestination['titre']])) echo $titres[$adherentDestination['titre']]['nom']; ?></div>
                    <div class="cellule"></div>
				</div>
                <?php
                    echo genererFormulaireFusion('date_inscription', $adherentSource, $adherentDestination, 'Date d\'inscription', false, '(Format : AAAA-MM-JJ)', 'date');
                    echo genererFormulaireFusion('date_fin_cotisation', $adherentSource, $adherentDestination, 'Date de fin de cotisation', false, '(Format : AAAA-MM-JJ)', 'date');
                ?>
				<div class="ligne">
                    <div class="cellule intitule">Commentaire</div>
                    <div class="cellule"><?php echo $adherentSource['commentaire']; ?></div>
                    <div class="cellule"><textarea name="commentaire"><?php echo getValeurFusionner('commentaire', $adherentSource, $adherentDestination); ?></textarea></div>
                    <div class="cellule"><?php echo $adherentDestination['commentaire']; ?></div>
                    <div class="cellule"></div>
				</div>
			<p>
				<input type="hidden" name="envoi" value="2" />
				<input type="hidden" name="adherentSourceId" value="<?php echo $adherentSourceId; ?>" />
				<input type="hidden" name="adherentDestinationId" value="<?php echo $adherentDestinationId; ?>" />
				<input id="bouton_valider" type="submit" name="modifier" value="Fusionner" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
<?php
    exit();
}

//On demande quel membre il faut fusionner
if(!isset($_GET['id']) || intval($_GET['id']) <= 0)
{
    ajouterErreurNotification('Le membre à fusionner n\'a pas été indiqué.');
    header('location: adherents.php');
    exit();
}

$mode = MODE_MODIF;
$id = intval($_GET['id']);
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
$user = $requete->fetch();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Fusion d'un adhérent</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<script type="text/javascript" src="js/form_adherent.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2>Fusion d'un adhérent</h2>

		<form action="fusion_adherent.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule">Adhérent à fusionner :</div>
					<div class="cellule"><?php echo $user['prenom'].' '.$user['nom']; ?></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="adherent">Adhérent de destination :</label></div>
					<div class="cellule">
						<select name="adherent" required>
							<?php $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id != '.$id.' ORDER BY prenom, nom ASC');
							if(!($requete === false))
							{
								$users = $requete->fetchAll();
								foreach($users as $u)
								{
									echo '<option value="'.$u['id'].'">'.$u['prenom'].' '.$u['nom'].'</option>';
								}
							} ?>
						</select>
					</div>
				</div>
            </div>

			<p>
				<input type="hidden" name="envoi" value="1" />
				<input type="hidden" name="id" value="<?php echo $id; ?>" />
				<input id="bouton_valider" type="submit" name="fusion" value="Démarrer la fusion" />
			</p>
        </form>
    </body>
</html>