<?php
session_start();
require_once 'prelude_page.php';
verifierSiFonctionnaliteEstActive('oauth');
verifierSiUtilisateurAPermission(GERER_OAUTH);

if (!isset($_GET['client_id']) || strlen($_GET['client_id']) == 0) {
    header('location: liste_oauth.php');
    exit();
}

$clientId = htmlspecialchars($_GET['client_id']);

$bdd->exec('DELETE FROM ' . $bdd->getNomTable('oauth_clients') . ' WHERE client_id = "' . $clientId . '"');
ajouterSuccesNotification("Client supprimé avec succès");

header('location: liste_oauth.php');
exit();
