<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('factures');
verifierSiUtilisateurAPermission(GERER_TYPE_FACTURE);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Gérer les types de factures</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Types de facture</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Externe</th><th>Actions</th></tr>
	<?php $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'factures_type');
		if(!($requete === false))
		{
			$donnees = $requete->fetchAll();
			foreach($donnees as $type)
			{
				echo '<tr>';
				echo '<td>'.$type['id'].'</td>';
				echo '<td>'.$type['nom'].'</td>';
				echo '<td>'.($type['externe'] ? 'Oui' : 'Non').'</td>';
				echo '<td><a href="form_facture_type.php?id='.$type['id'].'">Modifier</a></td>';
				echo '</tr>';
			}
		}
	?>
	</table>

	<p><a href="form_facture_type.php">Ajouter un type</a></p>
	<?php include('bas_page.php'); ?>
	</body>
</html>
