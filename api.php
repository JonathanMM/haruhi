<?php
session_start();
require_once('prelude_page.php');

if(!isset($_GET['action']))
    die();

switch($_GET['action'])
{
    case 'repondreInvitation':
        if(!isset($_GET['evenement']) || !isset($_GET['reponse']))
            die();
        
        $idEvenement = intval($_GET['evenement']);
        $reponse = intval($_GET['reponse']);
        if($idEvenement <= 0 || !in_array($reponse, array(CALENDRIER_REPONSE_NON_REPONDU, CALENDRIER_REPONSE_REFUS, CALENDRIER_REPONSE_PROVISOIRE, CALENDRIER_REPONSE_ACCEPTE)))
            die();

        $pdo->exec('UPDATE '.$bdd_prefixe.'calendrier_participants SET reponse = '.$reponse.'
        WHERE idEvenement = '.$idEvenement.' AND idMembre = '.$_SESSION['id']);

        echo json_encode(true);
    case 'creerMouvement':
        if(!isset($_POST['date']) || !isset($_POST['libele']) || (!isset($_POST['credit']) && !isset($_POST['debit'])) || !isset($_POST['etiquettes']))
            die();
        
        if(!testerPermission(GERER_TRESORERIE))
            die();
        
        $libele = htmlspecialchars($_POST['libele']);
        $date = htmlspecialchars($_POST['date']);
        $credit = floatval($_POST['credit']);
        $debit = floatval($_POST['debit']);
        $etiquettes = explode(',', $_POST['etiquettes']);
    
        $pdo->beginTransaction();
        $idMouvement = creer_mouvement($libele, $date, $credit, $debit, $etiquettes);
        $pdo->commit();

        echo json_encode($idMouvement);
    case 'modifierMouvement':
        if(!isset($_POST['type']) || !isset($_POST['idMouvement']) || !isset($_POST['valeur']))
            die();
        
        if(!testerPermission(GERER_TRESORERIE))
            die();

        $idMouvement = intval($_POST['idMouvement']);

        if($idMouvement <= 0)
            die();
        
        if(!in_array($_POST['type'], array('date', 'label', 'debit', 'credit', 'etiquettes')))
            die();

        $type = $_POST['type'];

        if($type == 'etiquettes')
        {
            //On récupère les étiquettes actuelles
            $pdo->beginTransaction();

            $etiquettes = explode(',', $_POST['valeur']);

            modifier_mouvement_etiquettes($idMouvement, $etiquettes);

            $pdo->commit();
        } else {   
            $valeur = null;
            if($type == 'credit' || $type == 'debit')
                $valeur = floatval($_POST['valeur']);
            else
                $valeur = htmlspecialchars($_POST['valeur']);
            
            $pdo->exec('UPDATE '.$bdd_prefixe.'tresorerie_mouvement SET '.$type.'="'.$valeur.'" WHERE id = '.$idMouvement);
        }
            
        echo json_encode(true);
        break;
    case 'supprimerMouvement':
        if(!isset($_POST['idMouvement']))
            die();
        
        if(!testerPermission(GERER_TRESORERIE))
            die();

        $idMouvement = intval($_POST['idMouvement']);

        if($idMouvement <= 0)
            die();

        $pdo->beginTransaction();

        $pdo->exec('DELETE FROM '.$bdd_prefixe.'tresorerie_mouvement WHERE id = '.$idMouvement);
        $pdo->exec('DELETE FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette WHERE idMouvement = '.$idMouvement);
        
        $pdo->commit();
        echo json_encode(true);
}
?>