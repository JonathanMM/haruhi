<?php
function aBesoinDeGuillemets($type)
{
    return in_array($type, array(PDO::PARAM_STR, PDO::PARAM_LOB));
}

function sauvegarder($prefixeChemin = '')
{
    global $pdo, $bdd_prefixe;
    //On prépare le fichier
    $auj = new DateTime();
    $nomFichier = $auj->format('Y-m-d-G-i-s').'.sql';
    $fichierSQL = fopen($prefixeChemin.'sauvegardes/'.$nomFichier, 'w');
    
    // On récupère la liste des tables
    $requete = $pdo->query('show tables LIKE "'.$bdd_prefixe.'%"');
    $tables = $requete->fetchAll();
    foreach($tables as $table)
    {
        $nomTable = $table[0];
        //Création de la table
        $requeteCreation = $pdo->query('SHOW CREATE TABLE '.$nomTable);
        fwrite($fichierSQL, $requeteCreation->fetch()[1].'; '."\n");
        
        //Récupération des données
        $requeteDonnees = $pdo->query('SELECT * FROM '.$nomTable);
        
        //On regarde les types
        $guillemets = array();
        for($i = 0; $i < $requeteDonnees->columnCount(); $i++)
        {
            $meta = $requeteDonnees->getColumnMeta($i);
            $guillemets[] = aBesoinDeGuillemets($meta['pdo_type']);
        }
        $donneesDonnees = $requeteDonnees->fetchAll(PDO::FETCH_NUM);
        $debutRequeteDonnees = 'INSERT INTO '.$nomTable.' VALUES ';
        $iData = 0;
        $maxIData = 100;
        $valSql = array();
        foreach($donneesDonnees as $donnees)
        {
            if($iData >= $maxIData)
            {
                fwrite($fichierSQL, $debutRequeteDonnees.implode(', ', $valSql).';'."\n");
                $iData = 0;
                $valSql = array();
            }
            $insert = array();
            foreach($donnees as $i => $col)
            {
                if($guillemets[$i])
                $insert[] = '"'.htmlspecialchars($col).'"';
                else
                $insert[] = $col;
            }
            $valSql[] = '('.implode(',', $insert).')';
            $iData++;
        }
        //Et on oublie pas le dernier !
        if(count($valSql) > 0)
        {
            fwrite($fichierSQL, $debutRequeteDonnees.implode(', ', $valSql).';'."\n");
        }
    }
    
    fclose($fichierSQL);
}

function restaurer($adresseFichier)
{
    global $pdo, $bdd_prefixe;
    try
    {
        //On commence par préparer une transaction
        $pdo->beginTransaction();

        //On efface toutes les tables !
        $requete = $pdo->query('show tables LIKE "'.$bdd_prefixe.'%"');
        $tables = $requete->fetchAll();
        foreach($tables as $table)
        {
            $pdo->exec('DROP TABLE '.$table);
        }

        //Et on passe le fichier
        $pdo->exec(file_get_contents($adresseFichier));

        //Tout s'est bien passé, on commit
        $pdo->commit();
    } catch(Exception $e)
    {
        //Erreur, on rollback !
        $pdo->rollBack();
    }
}
?>