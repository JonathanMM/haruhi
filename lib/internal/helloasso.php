<?php
/*
HelloAsso
 */
function importHelloAsso($dateDerniereExecution = null)
{
    global $fonctionnalites_statut, $slugAsso_helloasso, $champPseudo_helloasso, $pdo, $bdd_prefixe;

    $logger = Logger::getLogger('cron.helloasso');
    if (!isset($fonctionnalites_statut['helloasso']) || !$fonctionnalites_statut['helloasso']) //Import désactivé
    {
        $logger->info('Fonctionnalité désactivée.');
        return;
    }

    $logger->info('Début du traitement de helloasso.');
    $accessToken = getAccessTokenHelloAsso();

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));

    $page = 1;
    $maxPage = null;

    //On prépare la date du from
    $dateFrom = null;
    if (!is_null($dateDerniereExecution)) {
        $dateFrom = clone $dateDerniereExecution;
        $dateFrom->sub(new DateInterval("P7D"));
    }

    do {
        curl_setopt(
            $curl,
            CURLOPT_URL,
            'https://api.helloasso.com/v5/organizations/' . $slugAsso_helloasso .
                '/orders?pageIndex=' . $page .
                '&pageSize=20&sortOrder=Desc&sortField=Date&withDetails=true' .
                (is_null($dateFrom) ? '' : '&from=' . $dateFrom->format('Y-m-d') . 'T00:00:00')
        );
        $json = json_decode(curl_exec($curl));
        if (!isset($json->data) || count($json->data) == 0) //Pas de nouvelles actions
        {
            $logger->info('Aucune adhésion a traitée.');
            return;
        }

        // On gère la pagination
        $maxPage = intval($json->pagination->totalPages);

        //On prépare nos requêtes
        $requeteFactureExterne = $pdo->prepare('SELECT id FROM ' . $bdd_prefixe . 'factures WHERE lien = ?');
        $requeteAdherent = $pdo->prepare('SELECT * FROM ' . $bdd_prefixe . 'adherents WHERE nom = ? AND prenom = ?');

        //On récupère le titre par défaut
        $requeteTitreParDefaut = $pdo->query('SELECT id FROM ' . $bdd_prefixe . 'titre WHERE defaut = 1');
        $titreParDefaut = $requeteTitreParDefaut->fetch();
        if ($titreParDefaut === false) {
            $idTitreParDefaut = 1;
        } else {
            $idTitreParDefaut = $titreParDefaut['id'];
        }

        //On stocke le type de facture
        $idTypeCotisation = 0;
        $idTypeDonation = 0;
        $requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'factures_type');
        if (!($requete === false)) {
            $types = $requete->fetchAll();
            foreach ($types as $type) {
                if ($type['externe'] == 1) {
                    if ($type['cotisation'] != 0 && $idTypeCotisation == 0) {
                        $idTypeCotisation = $type['id'];
                    } elseif ($idTypeDonation == 0) {
                        $idTypeDonation = $type['id'];
                    }
                }
            }
        }

        if ($idTypeCotisation == 0) {
            $logger->error('Aucun type de facture externe de type cotisation n\'a été trouvée.');
            throw new Exception('Aucun type de facture externe de type cotisation n\'a été trouvée.');
        }

        if ($idTypeDonation == 0) {
            $logger->warn('Aucun type de facture externe de type don n\'a été trouvée.');
        }

        //On regarde ce qu'on a
        foreach ($json->data as $order) {
            $facture = array(
                'id' => $order->id,
                'date' => new DateTime($order->date),
                'beneficiaire' => array(
                    // 'nom' => $order->user->lastName,
                    // 'prenom' => $order->user->firstName,
                    // 'adresse_rue' => "", // $order->payer_address,
                    // 'adresse_cp' => "", // $order->payer_zip_code,
                    // 'adresse_ville' => "", // $order->payer_city,
                    // 'adresse_pays' => "", // $order->payer_country,
                    'courriel' => $order->payer->email,
                ),
                'url_recu' => "HELLOASSO://" . $order->id,
                'actions' => array(),
            );

            foreach ($order->items as $item) {
                switch ($item->type) {
                    case "Donation":
                        break;
                    case "Membership":
                        // On peut remplir le bénéficiaire

                        $facture['beneficiaire']['nom'] = $item->user->lastName;
                        $facture['beneficiaire']['prenom'] = $item->user->firstName;

                        // On regarde si on a le pseudo ou l'adresse
                        if (isset($item->customFields)) {
                            foreach ($item->customFields as $infoEnPlus) {
                                if ((string) $infoEnPlus->name == $champPseudo_helloasso) {
                                    $facture['beneficiaire']['pseudo'] = (string) $infoEnPlus->answer;
                                } else {
                                    switch ((string) $infoEnPlus->name) {
                                        case 'Adresse':
                                            $facture['beneficiaire']['adresse_rue'] = (string) $infoEnPlus->answer;
                                            break;
                                        case 'Code Postal':
                                            $facture['beneficiaire']['adresse_cp'] = (string) $infoEnPlus->answer;
                                            break;
                                        case 'Ville':
                                            $facture['beneficiaire']['adresse_ville'] = (string) $infoEnPlus->answer;
                                            break;
                                        default:
                                            $facture['beneficiaire'][(string) $infoEnPlus->name] = (string) $infoEnPlus->answer;
                                            break;
                                    }
                                }
                            }
                        }
                        break;
                    default: # Ça ne nous intéresse pas, on ignore
                        continue 2;
                }

                $facture['actions'][] = array(
                    "id" => $item->id,
                    "somme" => intval($item->amount) / 100, // On a la somme en centimes d'euros
                    "type" => $item->type,
                    "statut" => $item->state
                );
            }

            if ($logger->isDebugEnabled()) {
                $logger->debug(print_r($facture, true));
            }

            if (count($facture['actions']) === 0) // Aucune facture à traiter au final
            {
                if ($logger->isInfoEnabled()) {
                    $logger->info('Le type de la facture n\'est pas pris en charge : ' . $facture['url_recu']);
                }

                continue; //On passe à l'élément suivant
            }

            //On passe au traitement
            //Étape 1 : On regarde si la facture est déjà dans le système
            $requeteFactureExterne->execute(array($facture['url_recu']));
            $factureBDD = $requeteFactureExterne->fetch();
            if (!($factureBDD === false)) //On a déjà quelque chose
            {
                if ($logger->isInfoEnabled()) {
                    $logger->info('La facture à l\'url suivante a déjà été traitée : ' . $facture['url_recu']);
                }

                continue; //On passe à l'élément suivant
            }

            //Étape 2 : On cherche le bénéfiaire dans le système
            $adherent = $facture['beneficiaire'];

            if (!isset($adherent['nom']) || !isset($adherent['prenom'])) {
                $logger->warn('Adhérent sans prénom ni nom');
                continue;
            }

            $pdo->beginTransaction();

            $requeteAdherent->execute(array($adherent['nom'], $adherent['prenom']));
            $adherentBDD = $requeteAdherent->fetch();
            if (!($adherentBDD === false)) ///L'adhérent existe déjà :)
            {
                $facture['adherent'] = $adherentBDD['id'];
                $adherent = array_merge($adherentBDD, $adherent); //L'ordre est important !
            } else { //On l'ajoute au système
                $adherent['adresse'] = ""; // on n'a pas d'adresse
                $adherent['titre'] = $idTitreParDefaut;
                $adherent['date_inscription'] = '0000-00-00'; //Pour l'instant, il n'aura pas de date d'inscription
                $adherent['courriel'] = $adherent['courriel'];
                $adherent['date_fin_cotisation'] = null;
                $adherent['cotisation_infinie'] = false;
                $facture['adherent'] = creerAdherent($adherent);
                if ($facture['adherent'] === false) {
                    //L'adhérent n'a pas été crée, on saute la ligne et on le note !
                    $logger->warn('Un adhérent n\'a pu être crée !');
                    $pdo->rollBack();
                    continue;
                }
            }

            //Étape 3 : On parcourt les actions
            $hasActionWithoutError = false;
            foreach ($facture['actions'] as $action) {
                try {
                    if ($action['type'] == 'Membership') //Cotisation
                    {
                        //On ajoute la cotisation
                        $retour = ajouterCotisation($facture['adherent'], $idTypeCotisation, $facture['date']->format("Y-m-d"));

                        if (!$retour) {
                            //Problème lors de l'ajout de cotisation
                            $logger->warn("Problème lors de l'ajout d'une cotisation");
                        }

                        //Et on créer la facture
                        $retour = creerFacture($idTypeCotisation, $facture['adherent'], $action['somme'], $facture['date']->format("Y-m-d"), "Hello Asso", $facture['url_recu'], $adherent);
                    } elseif ($action['type'] == 'Donation') //Don
                    {
                        //On créer la facture
                        $retour = creerFacture($idTypeDonation, $facture['adherent'], $action['somme'], $facture['date']->format("Y-m-d"), "Hello Asso", $facture['url_recu'], $adherent);
                    }

                    if ($retour) {
                        $logger->info('Une facture a été importée avec succès.');
                        $hasActionWithoutError = true;
                    } else {
                        //Problème lors de l'ajout de la facture
                        $logger->warn("Problème lors de l'ajout d'une facture");
                    }
                } catch (Exception $ex) {
                    if ($logger->isErrorEnabled()) {
                        $logger->error('Erreur lors de l\'insertion d\'une facture : ' . $ex->getMessage() . ' (' . $ex->getFile() . ':' . $ex->getLine() . ')' . "\n" .
                            'Type : ' . $idTypeCotisation .
                            ', adherent : ' . $facture['adherent'] .
                            ', somme : ' . $action['somme'] .
                            ', date : ' . $facture['date']->format("Y-m-d") .
                            ', url : ' . $facture['url_recu'] . "\n" .
                            $ex->getTraceAsString());
                    }

                    if ($logger->isDebugEnabled()) {
                        $logger->debug('Id type cotisation : ' . $idTypeCotisation);
                        $logger->debug('Id type donation : ' . $idTypeDonation);
                        $logger->debug('Type d\'action : ' . $action['type']);
                    }
                }
            }

            if ($hasActionWithoutError) {
                $pdo->commit();
            } else {
                $pdo->rollBack();
            }
        }

        //On gère la pagination
        if (is_null($maxPage)) {
            $maxPage = intval((string) $json->pagination->max_page);
        }
        $page++;
    } while ($page <= $maxPage);

    curl_close($curl);
    $logger->info('Fin du traitement de helloasso.');
}

function getAccessTokenHelloAsso()
{
    global $clientId_helloasso, $clientSecret_helloasso;
    $logger = Logger::getLogger('cron.helloasso');
    $curl = curl_init();
    // On doit récupérer un access token
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded"));

    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
        "client_id" => $clientId_helloasso,
        "client_secret" => $clientSecret_helloasso,
        "grant_type" =>    "client_credentials"
    )));

    curl_setopt($curl, CURLOPT_URL, "https://api.helloasso.com/oauth2/token");
    $json = json_decode(curl_exec($curl));
    if (!isset($json->access_token)) {
        $logger->error("Impossible de se connecter à helloasso");
        return;
    }

    return $json->access_token;
}
