<?php
/*
Trésorerie
*/

define('TRESORERIE_LIEN_TYPE_AUCUN', 0);
define('TRESORERIE_LIEN_TYPE_FACTURE', 1);

function creer_mouvement($libele, $date, $credit, $debit, $etiquettes)
{
    global $pdo, $bdd_prefixe;
    if(strlen($libele) == 0 || strlen($date) == 0 || ($credit <= 0 && $debit <= 0))
        return null;

    $pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_mouvement (date, label, debit, credit) VALUES ("'.$date.'.", "'.$libele.'", '.($debit == 0 ? 'NULL' : $debit).', '.($credit == 0 ? 'NULL' : $credit).')');
    $idMouvement = $pdo->lastInsertId();

    foreach($etiquettes as $etiquette)
    {
        if(intval($etiquette) > 0)
        {
            $pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette (idMouvement, idEtiquette) VALUES ('.$idMouvement.', '.$etiquette.')');
        }
    }
    return $idMouvement;
}

function modifier_mouvement_etiquettes($idMouvement, $etiquettes)
{
    global $pdo, $bdd_prefixe;

    $requete = $pdo->query('SELECT idEtiquette FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette WHERE idMouvement = '.$idMouvement);
    $etiquettesSql = $requete->fetchAll();

    $dejaDansBDD = array();
    foreach($etiquettesSql as $etiquette)
    {
        $dejaDansBDD[] = $etiquette['idEtiquette'];
    }

    $aAjouter = array_diff($etiquettes, $dejaDansBDD);
    $aSupprimer = array_diff($dejaDansBDD, $etiquettes);

    foreach($aAjouter as $idEtiquette)
    {
        if(intval($idEtiquette) > 0)
            $pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette (idMouvement, idEtiquette) VALUES ('.$idMouvement.', '.$idEtiquette.')');
    }

    foreach($aSupprimer as $idEtiquette)
    {
        if(intval($idEtiquette) > 0)
            $pdo->exec('DELETE FROM '.$bdd_prefixe.'tresorerie_liens_mouvement_etiquette WHERE idMouvement = '.$idMouvement.' AND idEtiquette = '.$idEtiquette);
    }
}

function recuperer_label_lien($typePiece, $idPiece, $mettreLien = true)
{
    switch($typePiece)
    {
        case TRESORERIE_LIEN_TYPE_AUCUN:
            return 'Note';
                break;
        case TRESORERIE_LIEN_TYPE_FACTURE:
            $retour = '';
            if($mettreLien && testerFonctionnalite('factures') && testerPermission(VOIR_FACTURE))
                $retour .= '<a href="voir_facture.php?id='.$idPiece.'">';
            $retour .= 'Facture #'.$idPiece;
            if($mettreLien && testerFonctionnalite('factures') && testerPermission(VOIR_FACTURE))
                $retour .= '</a>';
            return $retour;
            break;
    }
}
?>