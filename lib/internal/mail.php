<?php
/*
Mail
*/
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Configuration\ConfigurationManager;

define('MAIL_STATUT_ERREUR', 0);
define('MAIL_STATUT_ENVOYE', 1);
define('MAIL_STATUT_RETENU', 2);

function envoyer_mail($destinataire, $titre, $contenu, $contexte, $date = null, $idMembre = 0, $sauvegarderContenu = true)
{
	global $bdd, $pdo, $bdd_prefixe, $envoyerMails, $infos_nomasso, $infos_adressemail;

	$logger = Logger::getLogger('mail');

	if(is_null($destinataire) || strlen($destinataire) == 0) //Pas de mail, on part
		return false;

	$configurationManager = new Configuration\ConfigurationManager($bdd);
	$contenuHTMLModele = $configurationManager->getConfiguration(ConfigurationManager::MODELE_HTML_MAIL)->getValeur();
	if(is_null($contenuHTMLModele))
		$contenuHTML = nl2br($contenu);
	else
		$contenuHTML = str_replace('{contenu}', nl2br($contenu), $contenuHTMLModele);

	if(is_null($titre) || strlen($titre) == 0 || is_null($contenuHTML) || strlen($contenu) == 0)
	{
		$etat_envoi = MAIL_STATUT_ERREUR;
	} else {
		if(!isset($envoyerMails) || $envoyerMails)
		{
			try
			{
				//On prépare le mail
				$mail = new PHPMailer(true);
				$mail->setLanguage('fr');
				$mail->CharSet = 'UTF-8';
				$mail->setFrom($infos_adressemail, $infos_nomasso);
				$mail->addAddress($destinataire);
				$mail->Subject = $titre;
				$mail->msgHTML($contenuHTML);
				$retourMail = $mail->send();
				$etat_envoi = $retourMail ? MAIL_STATUT_ENVOYE : MAIL_STATUT_ERREUR;
			} catch(Exception $e)
			{
				$etat_envoi = MAIL_STATUT_ERREUR;
				$logger->error('Exception durant l\'envoi d\'un courriel : '.$e->errorMessage());
			}
		}
		else
			$etat_envoi = MAIL_STATUT_RETENU; //On simule l'envoi

		if($date == null)
			$date = new DateTime();
	}

	$date_formate = $date->format('Y-m-d H:i:s');
	$pdo->exec('INSERT INTO '.$bdd_prefixe.'logs_mail (titre, contenu, destinataire, idMembre, contexte, date, envoye)
				VALUES ("'.htmlspecialchars($titre).'", "'.($sauvegarderContenu ? htmlspecialchars($contenuHTML) : '').'", "'.$destinataire.'", "'.$idMembre.'", "'.$contexte.'", "'.$date_formate.'", '.$etat_envoi.')');
	return $etat_envoi != MAIL_STATUT_ERREUR;
}
?>
