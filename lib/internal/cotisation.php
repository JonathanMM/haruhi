<?php
/*
Cotisation
*/

function ajouterCotisation($adherentId, $typeId, $date)
{
	global $pdo, $bdd_prefixe;

	//On récupère les infos de l'adhérent
	$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'adherents WHERE id = ' . $adherentId);
	$adherent = $requete->fetch();
	$anciennesInfosAdherent = $adherent; //On conserve l'ancien état pour les évents ensuite

	//On récupère les infos de la facture
	$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'factures_type WHERE id = ' . $typeId);
	$type = $requete->fetch();

	$infosTypeFacture = analyserTypeFacture($type, $adherent['date_fin_cotisation']);

	//Si le membre n'était pas inscrit avant, on l'inscrit à la date indiqué
	$inscription = '';
	if ($adherent['date_inscription'] == '0000-00-00') {
		$inscription = ', date_inscription = "' . $date . '"';
		$adherent['date_inscription'] = $date;
		//Et s'il n'avait pas de numéro de membre, il en a un !
		if ($adherent['numero'] == 0) {
			$numeroAdherent = getNouveauNumeroAdherent();
			$inscription .= ', numero = ' . $numeroAdherent;
			$adherent['numero'] = $numeroAdherent;
		}
	}
	$pdo->exec('UPDATE ' . $bdd_prefixe . 'adherents SET date_fin_cotisation = "' . $infosTypeFacture['fin'] . '"' . $inscription . ' WHERE id = ' . $adherentId);

	$adherent['date_fin_cotisation'] = $infosTypeFacture['fin'];

	$adherent['date_cotisation'] = $date;

	//Et on appelle toutes les implémentations
	$classes = importerEvent('adherent');
	foreach ($classes as $classe) {
		$classe::modifier($adherentId, $adherent, $anciennesInfosAdherent);
	}
	return true;
}

function determinerFinCotisationTypePeriode($debut_periode, $fin_periode, $termine_periode, $date)
{
	//On doit connaitre la valeur de n
	$debut_coupe = explode('-', $debut_periode);
	$debut_jour = $debut_coupe[2];
	$debut_mois = $debut_coupe[1];
	$debut_N = $debut_coupe[0];
	$fin_coupe = explode('-', $fin_periode);
	$fin_jour = $fin_coupe[2];
	$fin_mois = $fin_coupe[1];
	$fin_N = $fin_coupe[0];
	$auj_jour = $date->format('d');
	$auj_mois = $date->format('m');

	$n = 0;
	// Plusieurs possibilités
	// Soit on est après le début de la période, et dans ce cas, on va noter le n
	if ($auj_mois > $debut_mois || ($auj_mois == $debut_mois && $auj_jour >= $debut_jour)) {
		$n = $debut_N;
		// Soit on est avant la fin de la période
	} elseif ($auj_mois < $fin_mois || ($auj_mois === $fin_mois && $auj_jour <= $fin_jour)) {
		$n = $fin_N;
	}

	$termine_coupe = explode('-', $termine_periode);
	$termine_an = $date->format('Y') + ($termine_coupe[0] - $n);
	$date_fin_cotisation = $termine_an . '-' . $termine_coupe[1] . '-' . $termine_coupe[2];
	return $date_fin_cotisation;
}
