<?php
define('ADRESSE_SEPARATEUR_ADRESSE', '⁂');
define('ADRESSE_SEPARATEUR_CASE', '✱');

/**
 * Transforme une chaîne de caractères en tableau d'adresse
 *
 * @param string $texte
 * @return array(array(string))
 */
function parseAdresse($texte)
{
    if(strlen($texte) == 0) return array();

    $retourAdresses = array();
    $coupeAdresses = explode(ADRESSE_SEPARATEUR_ADRESSE, $texte);
    foreach($coupeAdresses as $adresseTxt)
    {
        $coupeAdresse = explode(ADRESSE_SEPARATEUR_CASE, $adresseTxt);
        if(count($coupeAdresse) != 5) //Si ce n'est pas le format attendu, on passe au suivant
            continue;
        
        $retourAdresses[] = array(
            'ligne1' => $coupeAdresse[0],
            'ligne2' => $coupeAdresse[1],
            'cp' => $coupeAdresse[2],
            'ville' => $coupeAdresse[3],
            'pays' => $coupeAdresse[4]
        );
    }
    return $retourAdresses;
}

/**
 * Transforme une chaîne de caractères en adresse affichable
 * 
 * @param string $texte
 * @return string
 */
function afficherAdresseSurUneLigne($texte)
{
    $adresse = parseAdresse($texte);

    if(count($adresse) == 0) return '';
    
    return implode(' ', $adresse[0]);
}

/**
 * Transforme un tableau d'adresse en chaîne de caractères
 *
 * @param array(array(string)) $adresses Tableau d'adresse au format array(array('ligne1' => string, 'ligne2' => string, 'cp' => string, 'ville' => string, 'pays' => string), …)
 * @return string
 */
function stringifyAdresse($adresses)
{
    $retourTableauAdresses = array();
    foreach($adresses as $adresse)
    {
        $retourTableauAdresse = array();
        $retourTableauAdresse[] = isset($adresse['ligne1']) ? $adresse['ligne1'] : '';
        $retourTableauAdresse[] = isset($adresse['ligne2']) ? $adresse['ligne2'] : '';
        $retourTableauAdresse[] = isset($adresse['cp']) ? $adresse['cp'] : '';
        $retourTableauAdresse[] = isset($adresse['ville']) ? $adresse['ville'] : '';
        $retourTableauAdresse[] = isset($adresse['pays']) ? $adresse['pays'] : '';
        $retourTableauAdresses[] = implode(ADRESSE_SEPARATEUR_CASE, $retourTableauAdresse);
    }
    return implode(ADRESSE_SEPARATEUR_ADRESSE, $retourTableauAdresses);
}
?>