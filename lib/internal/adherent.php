<?php
/*
Adhérent
*/

function creerAdherent($adherent)
{
    global $pdo, $bdd_prefixe;

    if(isset($adherent['prenom']) && strlen($adherent['prenom']) > 0 &&
       isset($adherent['nom']) && strlen($adherent['nom']) > 0 &&
       isset($adherent['date_inscription']) && strlen($adherent['date_inscription']) > 0)
    {
        if((!isset($adherent['numero']) || $adherent['numero'] == 0) && $adherent['date_inscription'] != '0000-00-00') //Pas de numéro mais inscrit !
            $adherent['numero'] = getNouveauNumeroAdherent();

        $pdo->exec('INSERT INTO '.$bdd_prefixe.'adherents 
            (numero, prenom, nom, pseudo, adresse, telephone, courriel, titre, id_membre, date_inscription, date_fin_cotisation, cotisation_infinie, commentaire) 
            VALUES ('.(isset($adherent['numero']) ? $adherent['numero'] : '0').', 
                "'.$adherent['prenom'].'", 
                "'.$adherent['nom'].'", 
                "'.(isset($adherent['pseudo']) ? $adherent['pseudo'] : '').'", 
                "'.(isset($adherent['adresse']) ? $adherent['adresse'] : '').'", 
                "'.(isset($adherent['telephone']) ? $adherent['telephone'] : '').'", 
                "'.trim($adherent['courriel']).'", 
                "'.$adherent['titre'].'", 
                "0", 
                "'.$adherent['date_inscription'].'", 
                '.((isset($adherent['date_fin_cotisation']) && !is_null($adherent['date_fin_cotisation'])) ? '"'.$adherent['date_fin_cotisation'].'"' : 'NULL').', 
                '.((isset($adherent['cotisation_infinie']) && $adherent['cotisation_infinie']) ? '1' : '0').', 
                "'.(isset($adherent['commentaire']) ? htmlspecialchars($adherent['commentaire']) : '').'")');
        $adherentId = $pdo->lastInsertId();
        //Et on met son id dans le tableau
        $adherent['id'] = $adherentId;

        //Maintenant, on peut appeler les différentes implémentations
        $classes = importerEvent('adherent');
        foreach($classes as $classe)
        {
            $classe::creer($adherent);
        }

        if(isset($adherent['newsletter']))
        {
            foreach($adherent['newsletter'] as $nl)
                $pdo->exec('INSERT INTO '.$bdd_prefixe.'ml_lien (id_ml, id_entite, is_titre) VALUES ('.intval($nl).', '.$adherentId.', 0)');
        }

        if(isset($adherent['enfants']))
        {
            foreach($adherent['enfants'] as $enfant)
                $pdo->exec('INSERT INTO '.$bdd_prefixe.'enfants (id, prenom, nom, classe, naissance, id_parent, id_intervenant) VALUES ("", "'.$enfant['prenom'].'", "'.$enfant['nom'].'", "'.$enfant['classe'].'", "'.$enfant['date_naissance'].'", "'.$adherentId.'", "0")');
        }

        return $adherentId;
    }
    return false;
}

function parserMailAdherent($contenu, $infosAdherent)
{
    global $infos_nomasso, $infos_siteasso;
    if(strlen($contenu) > 0)
    {
        $arrayModele = array('{asso.nom}', '{asso.haruhi}', '{adherent.prenom}', '{adherent.nom}', '{adherent.fin_cotisation}');
        $arrayDonnees = array($infos_nomasso, $infos_siteasso, $infosAdherent['prenom'], $infosAdherent['nom'], 
                                isset($infosAdherent['date_fin_cotisation']) ? $infosAdherent['date_fin_cotisation'] : '0000-00-00');

        if(isset($infosAdherent['date_cotisation']))
        {
            $arrayModele[] = '{cotisation.date}';
            $arrayDonnees[] = $infosAdherent['date_cotisation'];
        }
        
        return str_replace(
            $arrayModele,
            $arrayDonnees,
            $contenu
        );
    }

    return null;
}

function recupererAdherents($filtre = "1=1")
{
    global $pdo, $bdd_prefixe;

    $adherents = [];
    $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents a WHERE '.$filtre);
    if(!($requete === false))
    {
        $items = $requete->fetchAll();
        foreach($items as $donnees)
        {
            //Là, on a les adhérents qui vont recevoir un petit mail
            $adherents[] = array(
                'id' => $donnees['id'],
                'numero' => $donnees['numero'],
                'prenom' => $donnees['prenom'],
                'nom' => $donnees['nom'],
                'pseudo' => $donnees['pseudo'],
                'adresse' => $donnees['adresse'],
                'telephone' => $donnees['telephone'],
                'courriel' => trim($donnees['courriel']),
                'titre' => $donnees['titre'],
                'date_inscription' => $donnees['date_inscription'],
                'date_fin_cotisation' => $donnees['date_fin_cotisation'],
                'commentaire' => $donnees['commentaire']
            );
        }
    }
    return $adherents;
}

function getNouveauNumeroAdherent()
{
    global $pdo, $bdd_prefixe;
    $requete = $pdo->query('SELECT MAX(numero) max_numero FROM '.$bdd_prefixe.'adherents');
    $donneesAdherents = $requete->fetch();
    return $donneesAdherents['max_numero'] + 1;
}

function supprimerAdherent($idAdherent)
{
    global $pdo, $bdd_prefixe;
    //S'il a un compte, il saute aussi
    $requete = $pdo->query('SELECT id_membre, titre, courriel FROM '.$bdd_prefixe.'adherents WHERE id = '.$idAdherent);
    $donnees = $requete->fetch();
    if(count($donnees) >= 1 && $donnees['id_membre'] > 0)
        $pdo->exec('DELETE FROM '.$bdd_prefixe.'membres WHERE id = '.$donnees['id_membre']);

    //On déplace les éléments liés à l'adhérent
    deplacerElementsEntreAdherents($idAdherent, 0);

    //On déclanche les événements
    $classes = importerEvent('adherent');
    foreach($classes as $classe)
    {
        $classe::supprimer($idAdherent, $adherent);
    }

    $pdo->exec('DELETE FROM '.$bdd_prefixe.'adherents WHERE id = '.$idAdherent);
}

function deplacerElementsEntreAdherents($idSource, $idDestination)
{
    global $pdo, $bdd_prefixe;

    //On délie les factures
    $pdo->exec('UPDATE '.$bdd_prefixe.'factures SET adherent = '.$idDestination.' WHERE adherent = '.$idSource);
    //Idem dans les logs de mail
    $pdo->exec('UPDATE '.$bdd_prefixe.'logs_mail SET idMembre = '.$idDestination.' WHERE idMembre = '.$idSource);
}

function getValeurFusionner($propriete, $adherentSource, $adherentDestination)
{
    if(!isset($adherentSource[$propriete]) || is_null($adherentSource[$propriete]) || strlen($adherentSource[$propriete]) == 0)
        return isset($adherentDestination[$propriete]) ? $adherentDestination[$propriete] : '';
    
    if(!isset($adherentDestination[$propriete]) || is_null($adherentDestination[$propriete]) || strlen($adherentDestination[$propriete]) == 0)
        return $adherentSource[$propriete];
    
    if($adherentSource[$propriete] == $adherentDestination[$propriete])
        return $adherentSource[$propriete];
    return "";
}

function genererFormulaireFusion($propriete, $adherentSource, $adherentDestination, $nom = "", $obligatoire = false, $aide = "", $typeInput = null)
{
    return '
    <div class="ligne">
        <div class="cellule intitule"><label name="'.$propriete.'">'.$nom.'</label></div>
        <div class="cellule">'.$adherentSource[$propriete].'</div>
        <div class="cellule"><input'.(is_null($typeInput) ? '' : ' type="'.$typeInput.'"').($obligatoire ? ' required' : '').' name="'.$propriete.'" value="'.getValeurFusionner($propriete, $adherentSource, $adherentDestination).'" /></div>
        <div class="cellule">'.$adherentDestination[$propriete].'</div>
        <div class="cellule">'.$aide.'</div>
    </div>';
}

function genererRequeteSQLListeAdherent($terme = NULL, $titre = NULL, $onlyExpired = false, $tri = NULL, $ordre = null)
{
    $plus_req = array();
    if(!is_null($terme) && strlen($terme) > 0)
    {
        $champs = ['prenom', 'nom', 'pseudo'];
        $termes = explode(' ', htmlspecialchars($terme));

        $recherche = array();
        foreach($champs as $champ)
        {
            foreach($termes as $t)
            {
                $recherche[] = 'a.'.$champ.' LIKE "%'.$t.'%"';
            }
        }

        $plus_req[] = implode(' OR ', $recherche);
    }

    if(!is_null($titre) && count($titre) > 0)
        $plus_req[] = 'a.titre = '.implode(' OR a.titre = ', $titre);

    if($onlyExpired)
        $plus_req[] = '(a.date_fin_cotisation < CURDATE() AND a.cotisation_infinie = 0 AND a.date_inscription != "0000-00-00")';

    $triSQL = '';
    if(!is_null($tri) && strlen($tri) > 0)
    {
        $option_tri = array('id' => 'a.id', 'numero' => 'a.numero', 'nom' => 'a.nom', 'prenom' => 'a.prenom', 'pseudo' => 'a.pseudo', 'statut' => 't.nom', 'di' => 'a.date_inscription', 'df' => 'a.date_fin_cotisation', 'com' => 'a.commentaire');
        $tri = htmlspecialchars($tri);
        if(isset($option_tri[$tri]))
            $tri = $option_tri[$tri];
        else
            $tri = 'a.nom, a.prenom';

        if(!is_null($ordre) && strtolower($ordre) == 'desc')
            $triSQL = ' ORDER BY '.$tri.' DESC';
        else
            $triSQL = ' ORDER BY '.$tri.' ASC';
    } else
        $triSQL = ' ORDER BY IF(a.numero = 0, 0, -1), a.numero ASC'; //Les sans numéros vont à la fin

    $where = '';
    if(count($plus_req) > 0)
        $where = 'WHERE '.implode(' AND ', $plus_req);

    return trim($where.$triSQL);
}

function formater_adresse($adresse)
{
    $adresseArray = parseAdresse($adresse);
    if(count($adresseArray) == 0) return $adresse;
    $adresseArraySansVide = array_filter($adresseArray[0], function($elt) { return strlen($elt) > 0; });
    return implode(' ', $adresseArraySansVide);
}
?>
