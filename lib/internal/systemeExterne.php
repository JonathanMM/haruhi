<?php
/*
Mailing list
*/
function synchroListSE(SystemeExterneDO $SystemeExterneDO, SystemeExterneDO $SystemeExterneDODestination)
{
    $liste = $SystemeExterneDO->getAllSE();
    $listeNomML = array();
    foreach($liste as $infosML)
    {
        $nomML = $infosML['adresse'];
        $infosML['classe'] = get_class($SystemeExterneDO);
        if($SystemeExterneDODestination->estModifiable())
            $SystemeExterneDODestination->createSE($nomML, $infosML);
        $listeNomML[] = $nomML;
    }
    return $listeNomML;
}

function isAbonneInListe($infosAbonne, $liste)
{
    foreach($liste as $infosA)
    {
        if((isset($infosAbonne['pseudo']) && isset($infosA['pseudo']) && $infosA['pseudo'] == $infosAbonne['pseudo']) ||
            (isset($infosAbonne['courriel']) && isset($infosA['courriel']) && $infosA['courriel'] == $infosAbonne['courriel']))
        {
            return true;
        }
    }
    return false;
}

function synchroniserCourriels(SystemeExterneDO $SystemeExterneDOSource, SystemeExterneDO $SystemeExterneDODestination, $nomML)
{
    $listeAbonnes = $SystemeExterneDOSource->getSubscriberOfSE($nomML);

    //On récupère la liste des abonnés actuels
    $listeExistante = $SystemeExterneDODestination->getSubscriberOfSE($nomML);

    $nouvelleListe = array();
    foreach($listeExistante as $nomListe => $liste)
        $nouvelleListe[$nomListe] = array();

    foreach($listeAbonnes as $abonnes)
    {
        foreach($abonnes as $infosAbonne)
        {
            $estDansListe = false;
            foreach($listeExistante as $nomListe => $liste)
            {
                if(!$estDansListe && isAbonneInListe($infosAbonne, $liste))
                {
                    $nouvelleListe[$nomListe][] = $infosAbonne;
                    $estDansListe = true;
                }
            }

            //On n'a pas trouvé l'abonné dans les listes précédentes, alors on doit le créer
            if(!$estDansListe)
                $SystemeExterneDODestination->addSubscriberOfSE($nomML, $infosAbonne);
        }
    }
        
    //Maintenant, il faut enlever le surplus
    foreach($listeExistante as $nomListe => $liste)
    {
        $supplementaires = array_diff($liste, $nouvelleListe[$nomListe]);
        foreach($supplementaires as $infosAbonne)
        {
            $SystemeExterneDODestination->deleteSubscriberOfSE($nomML, $infosAbonne);
        }
    }
}
?>