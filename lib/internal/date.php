<?php
/*
Date
*/

function formater_date($date)
{
	if($date == "0000-00-00")
		return '';
	else
	{
		$coupe = explode('-', $date);
		if(count($coupe) == 3)
			return $coupe[2].'/'.$coupe[1].'/'.$coupe[0];
		else
			return $date;
	}
}

function formater_date_heure($date)
{
	$coupe = explode(' ', $date);
	return formater_date($coupe[0]).' '.$coupe[1];
}

function reste_jour($date) //Format AAAA-MM-JJ
{
	if(is_null($date))
		return '';
	
	$dateObj = new DateTime($date);
	return ($dateObj->format('Y') - date('Y')) * 365 + $dateObj->format('z') - date('z');
}

?>
