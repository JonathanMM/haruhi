<?php
/*
Facture
*/
function analyserTypeFacture($type, $dateInit, $adherent = null)
{
	if ($dateInit == '0000-00-00') //On met la date du jour
		$dateInit = new DateTime();
	else
		$dateInit = new DateTime($dateInit);

	if ($type['cotisation'] == 1) //Type durée supplémentaire
	{
		switch ($type['unite']) {
			case 1: //jour
				$interval = 'P' . $type['duree'] . 'D';
				break;
			case 2: //mois
				$interval = 'P' . $type['duree'] . 'M';
				break;
			case 3: //année
				$interval = 'P' . $type['duree'] . 'Y';
				break;
			default: //Dans le doute, c'est un 1
				$interval = 'P1Y';
				break;
		}
		$obj_fc = $dateInit;
		if (!is_null($adherent) && isset($adherent['date_fin_cotisation'])) {
			$act_fc = new DateTime($adherent['date_fin_cotisation']);
			if ($act_fc > $obj_fc) //La date actuelle de fin de cotisation dépasse la date de référence
				$obj_fc = $act_fc;
		}
		$obj_fc->add(new DateInterval($interval));
		$date_fin_cotisation = $obj_fc->format("Y-m-d");
	} else if ($type['cotisation'] == 2) //Type période
	{
		$date_fin_cotisation = determinerFinCotisationTypePeriode($type['debut_periode'], $type['fin_periode'], $type['termine_periode'], $dateInit);
	} else {
		$date_fin_cotisation = date('Y-m-d');
	}

	return array(
		'id' => $type['id'],
		'nom' => $type['nom'],
		'externe' => $type['externe'] == 1,
		'cotisation' => ($type['cotisation'] != 0),
		'fin' => $date_fin_cotisation
	);
}

function creerFacture($typeId, $adherentId, $somme, $date, $payement, $lien = NULL, $adherent = NULL)
{
	global $pdo, $bdd_prefixe, $infos_nomasso, $infos_adresseasso;

	$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'factures_type WHERE id = ' . $typeId);
	$type = $requete->fetch();
	if ($type['externe'] == 0) //facture interne, pas de lien
		$lienSql = 'NULL';
	else
		$lienSql = '"' . $lien . '"';

	//On va vérifier que les conditions sont correcte
	if (strlen($somme) == 0)
		throw new Exception("Aucune somme indiquée", 1);

	if (strlen($date) == 0)
		throw new Exception("Aucune date indiquée", 2);

	if (strlen($payement) == 0)
		throw new Exception("Aucun moyen de payement indiqué", 3);

	if ($somme < 0)
		throw new Exception("Le montant de la facture ne peut être négatif", 4);

	if ($type['externe'] == 1 && (is_null($lien) || strlen($lien) == 0))
		throw new Exception("Le lien vers la facture est obligatoire pour les types externes", 5);


	$pdo->exec('INSERT INTO ' . $bdd_prefixe . 'factures (adherent, somme, date, payement, type, visibilite, lien)
			VALUES ("' . $adherentId . '", "' . $somme . '", "' . $date . '", "' . $payement . '", ' . $typeId . ', 1, ' . $lienSql . ')');
	$id = $pdo->lastInsertId();
	if ($type['externe'] == 0) //facture interne, on va copier les valeurs dans factures détails
	{
		if (is_null($adherent)) {
			//On a besoin des infos de l'adherent
			$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'adherents WHERE id = ' . $adherentId);
			$adherent = $requete->fetch();
		}
		$pdo->exec('INSERT INTO `' . $bdd_prefixe . 'factures_details` (`id`, `asso`, `nom_adherent`, `adresse_adherent`, `signature`)
			VALUES (' . $id . ', "' . $infos_nomasso . '<br />' . $infos_adresseasso . '", "' . $adherent['prenom'] . ' ' . $adherent['nom'] . '", "' . $adherent['adresse'] . '", "' . signature($date) . '")');
	}
	return true;
}
