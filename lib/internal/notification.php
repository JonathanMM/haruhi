<?php
/*
Notification
*/

function afficherNotification()
{
    if(isset($_SESSION['msgErreur']) && $_SESSION['msgErreur'])
    {
        echo '<div class="msg msgErreur">'.$_SESSION['msgErreur'].'</div>';
        unset($_SESSION['msgErreur']);
    }

    if(isset($_SESSION['msgSucces']) && $_SESSION['msgSucces'])
    {
        echo '<div class="msg msgSucces">'.$_SESSION['msgSucces'].'</div>';
        unset($_SESSION['msgSucces']);
    }
}

function ajouterErreurNotification($erreur)
{
    $_SESSION['msgErreur'] = $erreur;
}

function ajouterSuccesNotification($succes)
{
    $_SESSION['msgSucces'] = $succes;
}
?>
