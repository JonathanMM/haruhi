<?php
/*
Connexion automatique
 */
use Ramsey\Uuid\Uuid;

define('CONNEXIONAUTO_NOM_COOKIE', 'haruhi_autologin');

function generateToken()
{
    return Uuid::uuid4()->toString();
}

function setCookieAutoLogin(string $token, DateTime $dateExpire)
{
    setcookie(CONNEXIONAUTO_NOM_COOKIE, $token, $dateExpire->format('U'));
}

function removeCookieAutoLogin()
{
    setcookie(CONNEXIONAUTO_NOM_COOKIE, "", time() - 3600);
}

function genererCookieAutoLogin(int $userId)
{
    global $bdd;
    $token = generateToken();
    $dateExpire = new DateTime('now', new DateTimeZone('Europe/Paris'));
    $dateExpire->add(new DateInterval('P30D'));

    $bdd->exec('INSERT INTO ' . $bdd->getNomTable('membresToken') . ' (token, userId, expire) VALUES ("' . $token . '", ' . $userId . ', "' . $dateExpire->format('Y-m-d H:i:s') . '")');

    setCookieAutoLogin($token, $dateExpire);
}

function needLogged()
{
    if (!(isset($_SESSION['co'])) || $_SESSION['co'] === false) {
        header('location: connexion.php');
        exit();
    }
}

function autoLogged()
{
    global $bdd;

    if (!isset($_COOKIE[CONNEXIONAUTO_NOM_COOKIE])) {
        return false;
    }

    $token = htmlspecialchars($_COOKIE[CONNEXIONAUTO_NOM_COOKIE]);

    $requete = $bdd->query('SELECT *, m.id AS id, a.id AS id_adherent, t.id AS id_titre FROM ' . $bdd->getNomTable('membresToken') . ' j
    INNER JOIN ' . $bdd->getNomTable('membres') . ' m ON m.id = j.userid
    LEFT JOIN ' . $bdd->getNomTable('adherents') . ' a ON a.id_membre = m.id
    LEFT JOIN ' . $bdd->getNomTable('titre') . ' t ON t.id = a.titre
    WHERE j.token = "' . $token . '" AND j.expire >= NOW()');
    $user = $requete->fetch();

    if (!($user === false)) {
        // Connexion réussie !
        initialiserUtilisateur($user);

        // Token utilisé, direction poubelle
        $bdd->exec('DELETE FROM ' . $bdd->getNomTable('membresToken') . ' WHERE token = "' . $token . '" OR expire < NOW()');

        // Et on prépare la prochaine connexion
        genererCookieAutoLogin($user['id']);

        return true;
    }
}

function initialiserUtilisateur($utilisateur)
{
    $_SESSION['pseudo'] = $utilisateur['pseudo'];
    $_SESSION['id'] = $utilisateur['id'];
    $_SESSION['id_adherent'] = $utilisateur['id_adherent'];
    $_SESSION['id_titre'] = $utilisateur['id_titre'];
    $_SESSION['co'] = true;
    $_SESSION['type_courriel'] = $utilisateur['type'];
    $_SESSION['permission'] = $utilisateur['permission'];
    $_SESSION['visibilite'] = $utilisateur['visibilite'];
}
