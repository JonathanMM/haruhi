<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
    $mode = MODE_MODIF;

    $pdo->beginTransaction();
    foreach($_POST as $key => $value)
    {
        if(substr($key, 0, 14) == 'attacher-lien-' && $value == 1)
        {
            $idLien = intval(substr($key, 14));
            $pdo->exec('UPDATE '.$bdd_prefixe.'tresorerie_justificatifs SET idMouvement = '.$id.' WHERE id = '.$idLien);
        }
    }
    $pdo->commit();

    ajouterSuccesNotification('Tout a été rattaché avec succès.');

    header('location: voir_mouvement.php?id='.$id);
    exit();
}

verifierSiIdInGet('tresorerie.php');

$id = intval($_GET['id']);

//Liens
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_justificatifs j '.
//LEFT JOIN '.$bdd_prefixe.'factures f ON j.idPiece = f.id AND j.typePiece = '.TRESORERIE_LIEN_TYPE_FACTURE.'
'WHERE j.idMouvement = 0');
$liens = $requete->fetchAll();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Attacher des liens à un mouvement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>
    <h2>Attacher des liens au mouvement #<?php echo $id; ?></h2>

    <form action="attacher_lien_mouvement.php" method="post">
        <table>
            <tr>
                <th>Attacher</th>
                <th>Lien</th>
                <th>Description</th>
                <th>Montant</th>
            </tr>
            <?php
            foreach($liens as $lien)
            {
                echo '<tr>';
                    echo '<td><input type="checkbox" name="attacher-lien-'.$lien['id'].'" value="1" /></td>';
                    echo '<td>'.recuperer_label_lien($lien['typePiece'], $lien['idPiece'], false).'</td>';
                    echo '<td>'.$lien['description'].'</td>';
                    echo '<td>'.formaterMontant($lien['montant']).'</td>';
                echo '</tr>';
            }
            ?>
        </table>
			<p>
				<input type="hidden" name="id" value="<?php echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
    </form>

	<?php include('bas_page.php'); ?>
	</body>
</html>