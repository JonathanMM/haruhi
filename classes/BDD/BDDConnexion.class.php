<?php
namespace BDD;

class BDDConnexion
{
	private $_pdo;
	private $_prefixe;
	
	public function __construct(BDDConfiguration $config)
	{
		$this->_pdo = new \PDO($config->getDns(), $config->getUsername(), $config->getPassword());
		$this->_pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION); //Renvoi des exceptions lors d'erreurs
		$this->_prefixe = $config->getPrefixe();
	}

	public function getPdo() : \PDO
	{
		return $this->_pdo;
	}

	public function getPrefixe() : string
	{
		return $this->_prefixe;
	}

	public function query($sql) : \PDOStatement
	{
		return $this->_pdo->query($sql);
	}

	public function exec($sql) : int
	{
		return $this->_pdo->exec($sql);
	}

	public function getNomTable(string $nom) : string
	{
		return $this->_prefixe . $nom;
	}
}
?>