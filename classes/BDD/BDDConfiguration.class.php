<?php
namespace BDD;

class BDDConfiguration
{
	private $_dns;
	private $_username;
	private $_password;
	private $_prefixe;

	public function __construct()
	{
		$a = func_get_args();
		$i = func_num_args();
		if (method_exists($this,$f='__construct'.$i)) {
			call_user_func_array(array($this,$f),$a);
		}
	} 

	private function __construct5(string $host, string $username, string $password, string $bdd_name, string $prefixe)
	{
		$this->__construct4('mysql:host='.$host.';dbname='.$bdd_name.';charset=utf8', $username, $password, $prefixe);
	}

	private function __construct4(string $dns, string $username, string $password, string $prefixe)
	{
		$this->_dns = $dns;
		$this->_username = $username;
		$this->_password = $password;
		$this->_prefixe = $prefixe;
	}

	public function getDns() : string
	{
		return $this->_dns;
	}

	public function getUsername() : string
	{
		return $this->_username;
	}

	public function getPassword() : string
	{
		return $this->_password;
	}

	public function getPrefixe() : string
	{
		return $this->_prefixe;
	}
}