<?php
namespace Configuration;

class ConfigurationManager
{
    public const MODELE_HTML_MAIL = 'modeleMailHtml';
    public const MODULE_PAGE_ACCUEIL = 'modulePageAccueil';

    private $_pdo;

    public function __construct(\BDD\BDDConnexion $pdo)
    {
        $this->_pdo = $pdo;
    }

    public function getConfiguration(string $cle): ConfigurationValue
    {
        $requete = $this->_pdo->query('SELECT valeur FROM ' . $this->_pdo->getNomTable('configuration') . ' WHERE cle = "' . htmlspecialchars($cle) . '"');
        $donnee = $requete->fetch();
        if ($donnee === false) {
            return new ConfigurationValue($cle, null);
        }

        return new ConfigurationValue($cle, htmlspecialchars_decode($donnee['valeur']));
    }

    public function setConfiguration(ConfigurationValue $config): void
    {
        $requete = $this->_pdo->query('SELECT id FROM ' . $this->_pdo->getNomTable('configuration') . ' WHERE cle = "' . htmlspecialchars($config->getCle()) . '"');
        $donnee = $requete->fetch();
        if ($donnee === false) {
            $this->_pdo->exec('INSERT INTO ' . $this->_pdo->getNomTable('configuration') . ' (cle, valeur)
                                VALUES ("' . htmlspecialchars($config->getCle()) . '", "' . htmlspecialchars($config->getValeur()) . '")');
        } else {
            $this->_pdo->exec('UPDATE ' . $this->_pdo->getNomTable('configuration') . ' SET  valeur = "' . htmlspecialchars($config->getValeur()) . '" WHERE id = ' . $donnee['id']);
        }

    }
}
