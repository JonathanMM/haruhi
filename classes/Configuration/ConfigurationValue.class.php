<?php
namespace Configuration;

class ConfigurationValue
{
    private $_cle;
    private $_valeur;

    public function __construct(string $cle, ?string $valeur)
    {
        $this->_cle = $cle;
        $this->_valeur = $valeur;
    }

    public function getCle(): string
    {
        return $this->_cle;
    }

    public function getValeur(): ?string
    {
        return $this->_valeur;
    }
}
