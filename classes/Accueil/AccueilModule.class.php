<?php

namespace Accueil;

class AccueilModule
{
    private $_type;
    private $_valeur;

    public function __construct(array $jsonConfig)
    {
        $this->_loadConfig($jsonConfig);
    }

    private function _loadConfig(array $jsonConfig)
    {
        if (!isset($jsonConfig['type'])) {
            throw new InvalidModuleException(isset($jsonConfig['type']) ? $jsonConfig['type'] : 'inconnu');
        }

        $this->_type = $jsonConfig['type'];
        $this->_valeur = $jsonConfig['valeur'];
    }

    public function getType(): string
    {
        return $this->_type;
    }

    public function getValeur(): ?string
    {
        return \html_entity_decode($this->_valeur);
    }

    public function getJsonConfiguration(): array
    {
        return array(
            'type' => $this->_type,
            'valeur' => \htmlentities($this->_valeur),
        );
    }
}
