<?php
namespace Accueil;

class AccueilSkeleton
{
    public const MESSAGE = 'message';

    private $_config;

    public function __construct(?string $jsonConfiguration = null)
    {
        $this->_config = [];
        if (!is_null($jsonConfiguration)) {
            $this->_loadConfiguration($jsonConfiguration);
        }

    }

    private function _loadConfiguration(string $jsonConfiguration): void
    {
        $json = json_decode($jsonConfiguration, true);

        foreach ($json as $moduleConfig) {
            $module = new AccueilModule($moduleConfig);
            $this->_config[$module->getType()] = $module;
        }
    }

    public function getModule(string $type)
    {
        if (isset($this->_config[$type])) {
            return $this->_config[$type];
        }

        return null;
    }

    public function setModule(AccueilModule $module): void
    {
        if (is_null($module)) {
            throw new NullPointerException;
        }

        $this->_config[$module->getType()] = $module;
    }

    public function getJsonConfiguration(): string
    {
        $config = [];
        foreach ($this->_config as $module) {
            $config[] = $module->getJsonConfiguration();
        }
        return json_encode($config);
    }
}
