<?php
namespace Accueil;

use Configuration\ConfigurationManager;
use Configuration\ConfigurationValue;

class AccueilProvider
{
    private $_bdd;
    private $_configurationManager;
    private $_accueil;

    public function __construct(\BDD\BDDConnexion $bdd)
    {
        $this->_bdd = $bdd;
        $this->_configurationManager = new ConfigurationManager($bdd);
        $this->loadConfiguration();
    }

    private function loadConfiguration(): void
    {
        $json = $this->_configurationManager->getConfiguration(ConfigurationManager::MODULE_PAGE_ACCUEIL)->getValeur();
        if (is_null($json) || strlen($json) == 0) {
            $this->_accueil = new AccueilSkeleton();
            return;
        }

        $this->_accueil = new AccueilSkeleton($json);
    }

    public function getAccueil(): AccueilSkeleton
    {
        return $this->_accueil;
    }

    public function save(): void
    {
        $configValue = new ConfigurationValue(ConfigurationManager::MODULE_PAGE_ACCUEIL, $this->_accueil->getJsonConfiguration());
        $this->_configurationManager->setConfiguration($configValue);
    }
}
