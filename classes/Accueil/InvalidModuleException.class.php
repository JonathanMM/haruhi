<?php
namespace Accueil;

class InvalidModuleException extends \Exception
{
    public function __construct($nomModule)
    {
        $message = "Impossible d'instancier le module " . $nomModule;
        parent::__construct($message, $code, $previous);
    }
}
