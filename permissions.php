<?php
session_start();
require_once 'prelude_page.php';
verifierSiUtilisateurAPermission(MODIFIER_PERMISSION);

if (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    $requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'titre');
    $i_visibilite = 0;
    if (!($requete === false)) {
        $donnees = $requete->fetchAll();
        foreach ($donnees as $permissionBDD) {
            $permission = 0;
            $visibilite = 0;
            if ($fonctionnalites_statut['intervenants']) {
                if (isset($_POST['page_intervenant'])) {
                    if (in_array($permissionBDD['id'], $_POST['page_intervenant'])) {
                        $permission |= PAGE_INTERVENANT;
                    }
                }
                if (isset($_POST['lire_dossier'])) {
                    if (in_array($permissionBDD['id'], $_POST['lire_dossier'])) {
                        $permission |= LIRE_DOSSIER;
                    }
                }
            }
            if (isset($_POST['liste_adherent'])) {
                if (in_array($permissionBDD['id'], $_POST['liste_adherent'])) {
                    $permission |= LISTE_ADHERENT;
                }
            }
            if (isset($_POST['touche_adherent'])) {
                if (in_array($permissionBDD['id'], $_POST['touche_adherent'])) {
                    $permission |= TOUCHE_ADHERENT;
                }
            }
            if ($fonctionnalites_statut['factures']) {
                if (isset($_POST['voir_facture'])) {
                    if (in_array($permissionBDD['id'], $_POST['voir_facture'])) {
                        $permission |= VOIR_FACTURE;
                    }
                }
                if (isset($_POST['faire_facture'])) {
                    if (in_array($permissionBDD['id'], $_POST['faire_facture'])) {
                        $permission |= FAIRE_FACTURE;
                    }
                }
                if (isset($_POST['gerer_type_facture'])) {
                    if (in_array($permissionBDD['id'], $_POST['gerer_type_facture'])) {
                        $permission |= GERER_TYPE_FACTURE;
                    }
                }
            }
            if ($fonctionnalites_statut['stockage']) {
                if (isset($_POST['envoyer_fichier'])) {
                    if (in_array($permissionBDD['id'], $_POST['envoyer_fichier'])) {
                        $permission |= ENVOYER_FICHIER;
                    }
                }
                if (isset($_POST['visibilite_fichier'])) {
                    $visibilite = intval($_POST['visibilite_fichier'][$i_visibilite]);
                }
            }
            if ($fonctionnalites_statut['mikuru']) {
                if (isset($_POST['avoir_courriel'])) {
                    if (in_array($permissionBDD['id'], $_POST['avoir_courriel'])) {
                        $permission |= AVOIR_COURRIEL;
                    }
                }
            }
            if (isset($_POST['modifier_permission'])) {
                if (in_array($permissionBDD['id'], $_POST['modifier_permission'])) {
                    $permission |= MODIFIER_PERMISSION;
                }
            }
            if ($fonctionnalites_statut['calendrier']) {
                if (isset($_POST['gerer_categorie_calendrier'])) {
                    if (in_array($permissionBDD['id'], $_POST['gerer_categorie_calendrier'])) {
                        $permission |= GERER_CATEGORIE_CALENDRIER;
                    }
                }
            }
            if ($fonctionnalites_statut['se']) {
                if (isset($_POST['gerer_ml'])) {
                    if (in_array($permissionBDD['id'], $_POST['gerer_ml'])) {
                        $permission |= GERER_ML;
                    }
                }
            }
            if ($fonctionnalites_statut['yuki']) {
                if (isset($_POST['consulter_ml'])) {
                    if (in_array($permissionBDD['id'], $_POST['consulter_ml'])) {
                        $permission |= CONSULTER_ML;
                    }
                }
            }
            if (isset($_POST['voir_logs_mail'])) {
                if (in_array($permissionBDD['id'], $_POST['voir_logs_mail'])) {
                    $permission |= VOIR_LOGS_MAIL;
                }
            }
            if (isset($_POST['sauvegarde'])) {
                if (in_array($permissionBDD['id'], $_POST['sauvegarde'])) {
                    $permission |= SAUVEGARDE;
                }
            }
            if (isset($_POST['touche_modele_mail'])) {
                if (in_array($permissionBDD['id'], $_POST['touche_modele_mail'])) {
                    $permission |= TOUCHE_MODELE_MAIL;
                }
            }
            if ($fonctionnalites_statut['tresorerie']) {
                if (isset($_POST['gerer_tresorerie'])) {
                    if (in_array($permissionBDD['id'], $_POST['gerer_tresorerie'])) {
                        $permission |= GERER_TRESORERIE;
                    }
                }
            }
            if ($fonctionnalites_statut['oauth']) {
                if (isset($_POST['connexion_oauth'])) {
                    if (in_array($permissionBDD['id'], $_POST['connexion_oauth'])) {
                        $permission |= CONNEXION_OAUTH;
                    }
                }

                if (isset($_POST['gerer_oauth'])) {
                    if (in_array($permissionBDD['id'], $_POST['gerer_oauth'])) {
                        $permission |= GERER_OAUTH;
                    }
                }
            }

            if (isset($_POST['gerer_accueil'])) {
                if (in_array($permissionBDD['id'], $_POST['gerer_accueil'])) {
                    $permission |= GERER_ACCUEIL;
                }
            }

            $pdo->exec('UPDATE ' . $bdd_prefixe . 'titre SET permission = "' . $permission . '", visibilite = ' . $visibilite . ' WHERE id = ' . $permissionBDD['id']);
            $i_visibilite++;
        }
        ajouterSuccesNotification("Les permissions ont bien été mises à jour.");
    } else {
        ajouterErreurNotification("Erreur lors de la récupération de la base de données.");
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Permissions</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include 'haut_page.php';?>
		<?php afficherNotification();?>
		<form action="permissions.php" method="post">
			<table id="permission">
				<tr><th>Titres</th>
					<?php if ($fonctionnalites_statut['intervenants']) {?>
					<th>Accéder à la page des intervenants</th><th>Consulter les dossiers</th>
					<?php }?>
					<th>Accéder à la liste des adhérents</th><th>Ajouter/Modifier les adhérents</th>
					<?php if ($fonctionnalites_statut['factures']) {?>
					<th>Consulter les factures</th><th>Faire des factures</th>
					<th>Gérer les types des factures</th>
					<?php }?>
					<?php if ($fonctionnalites_statut['stockage']) {?>
					<th>Envoyer des fichiers</th>
					<?php }?>
					<?php if ($fonctionnalites_statut['mikuru']) {?>
					<th>Avoir un courriel</th>
					<?php }?>
					<th>Modifier les permissions</th>
					<?php if ($fonctionnalites_statut['calendrier']) {?>
					<th>Gérer les catégories du calendrier</th>
					<?php }?>
					<?php if ($fonctionnalites_statut['se']) {?>
					<th>Gérer les mailing lists</th>
					<?php }?>
					<?php if ($fonctionnalites_statut['yuki']) {?>
					<th>Consulter les ML</th>
					<?php }?>
					<th>Consulter les logs des mails</th>
					<th>Réaliser des sauvegardes</th>
					<th>Modifier les modèles de courriel</th>
					<?php if ($fonctionnalites_statut['tresorerie']) {?>
					<th>Consulter et modifier la trésorerie</th>
					<?php }?>
					<?php if ($fonctionnalites_statut['oauth']) {?>
					<th>Connexion via OAuth2</th>
					<th>Gérer OAuth2</th>
					<?php }?>
					<th>Gérer la page d'accueil</th>
					<?php if ($fonctionnalites_statut['stockage']) {?>
					<th>Visibilité des fichiers</th>
					<?php }?>
				</tr>
				<?php
$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'titre');
if (!($requete === false)) {
    $donnees = $requete->fetchAll();
    foreach ($donnees as $permission) {
        echo '<tr><td>' . $permission['nom'] . '</td>';
        if ($fonctionnalites_statut['intervenants']) {
            echo '<td><input type="checkbox" name="page_intervenant[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & PAGE_INTERVENANT) {
                echo ' checked ';
            }
            echo ' /></td>';

            echo '<td><input type="checkbox" name="lire_dossier[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & LIRE_DOSSIER) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        echo '<td><input type="checkbox" name="liste_adherent[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & LISTE_ADHERENT) {
            echo ' checked ';
        }
        echo ' /></td>';

        echo '<td><input type="checkbox" name="touche_adherent[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & TOUCHE_ADHERENT) {
            echo ' checked ';
        }
        echo ' /></td>';

        if ($fonctionnalites_statut['factures']) {
            echo '<td><input type="checkbox" name="voir_facture[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & VOIR_FACTURE) {
                echo ' checked ';
            }
            echo ' /></td>';

            echo '<td><input type="checkbox" name="faire_facture[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & FAIRE_FACTURE) {
                echo ' checked ';
            }
            echo ' /></td>';

            echo '<td><input type="checkbox" name="gerer_type_facture[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & GERER_TYPE_FACTURE) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        if ($fonctionnalites_statut['stockage']) {
            echo '<td><input type="checkbox" name="envoyer_fichier[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & ENVOYER_FICHIER) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        if ($fonctionnalites_statut['mikuru']) {
            echo '<td><input type="checkbox" name="avoir_courriel[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & AVOIR_COURRIEL) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        echo '<td><input type="checkbox" name="modifier_permission[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & MODIFIER_PERMISSION) {
            echo ' checked ';
        }
        echo ' /></td>';

        if ($fonctionnalites_statut['calendrier']) {
            echo '<td><input type="checkbox" name="gerer_categorie_calendrier[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & GERER_CATEGORIE_CALENDRIER) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        if ($fonctionnalites_statut['se']) {
            echo '<td><input type="checkbox" name="gerer_ml[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & GERER_ML) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        if ($fonctionnalites_statut['yuki']) {
            echo '<td><input type="checkbox" name="consulter_ml[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & CONSULTER_ML) {
                echo ' checked ';
            }
            echo ' /></td>';
        }
        echo '<td><input type="checkbox" name="voir_logs_mail[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & VOIR_LOGS_MAIL) {
            echo ' checked ';
        }
        echo ' /></td>';

        echo '<td><input type="checkbox" name="sauvegarde[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & SAUVEGARDE) {
            echo ' checked ';
        }
        echo ' /></td>';

        echo '<td><input type="checkbox" name="touche_modele_mail[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & TOUCHE_MODELE_MAIL) {
            echo ' checked ';
        }
        echo ' /></td>';

        if ($fonctionnalites_statut['tresorerie']) {
            echo '<td><input type="checkbox" name="gerer_tresorerie[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & GERER_TRESORERIE) {
                echo ' checked ';
            }
            echo ' /></td>';
        }
        if ($fonctionnalites_statut['oauth']) {
            echo '<td><input type="checkbox" name="connexion_oauth[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & CONNEXION_OAUTH) {
                echo ' checked ';
            }
            echo ' /></td>';

            echo '<td><input type="checkbox" name="gerer_oauth[]" value="' . $permission['id'] . '"';
            if ((int) $permission['permission'] & GERER_OAUTH) {
                echo ' checked ';
            }
            echo ' /></td>';
        }

        echo '<td><input type="checkbox" name="gerer_accueil[]" value="' . $permission['id'] . '"';
        if ((int) $permission['permission'] & GERER_ACCUEIL) {
            echo ' checked ';
        }
        echo ' /></td>';

        if ($fonctionnalites_statut['stockage']) {
            echo '<td><input name="visibilite_fichier[]" style="width: 30px;" value="' . $permission['visibilite'] . '" /></td>';
        }
        echo '</tr>';
    }
}
?>
			</table>
			<p>
				<input type="hidden" name="envoi" value="1" />
				<input type="submit" value="Valider" />
		</form>

		<?php include 'bas_page.php';?>
	</body>
</html>
