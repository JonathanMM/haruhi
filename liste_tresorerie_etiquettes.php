<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<link rel="icon" type="image/png" href="images/favicon.png" />
		<title>Haruhi → Étiquettes</title>

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Étiquettes</h2>

	<table>
	<tr><th>Id</th><th>Nom</th><th>Action</th></tr>
	<?php
		$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_etiquettes');
		$etiquettes = $requete->fetchAll();
	if(!($etiquettes === false))
	{
		foreach($etiquettes as $etiquette)
		{
			echo '<tr>';
			echo '<td style="background-color:'.$etiquette['couleur'].'">'.$etiquette['id'].'</td>';
			echo '<td>'.$etiquette['label'].'</td>';
			echo '<td><a href="form_tresorerie_etiquette.php?id='.$etiquette['id'].'">Modifier</a></td>';
			echo '</tr>';
		}
	} ?>
	</table>

	<p><a href="form_tresorerie_etiquette.php">Ajouter une étiquette</a>
	
	<?php include('bas_page.php'); ?>
	</body>
</html>
