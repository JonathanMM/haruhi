<?php
session_start();
require_once 'prelude_page.php';

session_destroy();
removeCookieAutoLogin();
header('location: connexion.php');
