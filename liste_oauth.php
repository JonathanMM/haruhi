<?php
session_start();
require_once 'prelude_page.php';
verifierSiFonctionnaliteEstActive('oauth');
verifierSiUtilisateurAPermission(GERER_OAUTH);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Gestion des clients OAuth2</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include 'haut_page.php';
afficherNotification();
?>
    <h2>Gérer les clients OAuth2</h2>

	<table>
		<tr><th>Nom</th><th>Client Id</th><th>Client secret</th><th>Action</th></tr>
		<?php
$requete = $bdd->query('SELECT client_id, nom FROM ' . $bdd->getNomTable('oauth_clients'));
$clients = $requete->fetchAll();
if (!($clients === false)) {
    foreach ($clients as $client) {
        echo '<tr>';
        echo '<td>' . $client['nom'] . '</td>';
        echo '<td>' . $client['client_id'] . '</td>';
        echo '<td>******</td>';
        echo '<td><a href="form_oauth.php?client_id=' . $client['client_id'] . '">Modifier</a> − <a href="supp_oauth.php?client_id=' . $client['client_id'] . '">Supprimer</a></td>';
        echo '</tr>';
    }
}
?>
	</table>

	<nav>
		<a href="form_oauth.php">Ajouter un client</a>
	</nav>

	<p>
	Adresse des points d'entrées :<br />
	Authorize: /oauth2/1.0/authorize.php<br />
	Token: /oauth2/1.0/token.php<br />
	Profile: /oauth2/1.0/api.php
	</p>
	<?php include 'bas_page.php';?>
	</body>
</html>