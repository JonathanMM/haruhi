<?php
session_start();
require_once 'prelude_page.php';
verifierSiFonctionnaliteEstActive('oauth');
verifierSiUtilisateurAPermission(GERER_OAUTH);

if (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    if (isset($_POST['modif']) && $_POST['modif'] == 1) {
        $mode = MODE_MODIF;
    } else {
        $mode = MODE_AJOUT;
    }

    $nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);
    $clientId = htmlspecialchars($_POST['client_id'], ENT_QUOTES);
    $redirectURI = htmlspecialchars($_POST['redirect_uri'], ENT_QUOTES);
    $clientSecret = null;
    $renew = false;
    if ($mode == MODE_MODIF && isset($_POST['renew']) && $_POST['renew'] == 1) {
        $clientSecret = genererCodeAleatoire(80);
        $renew = true;
    } else {
        $clientSecret = htmlspecialchars($_POST['client_secret'], ENT_QUOTES);
    }

    if (strlen($nom) > 0 && strlen($clientId) > 0 && strlen($clientSecret) > 0) {
        if ($mode == MODE_AJOUT) {
            $pdo->exec('INSERT INTO ' . $bdd_prefixe . 'oauth_clients (nom, client_id, client_secret, redirect_uri) VALUE ("' . $nom . '", "' . $clientId . '", "' . $clientSecret . '", "' . $redirectURI . '")');
            ajouterSuccesNotification("Client ajouté avec succès");
        } elseif ($mode == MODE_MODIF) {
            $pdo->exec('UPDATE ' . $bdd_prefixe . 'oauth_clients SET nom = "' . $nom . '", client_secret = "' . $clientSecret . '", redirect_uri = "' . $redirectURI . '" WHERE client_id = "' . $clientId . '"');
            ajouterSuccesNotification("Client modifié avec succès." . ($renew ? " Nouveau client secret : " . $clientSecret : ""));
        }

    } else {
        ajouterErreurNotification("Les champs nom, client secret et client id sont obligatoires");
    }

    header('location: liste_oauth.php');
    exit();
}

if (isset($_GET['client_id']) && strlen($_GET['client_id']) > 0) {
    $clientId = htmlspecialchars($_GET['client_id']);
    $requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'oauth_clients WHERE client_id = "' . $clientId . '"');
    $client = $requete->fetch();
    $mode = MODE_MODIF;
} else {
    $mode = MODE_AJOUT;
}

?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un client OAuth2</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />

		<script type="application/javascript" src="js/form_couleur.min.js"></script>
	</head>

	<body>
		<?php include 'haut_page.php';?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'un client OAuth2</h2>

		<form action="form_oauth.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom : </label></div>
					<div class="cellule"><input name="nom" value="<?php if ($mode == MODE_MODIF) {
    echo $client['nom'];
}
?>" required maxlength="64" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="client_id">Client Id : </label></div>
					<div class="cellule"><input name="client_id" value="<?php if ($mode == MODE_MODIF) {echo $client['client_id'];}?>"
                        <?php if ($mode == MODE_MODIF) {echo ' readonly';}?> required maxlength="80" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="client_secret">Client secret : </label></div>
					<div class="cellule"><input name="client_secret" value="<?php if ($mode == MODE_MODIF) {
    echo $client['client_secret'];
} else {
    echo genererCodeAleatoire(80);
}
?>" required readonly /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="redirect_uri">URL de redirection : </label></div>
					<div class="cellule"><input name="redirect_uri" value="<?php if ($mode == MODE_MODIF) {
    echo $client['redirect_uri'];
}
?>" required maxlength="2000" /></div>
				</div>
                <?php if ($mode == MODE_MODIF) {?>
				<div class="ligne">
					<div class="cellule intitule"><label name="renew">Générer un nouveau client secret</label></div>
					<div class="cellule"><input name="renew" type="checkbox" value="1" /></div>
				</div>
                <?php }?>
			</div>
			<p>
                <?php if ($mode == MODE_MODIF) {?><input type="hidden" name="modif" value="1" /><?php }?>
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include 'bas_page.php';?>
	</body>
</html>
