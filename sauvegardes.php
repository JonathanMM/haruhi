<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(SAUVEGARDE);

//On parcours la liste des fichiers
$dossier = dir('sauvegardes/');
$nomSauvegardes = array();
while (false !== ($nomFichier = $dossier->read()))
{
	$coupeNom = explode('.', $nomFichier, 2);
	if($coupeNom[1] == 'sql')
	{
		$nomSauvegardes[] = $coupeNom[0];
	}
}
sort($nomSauvegardes);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Liste des sauvegardes</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>
    <nav id="panneau_page">
        <h2>Liste des sauvegardes</h2>
        <p><a href="creer_sauvegarde.php">Créer une sauvegarde</a></p>
	</nav>

	<div id="cadre_stockage">
		<?php //On s'occupe des messages
			afficherNotification();
		?>
		<table>
			<tr>
				<th>Date</th>
				<th>Actions</th>
			</tr>
			<?php
			foreach($nomSauvegardes as $sauvegarde)
			{
				$date = DateTime::createFromFormat('Y-m-d-G-i-s', $sauvegarde);
				echo '<tr>';
				echo '<td>'.$date->format('Y-m-d G:i:s').'</td>';
				echo '<td>';
				echo '<a href="restaure_sauvegarde.php?nom='.$sauvegarde.'">Restaurer</a>';
				echo ' − ';
				echo '<a href="supp_sauvegarde.php?nom='.$sauvegarde.'">Supprimer</a>';
				echo '</td>';
				echo '</tr>';
			}
			?>
	</div>

	<?php include('bas_page.php'); ?>
	</body>
</html>