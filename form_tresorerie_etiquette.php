<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('tresorerie');
verifierSiUtilisateurAPermission(GERER_TRESORERIE);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($id == 0)
		$mode = MODE_AJOUT;
	else
		$mode = MODE_MODIF;

	$nom = htmlspecialchars($_POST['nom'], ENT_QUOTES);

	if(substr($_POST['couleur'], 0, 1) == '#')
		$couleur = '#'.substr($_POST['couleur'], 1);
	else
		$couleur = htmlspecialchars($_POST['couleur']);

    if(strlen($nom) > 0)
    {
		if($mode == MODE_AJOUT)
			$pdo->exec('INSERT INTO '.$bdd_prefixe.'tresorerie_etiquettes (label, couleur) VALUE ("'.$nom.'", "'.$couleur.'")');
		elseif($mode == MODE_MODIF)
			$pdo->exec('UPDATE '.$bdd_prefixe.'tresorerie_etiquettes SET label = "'.$nom.'", couleur = "'.$couleur.'" WHERE id = '.$id);
	}
	header('location: liste_tresorerie_etiquettes.php');
	exit();
}

if(isset($_GET['id']) && intval($_GET['id']) > 0)
{
	$id = intval($_GET['id']);
	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'tresorerie_etiquettes WHERE id = '.$id);
	$titre = $requete->fetch();
	$mode = MODE_MODIF;
} else
	$mode = MODE_AJOUT;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → <?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'une étiquette</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />

		<script type="application/javascript" src="js/form_couleur.min.js"></script>
	</head>

	<body>
		<?php include('haut_page.php'); ?>

		<h2><?php echo ($mode == MODE_AJOUT ? 'Ajout' : 'Modification'); ?> d'une étiquette</h2>

		<form action="form_tresorerie_etiquette.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="nom">Nom : </label></div>
					<div class="cellule"><input name="nom" value="<?php if($mode == MODE_MODIF) echo $titre['nom']; ?>" required maxlength="128" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="couleur">Couleur : </label></div>
					<div class="cellule"><input name="couleur" type="color" id="couleur" onChange="maj_couleur();" value="<?php if($mode == MODE_MODIF) echo $titre['couleur']; ?>" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"></div>
					<div class="cellule">
						<div class="test_couleur" onClick="choisir_couleur('#46B8F1');" style="background-color:#46B8F1"></div>
						<div class="test_couleur" onClick="choisir_couleur('#ffedb0');" style="background-color:#ffedb0"></div>
						<div class="test_couleur" onClick="choisir_couleur('#f0f0f0');" style="background-color:#f0f0f0"></div>
						<div class="test_couleur" onClick="choisir_couleur('#F0BDE5');" style="background-color:#F0BDE5"></div>
						<div class="test_couleur" onClick="choisir_couleur('#d6d6d6');" style="background-color:#d6d6d6"></div>
						<div class="test_couleur" onClick="choisir_couleur('#FFB5B5');" style="background-color:#FFB5B5"></div>
						<div class="test_couleur" onClick="choisir_couleur('#FFE991');" style="background-color:#FFE991"></div>
						<div class="test_couleur" onClick="choisir_couleur('#CFFECC');" style="background-color:#CFFECC"></div>
					</div>
				</div>
			</div>
			<p>
				<input type="hidden" name="id" value="<?php if($mode == MODE_MODIF) echo $id; ?>" />
				<input type="hidden" name="envoi" value="1" />
				<input id="bouton_valider" type="submit" value="Valider" />
			</p>
		</form>

		<?php include('bas_page.php'); ?>
	</body>
</html>
