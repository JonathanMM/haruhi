<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Haruhi → Connexion</title>
        <link rel="icon" type="image/png" href="<?php echo isset($pathFile) ? $pathFile : ''; ?>images/favicon.png" />

        <link rel="stylesheet" href="<?php echo isset($pathFile) ? $pathFile : ''; ?>principal.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="<?php echo isset($pathFile) ? $pathFile : ''; ?>css/connexion.css" type="text/css" media="screen" />
    </head>

    <body>
        <div id="area-connection">
            <div id="area-background"></div>
            <div id="area-form">
                <h1><?php echo (isset($titreFormulaireConnexion) ? $titreFormulaireConnexion : 'Haruhi'); ?></h1>
                <?php if (isset($titreFormulaireConnexion)) {echo "<h2>Connexion via Haruhi</h2>";}?>

                <?php if (isset($erreur)) {?><p><?php echo '<div class="msg msgErreur">' . $erreur . '</div>'; ?></p><?php }?>

                <form method="post">
                    <div class="formulaire">
                        <div class="ligne">
                            <div class="cellule intitule"><label name="pseudo">Identifiant</label></div>
                            <div class="cellule"><input name="pseudo" required></div>
                        </div>
                        <div class="ligne">
                            <div class="cellule intitule"><label name="mdp">Mot de passe</label></div>
                            <div class="cellule"><input type="password" name="mdp" required></div>
                        </div>
                        <div class="ligne">
                            <div class="cellule intitule">
                                <label name="souvenir">Se souvenir de moi</label>
                            </div>
                            <div class="cellule">
                                <input type="checkbox" name="souvenir" value="1" />
                            </div>
                        </div>
                    </div>
                    <p>
                        <input type="hidden" name="envoi" value="1">
                        <input type="submit" id="bouton_valider" value="Valider">
                    </p>
                </form>

                <p><a href="<?php echo isset($pathFile) ? $pathFile : ''; ?>oubli_mdp.php">J'ai oublié mon mot de passe (et j'ai honte)</a></p>
            </div>
        </div>

        <?php include __DIR__ . '/../../bas_page.php';?>
    </body>
</html>