<?php
session_start();
require_once 'prelude_page.php';
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);

if (!(isset($_GET['id'])) || intval($_GET['id']) <= 0) {
    ajouterErreurNotification("Aucun adhérent n'a été sélectionné.");
    header('location: adherents.php');
    exit();
}

$id = intval($_GET['id']);
$requete = $pdo->query('SELECT nom, prenom, pseudo, courriel FROM ' . $bdd_prefixe . 'adherents WHERE id = ' . $id);
$user = $requete->fetch();

if (is_null($user['courriel']) || strlen($user['courriel']) == 0) {
    ajouterErreurNotification('Impossible de créer un compte à cet adhérent : Aucun courriel n\'a été renseigné dans son profil.');
    header('location: adherents.php');
    exit();
}
//Génération du mot de passe
$mdp = genererCodeAleatoire(8);

if (is_null($user['pseudo']) || strlen($user['pseudo']) == 0) {
    $pseudo = $user['prenom'] . '.' . $user['nom'];
} else {
    $pseudo = $user['pseudo'];
}

$mdp_hash = hash('sha512', $mdp);

$contenu = file_get_contents('./mail/creation-compte.txt');
$contenu = str_replace(array('{membre.pseudo}', '{membre.motdepasse}'), array($pseudo, $mdp), $contenu);
$contenu = parserMailAdherent($contenu, $user);
$courriel = envoyer_mail($user['courriel'], "Inscription sur Haruhi", $contenu, "creationCompte", null, $id, false);
if ($courriel) {
    $pdo->exec('INSERT INTO ' . $bdd_prefixe . 'membres (id, pseudo, mdp, mail, type) VALUES (NULL, "' . $pseudo . '", "' . $mdp_hash . '", "' . $user['courriel'] . '", "0")');
    $last_id = $pdo->lastInsertId();
    $pdo->exec('UPDATE ' . $bdd_prefixe . 'adherents SET id_membre = ' . $last_id . ' WHERE id = ' . $id);
    ajouterSuccesNotification("Le compte a été créé avec succès pour l'adhérent.");
} else {
    ajouterErreurNotification("Une erreur a eu lieu lors de l'envoi du courriel.");
}

header('location: adherents.php');
