<?php

//Infos de connexion
require 'infos_connexion.php';

//On charge nos classes internes
spl_autoload_register(function (string $nomClasse) {
    $path = __DIR__ . '/classes/' . str_replace('\\', '/', $nomClasse) . '.class.php';
    if (file_exists($path)) {
        include_once $path;
    }
});

//PDO
if (isset($bdd_pdo_dns)) {
    $pdoConfig = new BDD\BDDConfiguration($bdd_pdo_dns, $username, $password, $bdd_prefixe);
} else {
    $pdoConfig = new BDD\BDDConfiguration($host, $username, $password, $bdd_name, $bdd_prefixe);
}

$bdd = new BDD\BDDConnexion($pdoConfig);
//Pour compatibilité
$pdo = $bdd->getPdo();
$bdd_prefixe = $bdd->getPrefixe();

//Logs
require __DIR__ . '/vendor/autoload.php';
Logger::configure(array(
    'rootLogger' => array(
        'appenders' => array('errors'),
        'level' => 'WARN',
    ),
    'appenders' => array(
        'errors' => array(
            'class' => 'LoggerAppenderFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%date [%logger] %level %message%newline',
                ),
            ),
            'params' => array(
                'file' => __DIR__ . '/logs/errors.log',
            ),
        ),
        'cron' => array(
            'class' => 'LoggerAppenderFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%date [%logger] %level %message%newline',
                ),
            ),
            'params' => array(
                'file' => __DIR__ . '/logs/cron.log',
            ),
        ),
    ),
    'loggers' => array(
        'cron' => array(
            'appenders' => array('cron'),
            'level' => 'INFO',
        ),
    ),
));

//Permissions
require 'permission.php';

//Fonctions
require 'fonctions.php';
