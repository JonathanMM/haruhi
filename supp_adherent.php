<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	if($_POST['choix'] == "Oui")
	{
		supprimerAdherent($id);
	}
	ajouterSuccesNotification('Le membre a été supprimé avec succès.');
	header('location: adherents.php');
	exit();
} elseif(!(isset($_GET['id'])) || intval($_GET['id']) <= 0)
{
	ajouterErreurNotification('L\'adhérent n\'a pas été spécifié.');
	header('location: adherents.php');
	exit();
}

$id = intval($_GET['id']);
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
$donnees = $requete->fetch();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Suppression d'adhérent</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<h2>Supprimer un adhérent</h2>

	<form action="supp_adherent.php" method="post">
	<p>
		Voulez-vous vraiment supprimer cet adhérent ?
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<input type="hidden" name="envoi" value="1" />
		<input type="submit" value="Oui" name="choix" /> <input type="submit" value="Non" name="choix" />
	</p>
	</form>

	<?php include('bas_page.php'); ?>
	</body>
</html>