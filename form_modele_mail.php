<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(TOUCHE_MODELE_MAIL);

use Configuration\ConfigurationManager;
use Configuration\ConfigurationValue;

$modeles = array(
    'creation-compte' => 'Création d\'un compte',
    'nouveau-mdp' => 'Regénérer le mot de passe',
    'nouvel-adherent' => 'Nouvelle adhésion',
    'rappel-cotisation-0' => 'Rappel de cotisation (aujourd\'hui)',
    'rappel-cotisation-7' => 'Rappel de cotisation (une semaine)',
    'rappel-cotisation-30' => 'Rappel de cotisation (un mois)',
    'renouvellement-adherent' => 'Renouvellement de cotisation'
);

$etape = 0;
if (isset($_GET['action']) && $_GET['action'] == 'modif_structure') {
    $etape = -1;
    $configurationManager = new ConfigurationManager($bdd);
    $contenu = $configurationManager->getConfiguration(ConfigurationManager::MODELE_HTML_MAIL)->getValeur();
} elseif (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    $etape = 1;
    $modele = htmlspecialchars($_POST['modele']);
    if (isset($_POST['test'])) // On veut envoyer un mail de test
    {
        //Pour s'envoyer un mail à soi même, on récupère nos infos
        $infosAdherent = recupererAdherents('id = ' . $_SESSION['id_adherent']);
        $moi = $infosAdherent[0];
        $contenu_mail = file_get_contents('./mail/' . $modele . '.txt');
        $contenu = parserMailAdherent($contenu_mail, $moi);
        $mailEnvoye = envoyer_mail($moi['courriel'], 'Test d\'envoi de courriel depuis la platefore Haruhi', $contenu, 'test');
        if ($mailEnvoye)
            ajouterSuccesNotification('Mail de test envoyé avec succès.');
        else
            ajouterErreurNotification('Problème lors de l\'envoi du mail de test.');
        $etape = 0; //On revient à l'étape 0
    } else {
        $contenu = file_get_contents('./mail/' . $modele . '.txt');
    }
} elseif (isset($_POST['envoi']) && $_POST['envoi'] == 2) {
    $modele = htmlspecialchars($_POST['modele']);
    $contenu = $_POST['contenu'];
    if ($modele == 'structure_mail') {
        $configurationManager = new ConfigurationManager($bdd);
        $structureMail = new ConfigurationValue(ConfigurationManager::MODELE_HTML_MAIL, $contenu);
        $configurationManager->setConfiguration($structureMail);
    } else
        file_put_contents('./mail/' . $modele . '.txt', $contenu);
    ajouterSuccesNotification("Le modèle a bien été modifié");
    header('location: form_modele_mail.php');
    exit();
}

$mode = MODE_MODIF;
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Haruhi → Gestion des modèles de mail</title>
    <link rel="icon" type="image/png" href="images/favicon.png" />

    <link rel="stylesheet" href="principal.css" type="text/css" media="screen" />

    <script type="application/javascript" src="js/form_titre.min.js"></script>
</head>

<body>
    <?php include('haut_page.php'); ?>

    <h2>Gestion des modèles de mail</h2>

    <?php afficherNotification(); ?>

    <form action="form_modele_mail.php" method="post">
        <div class="formulaire">
            <div class="ligne">
                <div class="cellule intitule"><label name="modele">Modèle : </label></div>
                <div class="cellule">
                    <?php if ($etape == 1) {
                        echo $modeles[$modele];
                        echo '<input type="hidden" name="modele" value="' . $modele . '" />';
                    } elseif ($etape == -1) {
                        echo 'Structure des mails';
                        echo '<input type="hidden" name="modele" value="structure_mail" />';
                    } else {
                        echo '<select name="modele">';
                        foreach ($modeles as $id => $nom)
                            echo '<option value="' . $id . '">' . $nom . '</option>';
                        echo '</select>';
                    }
                    ?>
                </div>
            </div>
            <?php
            if ($etape == 1 || $etape == -1) {
            ?>
                <div class="ligne">
                    <div class="cellule intitule"><label name="contenu">Message : </label></div>
                    <div class="cellule">
                        <textarea name="contenu"><?php echo $contenu; ?></textarea>
                    </div>
                </div>
            <?php } ?>
        </div>
        <p>
            <input type="hidden" name="envoi" value="<?php echo ($etape == -1 ? 2 : $etape + 1); ?>" />
            <input id="bouton_valider" type="submit" value="Valider" />
            <?php if ($etape == 0) { ?>
                <input id="bouton_valider" type="submit" name="test" value="Tester" /><br />
                <a href="form_modele_mail.php?action=modif_structure">Modifier la structure des mails</a>
            <?php } ?>
        </p>
    </form>

    <?php include('bas_page.php'); ?>
</body>

</html>