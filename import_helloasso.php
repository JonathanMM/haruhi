<?php
session_start();
require_once('prelude_page.php');
verifierSiFonctionnaliteEstActive('helloasso');
verifierSiUtilisateurAPermission(TOUCHE_ADHERENT);

//On fait un import sur les 6 derniers mois seulement
$dateDebutImport = new DateTime();
$dateDebutImport->sub(new DateInterval('P6M'));
importHelloAsso($dateDebutImport);
ajouterSuccesNotification("Import depuis HelloAsso réalisé avec succès");
header('location: liaison_helloasso.php');
?>