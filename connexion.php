<?php
session_start();
require_once 'configuration.php';
if (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    $pseudo = htmlspecialchars($_POST['pseudo'], ENT_QUOTES);
    $mdp = hash('sha512', $_POST['mdp']);
    $requete = $bdd->query('SELECT *, m.id AS id, a.id AS id_adherent, t.id AS id_titre FROM ' . $bdd->getNomTable('membres') . ' m
				LEFT JOIN ' . $bdd->getNomTable('adherents') . ' a ON a.id_membre = m.id
				LEFT JOIN ' . $bdd->getNomTable('titre') . ' t ON t.id = a.titre
				WHERE m.pseudo = "' . $pseudo . '" AND m.mdp = "' . $mdp . '"');
    if (!($requete === false) && $donnees = $requete->fetch()) {
        initialiserUtilisateur($donnees);

        //On va gérer l'option Se souvenir de moi
        if (isset($_POST['souvenir']) && $_POST['souvenir'] == 1) {
            // On va créer un token que l'on stocke dans un cookie pour la prochaine fois
            genererCookieAutoLogin($donnees['id']);
        }

        header('location: index.php');
        exit();
    } else {
        $erreur = "Identifiant et/ou mot de passe incorrect(s).";
    }
} elseif (autoLogged()) {
    header('location: index.php');
    exit();
}

include 'includes/html/connexion.php';
