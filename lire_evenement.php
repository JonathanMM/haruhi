<?php
session_start();
require_once('prelude_page.php');
verifierSiIdInGet('calendrier.php');

$id = intval($_GET['id']);

$requete = $pdo->query('SELECT *, 
c.id AS idEvenement, c.titre AS nomEvenement, cat.nom AS nomCategorie
FROM '.$bdd_prefixe.'calendrier c
INNER JOIN '.$bdd_prefixe.'calendrier_categories cat ON cat.id = c.type
WHERE c.id = '.$id);
$evenement = $requete->fetch();
if($evenement === false)
{
    ajouterErreurNotification('Vous n\'avez pas le droit de consulter cet événement.');
    header('location: calendrier.php');
    exit();
}

//On récupère les participants
$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'calendrier_participants p
INNER JOIN '.$bdd_prefixe.'adherents a ON a.id = p.idMembre
WHERE p.idEvenement = '.$id);
$participantsSql = $requete->fetchAll();
$organisateur = null;
$participants = array();
foreach($participantsSql as $participant)
{
    $obj = array('nom' => $participant['prenom'].' '.$participant['nom'], 'reponse' => $participant['reponse']);
    if($participant['estOrganisateur'] == 1)
        $organisateur = $obj;
    else
        $participants[] = $obj;
}

//On regarde si on a les droits de modifications
$requete = $pdo->query('SELECT ecriture FROM '.$bdd_prefixe.'calendrier_permissions WHERE idCategorie = '.$evenement['type'].' AND idTitre = '.$_SESSION['id_titre']);
$droits = $requete->fetch();
$peutModifier = $droits['ecriture'] == 1;
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Afficher un événement</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="secondaire_page_avec_tab.css" type="text/css" media="screen" />
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<nav id="panneau_page">
	    <h2><a href="calendrier.php">Calendrier</a></h2>
        <?php
        if($peutModifier)
            echo '<a href="form_evenement.php?id='.$id.'">Modifier cet événement</a><br />';
        ?>
        <a href="export_ics.php?id=<?php echo $id; ?>" download="evenement-<?php echo $id; ?>.ics">Exporter en .ics</a>
	</nav>

	<div id="cadre_stockage"><h3><?php echo $evenement['nomEvenement']; ?></h3>

	<p>Date : <?php echo formater_date_heure($evenement['date']); ?><br />
	Catégorie : <?php echo $evenement['nomCategorie']; ?><br />
	<?php echo $evenement['description']; ?></p>

	<?php if(isset($evenement['lieu']) && $evenement['lieu'] != NULL)
	{ ?>
	<p>Lieu : <?php echo $evenement['lieu']; ?></p>
	<?php }
	if (isset($evenement['latitude']) && $evenement['latitude'] != 0)
	{ ?>
    <!-- DIV xhtml de la carte. La taille peut être spécifiée soit ici,
         dans l'attribut style, soit dans les css.
    -->
    <div id="demo-map" style="border: 1px solid black; width: 800px; height: 150px;"></div>
    <p style="font-size: 7px;"><a href="http://openstreetmap.org/" hreflang="fr" title="OpenStreetMap">Carte OpenStreetMap</a> (Licence CC by-sa 2.0)</p>
    
    
    <!-- Inclusion des bibliothèques javascript OpenLayers et Mapstraction -->
    <script
        type="text/javascript"
        src="http://openlayers.org/api/OpenLayers.js"
    ></script>
    <script
        type="text/javascript"
        src="mapstraction.js"
    ></script>
    
    <script type="text/javascript">
    // Initialisation de la carte.
    var mapstraction = new Mapstraction('demo-map','openlayers');
    mapstraction.addControls({
        pan: false, 
        zoom: 'small',
        map_type: false 
    });

    var centre = new LatLonPoint(<?php echo $evenement['latitude']; ?>,<?php echo $evenement['longitude']; ?>);
    mapstraction.setCenterAndZoom(centre, 16);

    lieu = new Marker(new LatLonPoint(<?php echo $evenement['latitude']; ?>,<?php echo $evenement['longitude']; ?>));
    lieu.setIcon('images/marqueur_geo.png', [18, 31]);
    mapstraction.addMarker(lieu);
    
    </script> 
    <?php } ?>
        <div class="area-participants">
            <?php if(!is_null($organisateur)) { ?>
            <p>Organisateur : <?php echo $organisateur['nom']; ?></p>
            <?php } ?>
            <?php if(count($participants) > 0) { ?>
            <p>Participants :</p>
            <ul>
                <?php
                foreach($participants as $participant)
                {
                    echo '<li>';
                    echo $participant['nom'];
                    echo ' ';
                    switch($participant['reponse'])
                    {
                        case CALENDRIER_REPONSE_NON_REPONDU:
                            echo '(Pas de réponse)';
                            break;
                        case CALENDRIER_REPONSE_REFUS:
                            echo '(Refusé)';
                            break;
                        case CALENDRIER_REPONSE_PROVOISOIRE:
                            echo '(Provisoire)';
                            break;
                        case CALENDRIER_REPONSE_ACCEPTE:
                            echo '(Accepté)';
                            break;
                    }
                    echo '</li>';
                }
                ?>
            </ul>
            <?php } ?>
        </div>
    </div>
	<?php include('bas_page.php'); ?>
	</body>
</html>