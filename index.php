<?php
session_start();
require_once 'prelude_page.php';

use Accueil\AccueilProvider;
use Accueil\AccueilSkeleton;

$provider = new AccueilProvider($bdd);
$accueil = $provider->getAccueil();
$moduleMessage = $accueil->getModule(AccueilSkeleton::MESSAGE);
$message = null;
if (!is_null($moduleMessage)) {
    $message = $moduleMessage->getValeur();
}

?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Accueil</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
		<script type="text/javascript" src="js/index.min.js"></script>
	</head>

	<body>
	<?php include 'haut_page.php';?>

	<?php
$requete = $pdo->query('SELECT * FROM ' . $bdd_prefixe . 'membres m LEFT JOIN ' . $bdd_prefixe . 'adherents a ON a.id_membre = m.id WHERE m.id = ' . $_SESSION['id']);
$donnees = $requete->fetch();
?>

	<div id="colonne_accueil">

		<div class="info_accueil">
		      Bienvenue <?php echo $donnees['prenom'] . ' ' . $donnees['nom']; ?>
		</div>

		<?php if (!is_null($message) && strlen($message) > 0) {
    echo '<div class="info_accueil">';
    echo $message;
    echo '</div>';
}?>

		<div class="info_accueil <?php
$jour = reste_jour($donnees['date_fin_cotisation']);
if ($donnees['date_inscription'] == "0000-00-00") {
    echo ' pas_membre';
} elseif ($jour > 30) {
    echo ' cotisation_ok';
} elseif ($jour <= 30 && $jour > 7) {
    echo ' cotisation_fin_loin';
} elseif ($jour <= 7 && $jour >= 0) {
    echo ' cotisation_fin_proche';
} else {
    echo ' cotisation_fini';
}

?>" >
			Cotisation :
			<?php

if ($donnees['date_fin_cotisation'] != '0000-00-00') {
    echo 'Expire le ' . formater_date($donnees['date_fin_cotisation']) . '.';
} else {
    echo 'Pas adhérent';
}

?>
		</div>

		<?php if ($fonctionnalites_statut['factures']) {?>
		<div class="info_accueil">
			<h3>Factures</h3>
			<ul>
			<?php
$requete = $pdo->query('SELECT *, f.id AS id FROM ' . $bdd_prefixe . 'factures f
			INNER JOIN ' . $bdd_prefixe . 'factures_type t ON t.id = f.type
			WHERE adherent = ' . $_SESSION['id_adherent'] . ' AND visibilite = 1 ORDER BY date DESC LIMIT 0, 3');
    if (!($requete === false)) {
        $factures = $requete->fetchAll();
        foreach ($factures as $facture) {
            echo '<li>#' . $facture['id'] . ' : ' . formater_date($facture['date']) . ' ' . number_format($facture['somme'], 2, ',', ' ') . ' € ';
            if ($facture['externe'] == 0) //Facture interne
            {
                echo '<a href="voir_facture.php?id=' . $facture['id'] . '">';
            } else //Facture externe
            {
                echo '<a target="_blank" href="' . $facture['lien'] . '">';
            }

            echo 'Afficher</a></li>';
        }
        echo '<li><a href="mes_factures.php">Plus de factures</a></li>';
    }?>
			</ul>
		</div>
		<?php }?>

		<?php if ($fonctionnalites_statut['calendrier']) {?>
		<div class="info_accueil">
			<h3>Votre agenda</h3>
			<ul>
			<?php $requete = $pdo->query('SELECT *, c.id AS idEvenement
			FROM ' . $bdd_prefixe . 'calendrier c
			INNER JOIN ' . $bdd_prefixe . 'calendrier_participants p ON p.idEvenement = c.id
			WHERE c.date >= CURRENT_DATE() AND p.idMembre = ' . $_SESSION['id'] . '
			ORDER BY c.date ASC;');
    if (!($requete === false)) {
        $events = $requete->fetchAll();
        foreach ($events as $event) {
            ?>
						<ol><?php echo formater_date_heure($event['date']); ?> : <?php echo $event['titre']; ?>
						(Participation :
						<select class="changer-participation" data-id="<?php echo $event['idEvenement']; ?>">
							<option disabled value="<?php echo CALENDRIER_REPONSE_NON_REPONDU; ?>" <?php if ($event['reponse'] == CALENDRIER_REPONSE_NON_REPONDU) {
                echo 'selected';
            }
            ?>>Aucune réponse</option>
							<option value="<?php echo CALENDRIER_REPONSE_ACCEPTE; ?>" <?php if ($event['reponse'] == CALENDRIER_REPONSE_ACCEPTE) {
                echo 'selected';
            }
            ?>>Je participerai</option>
							<option value="<?php echo CALENDRIER_REPONSE_PROVISOIRE; ?>" <?php if ($event['reponse'] == CALENDRIER_REPONSE_PROVISOIRE) {
                echo 'selected';
            }
            ?>>Je participerai peut-être</option>
							<option value="<?php echo CALENDRIER_REPONSE_REFUS; ?>" <?php if ($event['reponse'] == CALENDRIER_REPONSE_REFUS) {
                echo 'selected';
            }
            ?>>Je ne participerai pas</option>
						</select>
						)
						<a href="lire_evenement.php?id=<?php echo $event['idEvenement']; ?>">Lire</a></ol>
						<?php
}
        if (count($events) == 0) {
            echo '<ol><i>Aucun évenement prévu prochainement.</i></ol>';
        }

    }?>
			</ul>
		</div>

		<div class="info_accueil">
			<h3>À venir</h3>
			<ul>
			<?php $requete = $pdo->query('SELECT *, c.id AS idEvenement FROM ' . $bdd_prefixe . 'calendrier c
			INNER JOIN ' . $bdd_prefixe . 'calendrier_permissions p ON p.idCategorie = c.type
			WHERE date >= CURRENT_DATE() AND date <= DATE_ADD(CURRENT_DATE(), INTERVAL 3 MONTH) AND p.idTitre = ' . $_SESSION['id_titre'] . ' AND p.lecture = 1
			ORDER BY date ASC;');
    if (!($requete === false)) {
        $events = $requete->fetchAll();
        foreach ($events as $event) {
            echo '<ol>' . formater_date_heure($event['date']) . ' : ' . $event['titre'] . ' <a href="lire_evenement.php?id=' . $event['idEvenement'] . '">Lire</a></ol>';
        }
        if (count($events) == 0) {
            echo '<ol><i>Aucun évenement prévu prochainement.</i></ol>';
        }

    }?>
			</ul>
		</div>
		<?php }?>

	</div>

	<?php include 'bas_page.php';?>
	</body>
</html>
