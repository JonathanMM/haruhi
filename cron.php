<?php
require_once('configuration.php');
$fichier_derniere_execution_chemin = __DIR__ . '/config/cron-derniere-execution.txt';
$logger = Logger::getLogger('cron');

$date_derniere_execution_brut = file_get_contents($fichier_derniere_execution_chemin);
$nbExecution = 1;
$date = null;
$auj = new DateTime();
if ($date_derniere_execution_brut != '') {
    $date = new DateTime($date_derniere_execution_brut);
    $diff = $date->diff($auj);
    $nbExecution = $diff->days;

    //Et on met plus un à la date
    $date->add(new DateInterval("P1D"));
} else {
    $date = new DateTime();
    $logger->info('Aucune date d\'ancienne execution détectée');
}

$nJours = [30, 7, 0];

if ($nbExecution > 0) {
    $logger->info('Execution du cron. Précédente execution il y a ' . $nbExecution . ' jour(s)');
    //Hello Asso
    try {
        importHelloAsso($date);
    } catch (Exception $e) {
        if ($logger->isErrorEnabled())
            $logger->error('Exception durant le traitement de helloasso : ' . $e->getMessage() . "\n" . $e->getTraceAsString());
    }

    //Envoi des mails
    try {
        $logger->info('Préparation des mails de rappel de cotisations');
        for ($i = 0; $i < $nbExecution; $i++) {
            //On récupère tous les adhérents qui ont une date de fin de cotisation dans le créneau voulu
            foreach ($nJours as $jours) {
                $contenu_mail = file_get_contents(__DIR__ . '/mail/rappel-cotisation-' . $jours . '.txt');
                if ($contenu_mail != '') //Y'a un mail à envoyer
                {
                    $jour = clone $date;
                    $jour->add(new DateInterval("P" . $jours . "D"));

                    $adherents = recupererAdherents('date_fin_cotisation = "' . $jour->format("Y-m-d") . '" AND (NOT courriel IS NULL OR courriel != "")');

                    foreach ($adherents as $infosAdherent) {
                        $contenu = parserMailAdherent($contenu_mail, $infosAdherent);
                        if (!is_null($contenu) && strlen($contenu) > 0)
                            envoyer_mail($infosAdherent['courriel'], "Rappel de cotisation pour l'association " . $infos_nomasso, $contenu, "cotisation", new DateTime(), $infosAdherent['id']);
                        else
                            $logger->warn("Tentative d'envoi d'un mail de rappel de cotisation sans contenu !");
                    }
                }
            }

            //Et on met plus un à la date
            $date->add(new DateInterval("P1D"));
        }
    } catch (Exception $e) {
        if ($logger->isErrorEnabled())
            $logger->error('Exception durant l\'envoi des courriels : ' . $e->getMessage() . "\n" . $e->getTraceAsString());
    }

    //Si on est lundi, on fait une sauvegarde
    try {
        if ($auj->format('w') == 1) {
            $logger->info('Réalisation d\'une sauvegarde');
            sauvegarder(__DIR__ . '/');
        }
    } catch (Exception $e) {
        if ($logger->isErrorEnabled())
            $logger->error('Exception durant la sauvegarde : ' . $e->getMessage() . "\n" . $e->getTraceAsString());
    }

    //Maintenant que c'est fini, on met à jour le fichier
    $fichier = fopen($fichier_derniere_execution_chemin, 'w');
    fwrite($fichier, $auj->format("Y-m-d"));
    fclose($fichier);
    $logger->info('Fin de l\'execution du cron');
} else
    $logger->info('Pas de cron à executer');
