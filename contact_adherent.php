<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(LISTE_ADHERENT);

if(isset($_POST['envoi']) && $_POST['envoi'] == 1)
{
	$id = intval($_POST['id']);
	$titre = $_POST['sujet'];
	$contenu = nl2br($_POST['corps']);

	$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$id);
	$user = $requete->fetch();

	$ok = envoyer_mail($user['courriel'], $titre, $contenu, 'contact', null, $id);
	if($ok)
		ajouterSuccesNotification('Le courriel a été envoyé avec succès.');
	else
		ajouterErreurNotification('Un problème a eu lieu lors de l\'envoi');
	header('location: voir_adherent.php?id='.$id);
	exit();
}

verifierSiIdInGet('adherents.php');

$id = intval($_GET['id']);
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Contacter un adhérent</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
		<?php include('haut_page.php'); ?>
		<form action="contact_adherent.php" method="post">
			<div class="formulaire">
				<div class="ligne">
					<div class="cellule intitule"><label name="sujet">Sujet</label></div>
					<div class="cellule"><input name="sujet" /></div>
				</div>
				<div class="ligne">
					<div class="cellule intitule"><label name="corps">Message</label></div>
					<div class="cellule"><textarea name="corps"></textarea></div>
				</div>
			</div>
			<input type="hidden" name="envoi" value="1" />
			<input type="hidden" name="id" value="<?php echo $id; ?>" />
			<input id="bouton_valider" type="submit" value="Valider" />
		</form>
		<?php include('bas_page.php'); ?>
	</body>
</html>
