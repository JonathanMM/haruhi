<?php
class SystemeExterneXenForo implements SystemeExterneDO
{
    private $_listeGroup;
    private $_subscriberML;

    public function __construct()
    {
        global $fileDir_xenforo;
        
        require_once($fileDir_xenforo . '/src/XF.php');
        XF::start($fileDir_xenforo);

        $this->_listeGroup = null;
        $this->_subscriberSE = array();
    }

    public function getAllSE()
    {
        if(is_null($this->_listeGroup))
        {
            $groupes = \XF::finder('XF:UserGroup')->fetch();
            $this->_listeGroup = array();
            foreach($groupes as $groupe)
            {
                $this->_listeGroup[] = array(
                    'adresse' => $groupe->get('user_group_id'),
                    'nom' => $groupe->get('title')
                );
            }
        }
        
        return $this->_listeGroup;
    }

    public function getSubscriberOfSE($idSE)
    {
        if(!isset($this->_subscriberSE[$idSE]))
        {
            $finder = \XF::finder('XF:User');
            //Vu que secondary_group_ids est une liste, on doit utiliser ça pour faire une recherche dedans
            $users = $finder->whereSql("FIND_IN_SET(". $finder->quote($idSE) .", ". $finder->columnSqlName('secondary_group_ids').")")->fetch();
            $this->_subscriberSE[$idSE] = array();
            foreach($users as $user)
            {
                $this->_subscriberSE[$idSE][] = array('pseudo' => $user->get('username'));
            }
        }
        
        return array('unique' => $this->_subscriberSE[$idSE]);
    }

    public function addSubscriberOfSE($idSE, $infosAdherents)
    {
        if(!isset($infosAdherents['pseudo']))
            return;

        $pseudo = $infosAdherents['pseudo'];
        if(is_null($pseudo) || strlen($pseudo) == 0)
            return; //Si pas de pseudo, on ne l'ajoute pas
        
        //Et on l'ajoute dans le groupe
        $user = $this->getUser($pseudo);
        if(is_null($user)) //Utilisateur non trouvé
            return;
        $groups = $user->get('secondary_group_ids');
        if(in_array($idSE, $groups)) //Déjà dans le groupe
            return;
        $groups[] = $idSE;
        $user->set('secondary_group_ids', $groups);
        return $user->save();
    }

    public function deleteSubscriberOfSE($idSE, $infosAdherents)
    {
        if(!isset($infosAdherents['pseudo']))
            return;
        $pseudo = $infosAdherents['pseudo'];
        if(is_null($pseudo) || strlen($pseudo) == 0)
            return; //Si pas de pseudo, on ne le retire pas

        //Et on le retire du groupe
        $user = $this->getUser($pseudo);
        if(is_null($user)) //Utilisateur non trouvé
            return;
            
        $groups = $user->get('secondary_group_ids');
        $positionDansTableau = array_search($idSE, $groups);
        if($positionDansTableau === false) //N'est pas dans les groupes
            return;
        array_splice($groups, $positionDansTableau, 1);
        $user->set('secondary_group_ids', $groups);
        return $user->save();
    }

    private function getUser($pseudo)
    {
        $finder = \XF::finder('XF:User');
        return $finder->where('username', $pseudo)->fetchOne();
    }

    public function createSE($idSE, $infosML = null)
    {
        throw new Exception("Non supporté");
    }

    private function isExist($idSE)
    {
        $listeML = $this->getAllSE();
        foreach($listeML as $infosML)
        {
            if($infosML['adresse'] == $nomML)
                return true;
        }
        
        return false;
    }

    public function getInformationsSE($idSE)
    {
        throw new Exception("Non supporté");
    }

    public static function estActif()
    {
        global $fonctionnalites_statut;
        return isset($fonctionnalites_statut['xenforo']) && $fonctionnalites_statut['xenforo'];
    }

	public static function estModifiable()
    {
        return false;
    }
}
?>