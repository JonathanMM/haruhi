<?php
require __DIR__ . '/../../vendor/autoload.php';
use \Ovh\Api;

class SystemeExterneOVH implements SystemeExterneDO
{
    private $_ovh;
    private $_domaine;
    private $_listeML;
    private $_subscriberML;

    public function __construct()
    {
        global $domaineEmail_ovh;
        $this->_listeML = null;
        $this->_subscriberML = array();
        $this->_ovh = $this->getOVHAPI();
        $this->_domaine = $domaineEmail_ovh;
    }

    private function getOVHAPI()
    {
        global $applicationKey_ovh, $applicationSecret_ovh, $endpoint_ovh, $consumerKey_ovh;

        return new Api( $applicationKey_ovh,
                        $applicationSecret_ovh,
                        $endpoint_ovh,
                        $consumerKey_ovh);
    }

    public function getAllSE()
    {
        if(is_null($this->_listeML))
        {
            $listeML = $this->_ovh->get('/email/domain/'.$this->_domaine.'/mailingList');
            $this->_listeML = array();
            foreach($listeML as $nomML)
            {
                $this->_listeML[] = array(
                    'adresse' => $nomML,
                    'nom' => $nomML
                );
            }
        }
        
        return $this->_listeML;
    }

    public function getSubscriberOfSE($nomML)
    {
        if(!isset($this->_subscriberML[$nomML]))
        {
            $listeSubscriber = $this->_ovh->get('/email/domain/'.$this->_domaine.'/mailingList/'.$nomML.'/subscriber');
            $this->_subscriberML[$nomML] = array();
            foreach($listeSubscriber as $abonneML)
            {
                $this->_subscriberML[$nomML][] = array(
                    'courriel' => $abonneML
                );
            }
        }

        return array('unique' => $this->_subscriberML[$nomML]);
    }

    public function addSubscriberOfSE($nomML, $infosAdherents)
    {
        if(!isset($infosAdherents['courriel']))
            return;

        $courriel = $infosAdherents['courriel'];
        if(is_null($courriel) || strlen($courriel) == 0)
            return; //Si pas de mail, on ne l'ajoute pas

        if(!isset($this->_subscriberML[$nomML]))
            $this->getSubscriberOfSE($nomML);
        
        foreach($this->_subscriberML[$nomML] as $infosAbonneML) //S'il est déjà abonné, on ne l'ajoute pas une seconde fois
        {
            if($courriel == $infosAbonneML['courriel'])
                return null;
        }

        return $this->_ovh->post('/email/domain/'.$this->_domaine.'/mailingList/'.$nomML.'/subscriber', array(
            'email' => $courriel
        ));
    }

    public function deleteSubscriberOfSE($nomML, $infosAdherents)
    {
        if(!isset($infosAdherents['courriel']))
            return;

        $courriel = $infosAdherents['courriel'];
        if(is_null($courriel) || strlen($courriel) == 0)
            return; //Si pas de mail, on ne peut pas le supprimer

        return $this->_ovh->delete('/email/domain/'.$this->_domaine.'/mailingList/'.$nomML.'/subscriber/'.$courriel);
    }

    public function createSE($nomML, $infosML = null)
    {
        global $infos_adressemail;
        $nomML = strtolower($nomML);
        if($this->isExist($nomML))
            return null;

        return $this->_ovh->post('/email/domain/'.$this->_domaine.'/mailingList', array(
            'language' => is_null($infosML) ? 'fr' : $infosML['langue'] ,
            'name' => $nomML,
            'options' => is_null($infosML) ? 
                            array('subscribeByModerator' => true, 'usersPostOnly' => true, 'moderatorMessage' => false) :
                            array(
                                'subscribeByModerator' => $infosML['adhesionsModerees'], 
                                'usersPostOnly' => $infosML['messagesAbonnesSeulement'], 
                                'moderatorMessage' => $infosML['messagesModerees']
                            ),
            'ownerEmail' => is_null($infosML) ? $infos_adressemail : $infosML['proprietaire'],
            'replyTo' => is_null($infosML) ? 'systemeExterne' : $infosML['reponseA']
        ));
    }

    private function isExist($nomML)
    {
        $listeML = $this->getAllSE();
        foreach($listeML as $infosML)
        {
            if($infosML['adresse'] == $nomML)
                return true;
        }
        
        return false;
    }

    public function getInformationsSE($nomML)
    {
        $infos = $this->_ovh->get('/email/domain/'.$this->_domaine.'/mailingList/'.$nomML);

        return array(
            'langue' => $infos['language'],
            'adhesionsModerees' => $infos['options']['subscribeByModerator'],
            'messagesAbonnesSeulement' => $infos['options']['usersPostOnly'],
            'messagesModerees' => $infos['options']['moderatorMessage'],
            'proprietaire' => $infos['ownerEmail'],
            'reponseA' => $infos['replyTo']
        );
    }

    public static function estActif()
    {
        global $fonctionnalites_statut;
        return isset($fonctionnalites_statut['ovh']) && $fonctionnalites_statut['ovh'];
    }

	public static function estModifiable()
    {
        global $fonctionnalites_statut;
        return isset($fonctionnalites_statut['ovh']) && $fonctionnalites_statut['ovh'];
    }
}
?>