<?php
class SystemeExterneHaruhi implements SystemeExterneDO
{
    private $_listeCourrielAdherent;
    private $_idMLs;

    public function __construct()
    {
        //On va charger la liste des abonnés
        $this->_idMLs = array();
        $this->chargerCourrielsML();
    }

    public function getAllSE()
    {
        global $pdo, $bdd_prefixe;
        
        $requete = $pdo->query('SELECT nom, adresse FROM '.$bdd_prefixe.'ml');
        if($requete === false)
            return array();
        
        $liste = $requete->fetchAll();
        if($liste === false)
            return array();
        
        $retour = array();
        foreach($liste as $ml)
        {
            $retour[] = array(
                'adresse' => $ml['adresse'],
                'nom' => $ml['nom']
            );
        }
        return $retour;
    }

    public function createSE($nomML, $infosML = null)
    {
        global $pdo, $bdd_prefixe;
        $snomML = strtolower(htmlspecialchars($nomML));
        $requete = $pdo->query('SELECT id FROM '.$bdd_prefixe.'ml WHERE adresse = "'.$snomML.'"');
        if($requete === false)
            return;
        
        $liste = $requete->fetch();
        
        if($liste === false)
        {
            $pdo->exec('INSERT INTO '.$bdd_prefixe.'ml (id, nom, adresse, classe, newsletter) VALUES 
                            (NULL, 
                            "'.(is_null($infosML) || !isset($infosML['nom']) ? $snomML : $infosML['nom']).'", 
                            "'.$snomML.'",
                            "'.(is_null($infosML) || !isset($infosML['classe']) ? 'SystemeExterneOVH' : $infosML['classe']).'",
                            '.(is_null($infosML) || !isset($infosML['isNewsletter']) ? '0' : ($infosML['isNewsletter'] ? '1' : '0')).')');
            $this->_idMLs[$snomML] = $pdo->lastInsertId();
        }
    }

    public function getSubscriberOfSE($nomML)
    {
        global $pdo, $bdd_prefixe;
        
        $idListe = $this->getIdML($nomML);
    
        $carnetAdresse = array(
            'externes' => array(),
            'adherents' => array()
        );

        //Dans la liste envoyé, on va mettre l'identifiant unique dans toutes les cases, pour être sûr que ça colle !
    
        //On récupère d'abort les externes
        $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml_externe WHERE id_ml = '.$idListe);
        $adresses = $requete->fetchAll();
        if(!($adresses === false))
        {
            foreach($adresses as $adresse)
            {
                $carnetAdresse['externes'][] = array(
                    'courriel' => $adresse['courriel'],
                    'pseudo' => $adresse['courriel']
                );
            }
        }
    
        //Puis les adherents
        $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml_lien l
        INNER JOIN '.$bdd_prefixe.'adherents a ON a.id = l.id_entite
        WHERE l.id_ml = '.$idListe .' AND l.is_titre = 0');
        $adresses = $requete->fetchAll();
        $listeIdentifiantsAdherents = array();
        if(!($adresses === false))
        {
            foreach($adresses as $adresse)
            {
                $carnetAdresse['adherents'][] = array(
                    'courriel' => $adresse['courriel'],
                    'pseudo' => $adresse['pseudo']
                );
                $listeIdentifiantsAdherents[] = $adresse['courriel'];
                $listeIdentifiantsAdherents[] = $adresse['pseudo'];
            }
        }

        //Enfin les titres, dans ce cas là, on retrouve les adhérents attachés et on les met dans la case adhérent
        $requete = $pdo->query('SELECT *, t.id as id FROM '.$bdd_prefixe.'ml_lien l
        INNER JOIN '.$bdd_prefixe.'titre t ON t.id = l.id_entite
        WHERE l.id_ml = '.$idListe .' AND l.is_titre = 1');
        $titres = $requete->fetchAll();
        if(!($titres === false))
        {
            foreach($titres as $titre)
            {
                //Pour chaque titre, on doit récupérer la liste des courriels des adhérents concernés
                $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents 
                WHERE titre = '.$titre['id']);
                $adresses = $requete->fetchAll();
                if(!($adresses === false))
                {
                    foreach($adresses as $adresse)
                    {
                        //On évite quand même les doublons
                        if(!in_array($adresse['courriel'], $listeIdentifiantsAdherents) && !in_array($adresse['pseudo'], $listeIdentifiantsAdherents))
                            $carnetAdresse['adherents'][] = array(
                                'courriel' => $adresse['courriel'],
                                'pseudo' => $adresse['pseudo']
                            );
                    }
                }
            }
        }
        
        return $carnetAdresse;
    }

    public static function getDisplayableSubscriberOfML($idListe)
    {
        global $pdo, $bdd_prefixe;
    
        $carnetAdresse = array(
            'externes' => array(),
            'adherents' => array(),
            'titres' => array()
        );
    
        //On récupère d'abort les externes
        $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml_externe WHERE id_ml = '.$idListe);
        $adresses = $requete->fetchAll();
        if(!($adresses === false))
        {
            foreach($adresses as $adresse)
            {
                $carnetAdresse['externes'][] = array(
                    'courriel' => $adresse['courriel'],
                    'label' => $adresse['courriel'],
                    'idEntite' => $adresse['id']
                );
            }
        }
    
        //Puis les adherents
        $requete = $pdo->query('SELECT *, a.id as id FROM '.$bdd_prefixe.'ml_lien l
        INNER JOIN '.$bdd_prefixe.'adherents a ON a.id = l.id_entite
        WHERE l.id_ml = '.$idListe .' AND l.is_titre = 0');
        $adresses = $requete->fetchAll();
        if(!($adresses === false))
        {
            foreach($adresses as $adresse)
            {
                $carnetAdresse['adherents'][] = array(
                    'courriel' => $adresse['courriel'],
                    'label' => $adresse['prenom'].' '.$adresse['nom'],
                    'idEntite' => $adresse['id']
                );
            }
        }
    
        //Enfin les titres
        $carnetAdresse['titres'] = self::getTitleSubscriber($idListe);
    
        return $carnetAdresse;
    }

    private static function getTitleSubscriber($idListe)
    {
        global $pdo, $bdd_prefixe;
        $liste = array();

        $requete = $pdo->query('SELECT *, t.id as id FROM '.$bdd_prefixe.'ml_lien l
        INNER JOIN '.$bdd_prefixe.'titre t ON t.id = l.id_entite
        WHERE l.id_ml = '.$idListe .' AND l.is_titre = 1');
        $adresses = $requete->fetchAll();
        if(!($adresses === false))
        {
            foreach($adresses as $adresse)
            {
                $liste[] = array(
                    'courriel' => null,
                    'label' => $adresse['nom'],
                    'idEntite' => $adresse['id']
                );
            }
        }

        return $liste;
    }

    public static function addTitle($idListe, $idTitre)
    {
        global $pdo, $bdd_prefixe;
        $pdo->exec('INSERT INTO '.$bdd_prefixe.'ml_lien (id, id_ml, id_entite, is_titre) VALUES (NULL, '.$idListe.', '.$idTitre.', 1)');
    }

    private function getIdML($nomML)
    {
        if(isset($this->_idMLs[$nomML]))
            return $this->_idMLs[$nomML];
        
        $idListe = intval($this->getIdMailingList($nomML));
    
        if(is_null($idListe))
            throw new Exception('La liste de diffusion « '.$nomML.' » n\'existe pas.');

        $this->_idMLs[$nomML] = $idListe;

        return $idListe;
    }

    private function getIdMailingList($nomML)
    {
        global $pdo, $bdd_prefixe;
        $sNom = htmlspecialchars($nomML);
        $requete = $pdo->query('SELECT id FROM '.$bdd_prefixe.'ml WHERE adresse = "'.$sNom.'"');
        if($requete === false)
            return null;
        
        $liste = $requete->fetch();
        if($liste === false)
            return null;
        
        return $liste['id'];
    }

    private function chargerCourrielsML()
    {
        $adherents = recupererAdherents();
        $listeCourrielAdherent = array();
        foreach($adherents as $adherent)
        {
            if(!is_null($adherent['courriel']) && strlen($adherent['courriel']) > 0)
                $listeCourrielAdherent[$adherent['courriel']] = $adherent['id'];
            
            if(!is_null($adherent['pseudo']) && strlen($adherent['pseudo']) > 0)
                $listeCourrielAdherent[$adherent['pseudo']] = $adherent['id'];
        }
    
        $this->_listeCourrielAdherent = $listeCourrielAdherent;
    }

    public function addSubscriberOfSE($nomML, $infosAdherents)
    {
        global $pdo, $bdd_prefixe;

        $identifiant = '';
        if(!isset($infosAdherents['courriel']) || is_null($infosAdherents['courriel']) || strlen($infosAdherents['courriel']) == 0)
        {
            if(!isset($infosAdherents['pseudo']) || is_null($infosAdherents['pseudo']) || strlen($infosAdherents['pseudo']) == 0)
                return;
            
            $identifiant = $infosAdherents['pseudo'];
        } else {
            $identifiant = $infosAdherents['courriel'];
        }

        $idListe = $this->getIdML($nomML);

        //On récupère aussi la liste des titres
        $titres = self::getTitleSubscriber($idListe);
        //De ces titres, on récupère la liste des adhérents
        $adherentsInTitres = array();
        foreach($titres as $titre)
        {
            $adherents = recupererAdherents('titre = '.$titre['idEntite']);
            foreach($adherents as $adherent)
            {
                if(!in_array($adherent['id'], $adherentsInTitres))
                    $adherentsInTitres[] = $adherent['id'];
            }
        }

        if(array_key_exists($identifiant, $this->_listeCourrielAdherent))
        {
            //C'est un adhérent
            $idAdherent = $this->_listeCourrielAdherent[$identifiant];
            if(!in_array($idAdherent, $adherentsInTitres)) //On ajoute pas un adhérent qui est déjà dans la ML via un title
                $pdo->exec('INSERT INTO '.$bdd_prefixe.'ml_lien (id, id_ml, id_entite, is_titre) VALUES (NULL, '.$idListe.', '.$idAdherent.', 0)');
        } else {
            //C'est un externe
            $pdo->exec('INSERT INTO '.$bdd_prefixe.'ml_externe (id, id_ml, courriel) VALUES (NULL, '.$idListe.', "'.htmlspecialchars($identifiant).'")');
        }
    }

    public function deleteSubscriberOfSE($nomML, $infosAdherents)
    {
        global $pdo, $bdd_prefixe;

        $identifiant = '';
        if(!isset($infosAdherents['courriel']) || is_null($infosAdherents['courriel']) || strlen($infosAdherents['courriel']) == 0)
        {
            if(!isset($infosAdherents['pseudo']) || is_null($infosAdherents['pseudo']) || strlen($infosAdherents['pseudo']) == 0)
                return;
            
            $identifiant = $infosAdherents['pseudo'];
        } else {
            $identifiant = $infosAdherents['courriel'];
        }

        $idListe = $this->getIdML($nomML);

        if(array_key_exists($identifiant, $this->_listeCourrielAdherent))
        {
            //C'est un adhérent
            $idAdherent = $this->_listeCourrielAdherent[$identifiant];
            $pdo->exec('DELETE FROM '.$bdd_prefixe.'ml_lien WHERE id_ml = '.$idListe.' AND is_titre = 0 AND id_entite = "'.$idAdherent.'"');
        } else {
            //C'est un externe
            $pdo->exec('DELETE FROM '.$bdd_prefixe.'ml_externe WHERE id_ml = '.$idListe.' AND courriel = "'.htmlspecialchars($identifiant).'"');
        }
    }

    public function getInformationsSE($nomML)
    {
        global $pdo, $bdd_prefixe;

        $requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'ml WHERE adresse = "'.strtolower(htmlspecialchars($nomML)).'"');
        $mailingList = $requete->fetch();

        return array(
            'id' => $mailingList['id'],
            'nom' => $mailingList['nom'],
            'isNewsletter' => ($mailingList['newsletter'] == 1)
        );
    }

    public static function getNomMLFromId($idList)
    {
        global $bdd_prefixe;
        $requete = 'SELECT * FROM '.$bdd_prefixe.'ml WHERE id = "'.$idList.'"';
        return self::getNomMLFromSQLQuery($requete)[0];
    }

    public static function getNomMLFromIdTitle($idTitre)
    {
        global $bdd_prefixe;
        $requete = 'SELECT * FROM '.$bdd_prefixe.'ml_lien l
        INNER JOIN '.$bdd_prefixe.'ml m ON l.id_ml = m.id
        WHERE l.is_titre = 1 AND l.id_entite = '.$idTitre;
        return self::getNomMLFromSQLQuery($requete);
    }

    public static function getNomMLFromIdAdherent($idAdherent)
    {
        global $bdd_prefixe;
        $requete = 'SELECT * FROM '.$bdd_prefixe.'ml_lien l
        INNER JOIN '.$bdd_prefixe.'ml m ON l.id_ml = m.id
        WHERE l.is_titre = 0 AND l.id_entite = '.$idAdherent;
        return self::getNomMLFromSQLQuery($requete);
    }

    private static function getNomMLFromSQLQuery($sql)
    {
        global $pdo;
        $requete = $pdo->query($sql);
        $listeML = array();
        foreach($requete->fetchAll() as $ml)
        {
            $listeML[] = array(
                'adresse' => $ml['adresse'],
                'classe' => $ml['classe']
            );
        }
        return $listeML;
    }

    public static function estActif()
    {
        return true;
    }

	public static function estModifiable()
    {
        return true;
    }
}
?>