<?php
interface SystemeExterneDO
{
	public static function estActif();
	public static function estModifiable();
	public function getAllSE();
	public function getInformationsSE($nomML);
	public function createSE($nomML, $infosML = null);
	public function getSubscriberOfSE($nomML);
	public function addSubscriberOfSE($nomML, $infosAdherents);
	public function deleteSubscriberOfSE($nomML, $infosAdherents);
}
?>
