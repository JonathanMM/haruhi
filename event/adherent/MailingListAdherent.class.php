<?php
class MailingListAdherent implements AdherentEvent
{
    public static function creer($infosAdherent)
    {
        global $fonctionnalites_statut;
        if(!$fonctionnalites_statut['se'])
            return null;

        //À la création, le seul truc qui peut impliquer qu'un adhérent rejoigne une mailing list est son titre
        if((!is_null($infosAdherent['courriel']) || !is_null($infosAdherent['pseudo'])) && $infosAdherent['titre'] != 0)
        {
            $classesSystemeExterneDO = importerEvent('systemeExterne');
            $SystemeExterneDOs = getImplementations($classesSystemeExterneDO);
            
            //On récupère toutes les ML où le titre est impliqué
            $infosMLs = SystemeExterneHaruhi::getNomMLFromIdTitle($infosAdherent['titre']);

            foreach($infosMLs as $infosML)
            {
                $classes = ['SystemeExterneHaruhi', $infosML['classe']];
                foreach($classes as $nomClasse)
                {
                    try
                    {
                        $classe = $SystemeExterneDOs[$nomClasse];
                        $nomML = $infosML['adresse'];
                        $classe->addSubscriberOfSE($nomML, $infosAdherent);
                    } catch (Exception $e)
                    {
                        ajouterErreurNotification("Une erreur a eu lieu lors de la synchronisation des systèmes externes : ".$e->getMessage());
                    }
                }
            }
        }
    }

    public static function modifier($idAdherent, $infosAdherent, $anciennesInfosAdherent)
    {
        global $fonctionnalites_statut;
        if(!$fonctionnalites_statut['se'])
            return null;
        //Pas de changement, on sort
        if($anciennesInfosAdherent['courriel'] == $infosAdherent['courriel'] && $anciennesInfosAdherent['pseudo'] == $infosAdherent['pseudo'] && $anciennesInfosAdherent['titre'] == $infosAdherent['titre'])
            return null;

        //Vu qu'il ne s'agit que d'une modification, on ne va rien faire côté Haruhi
        $classesSystemeExterneDO = importerEvent('systemeExterne');
        $classesSystemeExterneDO = array_diff($classesSystemeExterneDO, array('SystemeExterneHaruhi'));
        $SystemeExterneDOs = getImplementations($classesSystemeExterneDO);

        //On va commencer par l'enlever de toutes les ML où il était
        if(!is_null($anciennesInfosAdherent['courriel']) || !is_null($anciennesInfosAdherent['pseudo']))
        {
            //On récupère les titres dans tous les cas car que ce soit le titre ou le courriel, on l'enlève dans les deux cas
            $infosMLs = SystemeExterneHaruhi::getNomMLFromIdTitle($anciennesInfosAdherent['titre']);

            if($anciennesInfosAdherent['courriel'] != $infosAdherent['courriel'] || $anciennesInfosAdherent['pseudo'] != $infosAdherent['pseudo'])
                $infosMLs = array_merge($infosMLs, SystemeExterneHaruhi::getNomMLFromIdAdherent($idAdherent));

            $infosMLs = array_unique($infosMLs); //On supprime les doublons

            foreach($infosMLs as $infosML)
            {
                $classes = [$infosML['classe']];
                foreach($classes as $nomClasse)
                {
                    try
                    {
                        $classe = $SystemeExterneDOs[$nomClasse];
                        $nomML = $infosML['adresse'];
                        $classe->deleteSubscriberOfSE($nomML, $anciennesInfosAdherent);
                    } catch (Exception $e)
                    {
                        ajouterErreurNotification("Une erreur a eu lieu lors de la synchronisation des systèmes externes : ".$e->getMessage());
                    }
                }
            }
        }

        //Et on l'ajout là où on doit l'ajouter
        if(!is_null($infosAdherent['courriel']) || !is_null($infosAdherent['pseudo']))
        {
            $infosMLs = SystemeExterneHaruhi::getNomMLFromIdTitle($infosAdherent['titre']);
            if($anciennesInfosAdherent['courriel'] != $infosAdherent['courriel'] || $anciennesInfosAdherent['pseudo'] != $infosAdherent['pseudo'])
                $infosMLs = array_merge($infosMLs, SystemeExterneHaruhi::getNomMLFromIdAdherent($idAdherent));

            $infosMLs = array_unique($infosMLs); //On supprime les doublons

            foreach($infosMLs as $infosML)
            {
                $classes = [$infosML['classe']];
                foreach($classes as $nomClasse)
                {
                    try
                    {
                        $classe = $SystemeExterneDOs[$nomClasse];
                        $nomML = $infosML['adresse'];
                        $classe->addSubscriberOfSE($nomML, $infosAdherent);
                    } catch (Exception $e)
                    {
                        ajouterErreurNotification("Une erreur a eu lieu lors de la synchronisation des systèmes externes : ".$e->getMessage());
                    }
                }
            }

        }
    }

    public static function supprimer($idAdherent, $infosAdherent)
    {
        global $fonctionnalites_statut;
        if(!$fonctionnalites_statut['se'])
            return null;
        //On le supprime des ML où il était 
        if(!is_null($infosAdherent['courriel']) || !is_null($infosAdherent['pseudo']))
        {
            $classesSystemeExterneDO = importerEvent('systemeExterne');
            $SystemeExterneDOs = getImplementations($classesSystemeExterneDO);

            $infosMLs = SystemeExterneHaruhi::getNomMLFromIdAdherent($idAdherent);

            foreach($infosMLs as $infosML)
            {
                $classes = ['SystemeExterneHaruhi', $infosML['classe']];
                foreach($classes as $nomClasse)
                {
                    try
                    {
                        $classe = $SystemeExterneDOs[$nomClasse];
                        $nomML = $infosML['adresse'];
                        $classe->deleteSubscriberOfSE($nomML, $infosAdherent);
                    } catch (Exception $e)
                    {
                        ajouterErreurNotification("Une erreur a eu lieu lors de la synchronisation des systèmes externes : ".$e->getMessage());
                    }
                }
            }
        }
    }
}
?>
