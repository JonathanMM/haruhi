<?php
class EnvoiMailAdherent implements AdherentEvent
{
    public static function creer($infosAdherent)
    {
        global $infos_nomasso;
        if(!is_null($infosAdherent['courriel']) && strlen($infosAdherent['courriel']) > 0 && (!is_null($infosAdherent['date_fin_cotisation']) || $infosAdherent['cotisation_infinie']))
        {
            $contenu = parserMailAdherent(file_get_contents('./mail/nouvel-adherent.txt'), $infosAdherent);
            if(!is_null($contenu))
                envoyer_mail($infosAdherent['courriel'], "Bienvenue dans l'association ".$infos_nomasso, $contenu, "cotisation", new DateTime(), $infosAdherent['id']);
        }
    }

    public static function modifier($idAdherent, $infosAdherent, $anciennesInfosAdherent)
    {
        global $infos_nomasso;
        if($anciennesInfosAdherent['date_fin_cotisation'] != $infosAdherent['date_fin_cotisation'] && !is_null($infosAdherent['courriel']) && strlen($infosAdherent['courriel']) > 0)
        {
            if(is_null($anciennesInfosAdherent['date_fin_cotisation']))
            {
                $infosAdherent['id'] = $idAdherent;
                //En réalité, c'est une nouvelle adhésion
                self::creer($infosAdherent);
            } else {
                $contenu = parserMailAdherent(file_get_contents('./mail/renouvellement-adherent.txt'), $infosAdherent);
                if(!is_null($contenu))
                    envoyer_mail($infosAdherent['courriel'], "Renouvellement de votre cotisation dans l'association ".$infos_nomasso, $contenu, "cotisation", new DateTime(), $idAdherent);
            }
        }
    }

    public static function supprimer($idAdherent, $infosAdherent)
    {

    }
}
?>
