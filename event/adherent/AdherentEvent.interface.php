<?php
interface AdherentEvent
{
	public static function creer($infosAdherent);
	public static function modifier($idAdherent, $infosAdherent, $anciennesInfosAdherent);
	public static function supprimer($idAdherent, $infosAdherent);
}
?>
