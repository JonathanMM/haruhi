<?php
session_start();
require_once('prelude_page.php');
verifierSiUtilisateurAPermission(SAUVEGARDE);

if(!isset($_GET['nom']) || strlen($_GET['nom']) == 0)
{
    header('location: sauvegardes.php');
    exit();
}

restaurer('sauvegardes/'.htmlspecialchars($_GET['nom']).'.sql');
ajouterSuccesNotification('Sauvegarde restaurée');
header('location: sauvegardes.php');
exit();
?>