<?php
session_start();
require_once('prelude_page.php');

header('Content-Type: text/csv');

//On créer les entêtes
echo 'Numéro;Prénom;Nom;Pseudo;Courriel;"Adresse Ligne 1";"Adresse Ligne 2";"Code Postal";"Ville";"Pays";Statut;"Date d\'inscription";"Date de fin de cotisation"'."\n"; 

$conditionSQL = genererRequeteSQLListeAdherent(
    isset($_POST['terme']) ? $_POST['terme'] : NULL,
    isset($_POST['titre']) ? $_POST['titre'] : NULL,
    isset($_POST['only_expired']),
    isset($_GET['tri']) ? $_GET['tri'] : NULL,
    isset($_GET['ordre']) ? $_GET['ordre'] : NULL
);
$requete = $pdo->query('SELECT *, a.id AS id, a.nom AS nom, t.nom AS nom_titre FROM '.$bdd_prefixe.'adherents a LEFT JOIN '.$bdd_prefixe.'titre t ON a.titre = t.id '.$conditionSQL);
if(!($requete === false))
{
    $items = $requete->fetchAll();
    foreach($items as $donnees)
    {
        echo ($donnees['numero'] == 0 ? '-' : $donnees['numero']).';';
        echo '"'.$donnees['prenom'].'"'.';';
        echo '"'.$donnees['nom'].'"'.';';
        echo '"'.$donnees['pseudo'].'"'.';';
        echo $donnees['courriel'].';';
        
        //Adresse
        $adresse = parseAdresse($donnees['adresse']);
        //Mode de compatibilité
        if(count($adresse) == 0)
        {
            $adresse = array(array(
                'ligne1' => $donnees['adresse'],
                'ligne2' => '',
                'cp' => '',
                'ville' => '',
                'pays' => ''
            ));
        }
        echo '"'.$adresse[0]['ligne1'].'"'.';';
        echo '"'.$adresse[0]['ligne2'].'"'.';';
        echo '"'.$adresse[0]['cp'].'"'.';';
        echo '"'.$adresse[0]['ville'].'"'.';';
        echo '"'.$adresse[0]['pays'].'"'.';';

        echo $donnees['nom_titre'].';';
        echo ($donnees['date_inscription'] == "0000-00-00" ? 'Non inscrit' : formater_date($donnees['date_inscription'])).';';
        if($donnees['cotisation_infinie'] == 1)
            echo 'À vie';
        elseif($donnees['date_fin_cotisation'] == "0000-00-00")
            echo '';
        else 
            echo formater_date($donnees['date_fin_cotisation']);
        echo ';';
        echo "\n";
    }
}
?>