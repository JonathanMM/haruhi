<?php
session_start();
require_once('prelude_page.php');

$requete = $pdo->query('SELECT * FROM '.$bdd_prefixe.'adherents WHERE id = '.$_SESSION['id']);
$user = $requete->fetch();

$requete = $pdo->query('SELECT pseudo FROM '.$bdd_prefixe.'membres WHERE id = '.$_SESSION['id']);
$compte = $requete->fetch();
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Mon compte</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen">
	</head>

	<body>
	<?php include('haut_page.php'); ?>

	<?php afficherNotification(); ?>

	<h2>Mon compte</h2>

	<h3>Informations personnelles</h3>
	<p>
		Nom : <?php echo $user['nom']; ?> <a href="modif_info_perso.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a><br />
		Prénom : <?php echo $user['prenom']; ?> <a href="modif_info_perso.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a><br />
		Pseudo : <?php echo (is_null($user['pseudo']) || strlen($user['pseudo']) == 0 ? '<i>Aucun</i>' : $user['pseudo']); ?> <a href="modif_info_perso.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a><br />
		Adresse : <?php echo (is_null($user['adresse']) || strlen($user['adresse']) == 0 ? '<i>Aucune</i>' : afficherAdresseSurUneLigne($user['adresse'])); ?> <a href="modif_info_perso.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a><br />
		Téléphone : <?php echo (is_null($user['telephone']) || strlen($user['telephone']) == 0 ? '<i>Aucun</i>' : $user['telephone']); ?> <a href="modif_info_perso.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a><br />
		Courriel : <?php echo $user['courriel']; ?> <a href="modif_info_perso.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a>
	</p>
	
	<h3>Informations de connexion</h3>
	<p>
		Identifiant : <?php echo $compte['pseudo']; ?> <a href="modif_pseudo.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a><br />
		Mot de passe : ****** <a href="modif_mdp.php"><img src="images/adherent_modifier.png" title="Modifier" alt="Modifier" /></a>
	</p>

	<nav>
	<?php if($fonctionnalites_statut['avatar']) { ?><a href="modif_avatar.php">Modifier son avatar</a><br /><?php } ?>
	<?php if($fonctionnalites_statut['factures']) { ?><a href="mes_factures.php">Voir mes factures</a><br /><?php } ?>
	<?php if($fonctionnalites_statut['mikuru'] && (int)$_SESSION['permission'] & AVOIR_COURRIEL) { ?>
		<?php if($_SESSION['type_courriel'] == 1) { ?>
		<a href="modif_mdp_courriel.php">Modifier le mot de passe de son courriel</a><br />
		<?php } elseif($_SESSION['type_courriel'] == 2) { ?>
		<a href="ajout_courriel.php">Transformer son alias en vraie boîte mail</a><br />
		<a href="modif_alias.php">Modifier son alias mail</a><br />
		<?php } else { ?>
		<a href="ajout_courriel.php">Créer une boîte mail</a><br />
		<a href="ajout_alias.php">Créer un alias mail</a><br />
		<?php } ?>
		
		<?php
		$requete = mysql_query('SELECT mikuru FROM '.$bdd_prefixe.'membres WHERE id = '.$_SESSION['id']);
		$donnees = mysql_fetch_array($requete);
		if($donnees['mikuru'] == '')
		{ ?>
		<a href="creer_identifiants_mikuru.php">Créer ses identifiants Mikuru</a>
		<?php } else { ?>
		<a href="infos_mikuru.php">Infos Mikuru</a>
		<?php } ?>
	<?php } ?>
	</nav>
	<?php include('bas_page.php'); ?>
	</body>
</html>
