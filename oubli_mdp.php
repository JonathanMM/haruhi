<?php
session_start();
require_once 'configuration.php';
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>Haruhi → Oubli de mot de passe</title>
		<link rel="icon" type="image/png" href="images/favicon.png" />

		<link rel="stylesheet" href="principal.css" type="text/css" media="screen" />
	</head>

	<body>
		<header>
			<h1>Herbier d'Adhérents Réalisé Uniquement pour l'Harmonisation Interne</h1>
			<div id="logo"><img src="images/logo.png" alt="Logo de l'association" /></div>
		</header>

		<h2>Oubli de mot de passe</h2>

		<?php
if (isset($_POST['envoi']) && $_POST['envoi'] == 2) {
    if (isset($_POST['code']) && isset($_SESSION['code']) && $_POST['code'] == $_SESSION['code']) {
        $id = intval($_POST['id']);
        $mdp = genererCodeAleatoire(16);

        $mdp_hash = hash('sha512', $mdp);
        $pdo->exec('UPDATE ' . $bdd_prefixe . 'membres SET mdp = "' . $mdp_hash . '" WHERE id = ' . $id);
        ?>
		<div class="msg msgSucces">Votre mot de passe a été renouvelé ! Votre nouveau mot de passe est : <?php echo $mdp; ?></p>
		<p><a href="index.php">&lt; Retour à l'accueil</a></p>
		<?php
} else {
        ?>
		<div class="msg msgErreur">Erreur lors de la récupération du mot de passe : le code saisi est incorrect !</div>
		<form action="oubli_mdp.php" method="post">
			<p>
			Veuillez taper le code que vous avez reçu par mail (vérifiez vos spams si vous n'avez rien reçu).
			En cas de problème, vous pouvez également nous envoyer un courriel
			<?php if (isset($infos_adressemail) && strlen($infos_adressemail) > 0) {echo ' à l\'adresse ' . $infos_adressemail;}?><br />
				<label name="code">Code : <input name="code" /></label>
				<input type="hidden" name="envoi" value="2" />
				<input type="hidden" name="id" value="<?php echo intval($_POST['id']); ?>" />
				<input type="submit" value="OK" />
			</p></form>
		<p><a href="oubli_mdp.php">&lt; Revenir en arrière</a> | <a href="index.php">&lt; Retour à l'accueil</a></p>
		<?php }

} elseif (isset($_POST['envoi']) && $_POST['envoi'] == 1) {
    $courriel = htmlspecialchars($_POST['courriel']);
    $requete = $pdo->query('SELECT id, pseudo, mail FROM ' . $bdd_prefixe . 'membres WHERE mail = "' . $courriel . '"');
    $membre = $requete->fetch();

    if (!($membre === false)) {
        $code = genererCodeAleatoire(8);

        $_SESSION['code'] = $code;
        $message = 'Bonjour ' . $membre['pseudo'] . '
	Quelqu\'un (probablement vous) a fait une demande de nouveau mot de passe sur Haruhi.
	Pour obtenir un nouveau mot de passe, le code est : ' . $code . '
	Si vous n\'avez pas demander de changement de mot de passe, merci d\'ignorer ce courriel';
        envoyer_mail($courriel, 'Oubli du mot de passe sur Haruhi', $message, 'oublimdp', null, $membre['id'], false);
    }
    ?>
		<form action="oubli_mdp.php" method="post">
			<p>Si l'adresse est associé à un compte, vous devriez recevoir un code (vérifiez dans vos spams si vous ne le trouvez pas !).<br />
			Veuillez taper le code que vous avez reçu par mail :<br />
				<label name="code">Code : <input name="code" /></label>
				<input type="hidden" name="envoi" value="2" />
				<input type="hidden" name="id" value="<?php echo $membre['id']; ?>" />
				<input type="submit" value="OK" />
			</p></form>
		<p><a href="oubli_mdp.php">&lt; Revenir en arrière</a> | <a href="index.php">&lt; Retour à l'accueil</a></p>
		<?php
} else {?>
		<?php if (isset($erreur)) {?><p><?php echo $erreur; ?></p><?php }?>
		<form action="oubli_mdp.php" method="post">
			<p>Vous avez oublié votre mot de passe ? Pas de panique ! Indiquez nous l'adresse associée à votre compte.<br />
				<label name="courriel">Courriel : <input name="courriel" type="email" /></label>
				<input type="hidden" name="envoi" value="1" />
				<input type="submit" value="OK" />
			</p></form>
		<?php }?>
		<?php include 'bas_page.php';?>
	</body>
</html>
